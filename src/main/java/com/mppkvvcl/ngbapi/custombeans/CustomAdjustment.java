package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentProfileInterface;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentTypeInterface;
import com.mppkvvcl.ngbentity.beans.Adjustment;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by ANKIT on 11-10-2017.
 */
public class CustomAdjustment extends Adjustment {

    private AdjustmentTypeInterface adjustmentType;

    private List<AdjustmentProfileInterface> adjustmentProfiles;

    public AdjustmentTypeInterface getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(AdjustmentTypeInterface adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

    public List<AdjustmentProfileInterface> getAdjustmentProfiles() {
        return adjustmentProfiles;
    }

    public void setAdjustmentProfiles(List<AdjustmentProfileInterface> adjustmentProfiles) {
        this.adjustmentProfiles = adjustmentProfiles;
    }

    public CustomAdjustment(){}

    public CustomAdjustment(AdjustmentInterface adjustmentInterface){
        if(adjustmentInterface != null){
            this.setId(adjustmentInterface.getId());
            this.setCode(adjustmentInterface.getCode());
            this.setConsumerNo(adjustmentInterface.getConsumerNo());
            this.setLocationCode(adjustmentInterface.getLocationCode());
            this.setAmount(adjustmentInterface.getAmount());
            this.setPosted(adjustmentInterface.isPosted());
            this.setPostingBillMonth(adjustmentInterface.getPostingBillMonth());
            this.setPostingDate(adjustmentInterface.getPostingDate());
            this.setDeleted(adjustmentInterface.isDeleted());
            this.setApprovalStatus(adjustmentInterface.getApprovalStatus());
            this.setRangeId(adjustmentInterface.getRangeId());
            this.setRemark(adjustmentInterface.getRemark());
            this.setCreatedBy(adjustmentInterface.getCreatedBy());
            this.setCreatedOn(adjustmentInterface.getCreatedOn());
            this.setUpdatedBy(adjustmentInterface.getUpdatedBy());
            this.setUpdatedOn(adjustmentInterface.getUpdatedOn());
        }
    }
}
