package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.ReadingDiaryNoInterface;

/**
 * Created by PREETESH on 11/7/2017.
 */
public class ReadingDiaryConsumerData {

    private ReadingDiaryNoInterface readingDiary;
    private long consumers;
    private long activeConsumers;
    private long inactiveConsumers;

    public long getConsumers() {
        return consumers;
    }

    public void setConsumers(long consumers) {
        this.consumers = consumers;
    }

    public long getActiveConsumers() {
        return activeConsumers;
    }

    public void setActiveConsumers(long activeConsumers) {
        this.activeConsumers = activeConsumers;
    }

    public long getInactiveConsumers() {
        return inactiveConsumers;
    }

    public void setInactiveConsumers(long inactiveConsumers) {
        this.inactiveConsumers = inactiveConsumers;
    }

    public ReadingDiaryNoInterface getReadingDiary() {
        return readingDiary;
    }

    public void setReadingDiary(ReadingDiaryNoInterface readingDiary) {
        this.readingDiary = readingDiary;
    }

    @Override
    public String toString() {
        return "ReadingDiaryConsumerData{" +
                "readingDiary=" + readingDiary +
                ", consumers=" + consumers +
                ", activeConsumers=" + activeConsumers +
                ", inactiveConsumers=" + inactiveConsumers +
                '}';
    }
}
