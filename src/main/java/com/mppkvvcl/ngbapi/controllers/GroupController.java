package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.GroupService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GroupInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.Group;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by PREETESH on 9/1/2017.
 */
@RestController
@RequestMapping(value = "/group" )
public class GroupController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(GroupController.class);

    /*
    Asking spring to inject GroupService object so that we can use
    its methods for performing various operations through repo.
     */
    @Autowired
    private GroupService groupService;

    /**
     * URI - /location/{location-code}<br><br>
     * This method takes the location code and fetch a groups against it.<br><br>
     * Response 200 for OK status and list of groups is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * param locationCode<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "location-code/{locationCode}", produces = "application/json")
    public ResponseEntity getByLocationCode(@PathVariable("locationCode") String locationCode) {
        String methodName = "getByLocationCode() : ";
        logger.info(methodName + "called for " + locationCode);
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage=null;
        if (locationCode != null) {
            List<GroupInterface> groups = groupService.getByLocationCode(locationCode);
            if (groups != null && groups.size()>0) {
                response = new ResponseEntity<>(groups, HttpStatus.OK);
            } else {
                errorMessage = new ErrorMessage("no groups found for location "+locationCode);
                response = new ResponseEntity<>(errorMessage,HttpStatus.NO_CONTENT);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch groups hierarchy");
        return response;
    }


    /**
     * URI - /group<br><br>
     * This create method takes group object and insert it into the backend database when user hit the URI.<br><br>
     * Response 201 for CREATED and group object is sent if successful<br>
     * Response 417 for EXPECTATION_FAILED is sent when insertion fail<br>
     * Response 400 for BAD_REQUEST is sent when input param found null.<br><br>
     * param group<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public ResponseEntity create(@RequestBody Group groupToCreate){
        String methodName = "create() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        GroupInterface createdGroup = null;
        ErrorMessage errorMessage = new ErrorMessage();
        if(groupToCreate != null ){
            createdGroup = groupService.create(groupToCreate,errorMessage);
            if(createdGroup != null){
                logger.info(methodName + "group created successfully");
                response = new ResponseEntity<>(createdGroup,HttpStatus.CREATED);
            }else{
                logger.error(methodName + "error in creating group");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "group object is NULL");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
