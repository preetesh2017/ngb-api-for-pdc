package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.SubCategoryService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SubCategoryInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by SUMIT on 24-05-2017.
 */
@RestController
@RequestMapping(value = "/sub-category")
public class SubCategoryController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(SubCategoryController.class);

    /**
     * Asking spring to inject SubCategoryService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on SubCategoryService table data at the backend database.
     */
    @Autowired
    private SubCategoryService subCategoryService;

    /**
     * URI - /sub-category/tariff/tariff-category/{tariffCategory}<br><br>
     * method which will get executed when user will hit the URI<br><br>
     * and will return various subcategories under passed tariff category in json format with 200 Status code<br>
     * param tariffCategory<br>
     * param connectionType<br>
     * param meteringStatus<br>
     * param premiseType<br><br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/tariff/tariff-category/{tariffCategory}",produces = "application/json")
    public ResponseEntity getSubCategoriesByTariffCategory(@PathVariable("tariffCategory")String tariffCategory,@RequestParam("connectionType")String connectionType,
                                           @RequestParam("meteringStatus")String meteringStatus,@RequestParam("premiseType")String premiseType){
        logger.info("getSubCategoriesByTariffCategory: Got Request to fetch sub categories with parameters as: "+ tariffCategory + " "+ connectionType
        +" "+meteringStatus+" "+premiseType);
        ResponseEntity<List<SubCategoryInterface>> subCategoriesResponse = null;
        if(tariffCategory != null && connectionType != null && meteringStatus != null && premiseType != null){
            List<SubCategoryInterface> subCategories = subCategoryService.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDateAndPremiseType(
                    tariffCategory,meteringStatus,connectionType,null,premiseType
            );
            if(subCategories != null){
                logger.info("getSubCategoriesByTariffCategory: Got "+subCategories.size()+" subcategories in controller");
                subCategoriesResponse = new ResponseEntity<List<SubCategoryInterface>>(subCategories, HttpStatus.OK);
            }
        }
        logger.info("getSubCategoriesByTariffCategory: Returning from fetch sub category");
        return subCategoriesResponse;
    }

    /**
     * URI - sub-category/tariff/tariff-category/{tariffId}<br><br>
     * method which will get executed when user will hit the URI
     * and will return various subcategories under passed tariff category in json format with 200 Status code<br><br>
     * param tariffId<br>
     * param premiseType<br>
     * param purposeOfInstallationId<br>
     * param connectedLoad<br>
     * param applicantType<br><br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/tariff/tariff-id/{tariffId}",produces = "application/json")
    public ResponseEntity getSubCategoriesByTariffId(@PathVariable("tariffId")Long tariffId,
                                                     @RequestParam("premiseType") String premiseType,
                                                     @RequestParam("purposeOfInstallationId") Long purposeOfInstallationId,
                                                     @RequestParam("connectedLoad") BigDecimal connectedLoad,
                                                     @RequestParam("applicantType") String applicantType){
        logger.info("getSubCategoriesByTariffCategory : Got Request to fetch sub categories with parameters as : tariffId"+tariffId+" "+"premiseType "
                +premiseType+" purpose of installation id "+purposeOfInstallationId+" "+"connectedLoad"+ " "+connectedLoad + " applicantType "+applicantType);
        ResponseEntity<?> response=null;
        List<SubCategoryInterface> subCategories=null;
        if(tariffId != null && premiseType !=null && purposeOfInstallationId !=null && connectedLoad!=null && applicantType!=null){
            subCategories = subCategoryService.getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoad(tariffId,premiseType,purposeOfInstallationId,applicantType,connectedLoad);
            if(subCategories != null){
                if(subCategories.size()>0){
                    logger.info("getSubCategoriesByTariffId : Got SubCategories "+subCategories);
                    response=new ResponseEntity<>(subCategories,HttpStatus.OK);
                }else{
                    logger.info("getSubCategoriesByTariffId : Got zero SubCategories");
                    response=new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error("getSubCategoriesByTariffId : SubCategory is returned NULL. Returning No Error due same");
                response=new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error("getSubCategoriesByTariffId : Input paremeter passed does not meet requirement of being NOT NULL, hence returning BAD REQUEST as response");
            response=new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return response;
    }

    @RequestMapping(value = "current/code/{code}",method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getCurrentSubCategory(@PathVariable("code") long code){
        String methodName = "getCurrentSubCategory()";
        SubCategoryInterface subCategory = null;
        ResponseEntity response = null;
        logger.info(methodName + " Got request to fetch current subcategory with subcategory code " + code);
        subCategory = subCategoryService.getCurrentSubcategory(code);
        if (subCategory != null){
            logger.info(methodName + "Successfully fetched subCategory object ");
            response = new ResponseEntity(subCategory,HttpStatus.OK);
        }else{
            logger.error(methodName + "Couldnt retrieve subCategory corresponding to code " + code);
            response = new ResponseEntity(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    @RequestMapping(value = "applicable/tariff-id/{tariffId}")
    public ResponseEntity getApplicableSubCategory(@PathVariable("tariffId") long tariffId,
                                                   @RequestParam("connectedLoad") BigDecimal connectedLoad,
                                                   @RequestParam("premiseType") String premiseType,
                                                   @RequestParam("purposeOfInstallationId") long purposeOfInstallationId,
                                                   @RequestParam("applicantType") String applicantType){
        String methodName = "getApplicableSubCategory()";
        List<SubCategoryInterface> applicableSubCategory = null;
        ResponseEntity response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + " Got request to fetch applicable subcategory with connectedLoad as  " + connectedLoad + " premiseType " + premiseType + " purposeOfInstallationId " +purposeOfInstallationId+" applicantType " +applicantType);
        applicableSubCategory = subCategoryService.getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoad(tariffId,premiseType,purposeOfInstallationId,applicantType,connectedLoad);
        if (applicableSubCategory != null && applicableSubCategory.size() > 0){
            logger.info(methodName + " Successfully got applicable subcategory ");
            response = new ResponseEntity(applicableSubCategory, HttpStatus.OK);
        }else{
            logger.error(methodName + " Got no subcategory corresponding to input parameters ");
            errorMessage = new ErrorMessage();
            errorMessage.setErrorMessage(" No Tariff Category available for such a load change");
            response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
