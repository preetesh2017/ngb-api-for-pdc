package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by PREETESH on 9/25/2017.
 */
@RestController
@RequestMapping(value = "/user")
public class UserDetailController {

    private static final Logger logger = GlobalResources.getLogger(UserDetailController.class);

    @Autowired
    UserDetailService userDetailService;

    /**
     * URI - /zone<br><br>
     * This getAllZone method fetches all the zones when user hit the URI.<br><br>
     * Response 200 for OK status and list of zones is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br><br>
     * return response<br>
     */
    @RequestMapping(method = RequestMethod.GET,value = "/location-code/{locationCode}/role/{role}", produces = "application/json")
    public ResponseEntity<?> getByLocationCodeAndRole(@PathVariable("locationCode") String locationCode,
                                                      @PathVariable("role") String role){
        String methodName = "getByLocationCodeAndRole() : ";
        ResponseEntity<?> response = null;
        logger.info(methodName + "called");
        List<UserDetail> userDetails = userDetailService.getByLocationCodeAndRole(locationCode,role);
        if(userDetails != null){
            if(userDetails.size() > 0){
                logger.info(methodName + "List received successfully with rows : " + userDetails.size());
                response = new ResponseEntity<>(userDetails, HttpStatus.OK);
            }else{
                logger.error(methodName + "No content in the list");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "List not found for zones");
            response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
