package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ConsumerInformationHistoryService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationHistoryInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 08-06-2017.
 */
@RestController
@RequestMapping(value = "consumer-information-history")
public class ConsumerInformationHistoryController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerInformationHistoryController.class);

    /**
     * Asking spring to inject ConsumerInformationHistoryService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on consumer_information_history table data at the backend database.
     */
    @Autowired
    private ConsumerInformationHistoryService consumerInformationHistoryService;

    /**
     * URI /consumer-property-name/{propertyName}<br><br>
     * This method getConsumerInformationHistoryByPropertyNameAndEndDateBetween maps to URI
     * so that any requests corresponding to getting consumer histories is given within date range specified is fulfilled.<br><br>
     * Response 200 for OK status and list of consumerInformationHistories is sent if successful.<br>
     * Response 204 for NO_CONTENT status is sent if no content in list.<br>
     * Response 417 for EXPECTATION_FAILED status is sent if list is empty.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br><br>
     * param propertyName<br>
     * param startingDate<br>
     * param endingDate<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET,value = "/consumer-property-name/{propertyName}",produces = "application/json")
    public ResponseEntity getConsumerInformationHistoryByPropertyNameAndEndDateBetween(@PathVariable("propertyName") String propertyName, @RequestParam("startingDate") Date startingDate,@RequestParam("endingDate") Date endingDate){
        String methodName = " getConsumerInformationHistoryByPropertyNameAndEndDateBetween :  ";
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get ConsumersInformationHistories by propertyName : " + propertyName + "Start Date : " + startingDate + "Ending Date " + endingDate);
        if(propertyName != null && startingDate != null && endingDate != null){
            logger.info(methodName + "Calling ConsumersInformationHistoryService method to get ConsumersInformationHistories");
            consumerInformationHistories = consumerInformationHistoryService.getConsumerInformationHistoryByPropertyNameAndEndDateBetween(propertyName,startingDate,endingDate);
            if(consumerInformationHistories != null){
                if(consumerInformationHistories.size()>0){
                    logger.info(methodName + "Got ConsumersInformationHistories as : " + consumerInformationHistories);
                    response = new ResponseEntity<>(consumerInformationHistories, HttpStatus.OK);
                }else{
                    logger.info(methodName+"Got zero ConsumersInformationHistories : " + consumerInformationHistories);
                    response = new ResponseEntity<>(consumerInformationHistories,HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + "Got ConsumersInformationHistories as null");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Got inputs may be null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI /consumer-no/{consumerNo}<br><br>
     * This method getConsumerInformationHistoryByConsumerNo maps to URI
     * so that any requests corresponding to getting consumer histories is fulfilled.<br><br>
     * Response 200 for OK status and list of consumerInformationHistories is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 417 for EXPECTATION_FAILED status is sent if list is empty.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET,value = "/consumer-no/{consumerNo}",produces = "application/json")
    public ResponseEntity getConsumerInformationHistoryByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = " getConsumerInformationHistoryByConsumerNo :  ";
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get ConsumersInformationHistories by consumerNo : " + consumerNo );
        if(consumerNo != null ){
            logger.info(methodName + "Calling ConsumersInformationHistoryService method to get ConsumersInformationHistories");
            consumerInformationHistories = consumerInformationHistoryService.getConsumerInformationHistoryByConsumerNo(consumerNo);
            if(consumerInformationHistories != null){
                if(consumerInformationHistories.size()>0){
                    logger.info(methodName + "Got ConsumersInformationHistories as : " + consumerInformationHistories);
                    response = new ResponseEntity<>(consumerInformationHistories, HttpStatus.OK);
                }else{
                    logger.info(methodName+"Got zero ConsumersInformationHistories : " + consumerInformationHistories);
                    response = new ResponseEntity<>(consumerInformationHistories,HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + "Got ConsumersInformationHistories as null");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Got input may be null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
