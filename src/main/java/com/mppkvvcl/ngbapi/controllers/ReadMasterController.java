package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomReadMaster;
import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.ConsumerConnectionAreaInformationService;
import com.mppkvvcl.ngbapi.services.ReadMasterService;
import com.mppkvvcl.ngbapi.services.ReadService;
import com.mppkvvcl.ngbapi.services.ScheduleService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.ReadMaster;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by PREETESH on 6/30/2017.
 */
@RestController
@RequestMapping(value = "consumer/meter/read")
public class ReadMasterController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadMasterController.class);

    /*
    Asking spring to inject ReadMasterService object so that we can use
    its methods for performing various operations through repo.
     */
    @Autowired
    private ReadMasterService readMasterService;

    /*
    Asking spring to inject ScheduleService to perform various operations
    in current class.
     */
    @Autowired
    private ScheduleService scheduleService;

    /*
    Asking spring to inject  ReadService object to perform various
     operations in current class.
     */
    @Autowired
    private ReadService readService;

    /*
    Asking spring to inject ConsumerConnectionAreaInformationService to perform various operations
    in current class.
     */
    @Autowired
    private ConsumerConnectionAreaInformationService consumerConnectionAreaInformationService;

    /**
     * code by nitish
     * Below method returns count of readings for consumerNo
     * @param consumerNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getCountByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            long count = readMasterService.getCountByConsumerNo(consumerNo);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * code by nitish
     * Below method returns result in paginated way based on passed
     * pageSize and pagedNumber
     * @param consumerNo
     * @param pageNumber
     * @param pageSize
     * @param sortOrder
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/paged", produces = "application/json")
    public ResponseEntity getReadingsByConsumerNoOrderByBillMonthAndId(@PathVariable("consumerNo") String consumerNo,
                                                                       @PathParam("pageNumber") int pageNumber,@PathParam("pageSize") int pageSize,
                                                                       @PathParam("sortOrder") String sortOrder) {
        String methodName = "getReadingsByConsumerNoOrderByBillMonthAndId() : ";
        logger.info(methodName + "called " + consumerNo + " pageNumber " + pageNumber + " pageSize " + pageSize + " sort " + sortOrder);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            List<ReadMasterInterface> readings = readMasterService.getByConsumerNoOrderByBillMonthWithPagination(consumerNo,pageNumber,pageSize,sortOrder);
            if (readings != null) {
                response = new ResponseEntity<>(readings, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI : consumer/meter/read/consumer-no/?<br><br>
     * This getMeterReadingByConsumerNo method takes consumerNo to fetch readMasters  with URI. <br><br>
     * Response: 200(Ok) For fetch successful readMasters<br>
     * Response: 204(No_Content): For fetch successful readMasters with zero size<br>
     * Response: 417(Expectation_failed): For fetch null readMasters<br>
     * Response: 400(Bad_Request): For received consumerNo in parameter is null.<br><br>
     * param consumerNo<br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getMeterReadingsByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getMeterReadingsByConsumerNo() : ";
        logger.info(methodName + "called for consumer no " + consumerNo);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            List<ReadMasterInterface> readings = readMasterService.getByConsumerNo(consumerNo);
            if (readings != null) {
                if (readings.size() > 0) {
                    logger.info(methodName + " Got " + readings.size() + " read masters in controller");
                    response = new ResponseEntity<List<ReadMasterInterface>>(readings, HttpStatus.OK);
                } else {
                    logger.error(methodName + "Got " + readings.size() + " read master in controller, hence returning no content");
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            } else {
                logger.error(methodName + " some error in fetching Returning error response");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }

    /**
     * URI : consumer/meter/read/latest/consumer-no/?<br><br>
     * This getLatestMeterReadingByConsumerNo method is used for find latest read with URI.<br><br>
     * Response 200 for OK with latest reading if successfully.<br>
     * Response 204 for No_Content if no content in latestReading object.<br>
     * Response 400 for BAD_REQUEST when consumerNo not found in parameter.<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/latest/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getLatestMeterReadingByConsumerNo(@PathVariable("consumerNo") String consumerNo) throws Exception {
        String methodName = "getLatestMeterReadingByConsumerNo() : ";
        logger.info(methodName + " Got requests to fetch latest reading by consumer no , " + consumerNo);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            ReadMasterInterface latestReading = readMasterService.getLatestReadingByConsumerNo(consumerNo);
            if (latestReading != null) {
                logger.info(methodName + " Got latest reading in controller");
                response = new ResponseEntity<ReadMasterInterface>(latestReading, HttpStatus.OK);
            } else {
                logger.error(methodName + "Got no reading in controller, hence returning no content");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }

    /**
     * URI: consumer/meter/read/latest/consumer-no/?/punching<br><br>
     * This getPreviousMeterReadingWithScheduleCheckByConsumerNo method is used for punching
     * latest reading of consumer with URI.<br><br>
     * Response : 200(OK) with Latest reading is sent when successfully.<br>
     * Response : 409(CONFLICT) when Schedule is present for this consumer<br>
     * Response : 417(EXPECTATION_FAILED) for any error during finding latest reading<br>
     * Response : 400(BAD_REQUEST) when consumerNo not found in parameter.<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/latest/consumer-no/{consumerNo}/punching", produces = "application/json")
    public ResponseEntity getPreviousMeterReadingWithScheduleCheckByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getPreviousMeterReadingWithScheduleCheckByConsumerNo() : ";
        logger.info(methodName + "called for consumerno" + consumerNo);
        Read read = null;
        ErrorMessage errorMessage =null;
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            try{
                read = readService.getLatestByConsumerNoForPunching(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                read = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                read = null;
            }
            if(read != null){
                logger.info(methodName + "read set successfully ");
                response =  new ResponseEntity<Read>(read, HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in fetching reading data ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }


    /**
     * URI: consumer/meter/read<br><br>
     * This insertMeterReadingsWithScheduleCheck method takes ReadMaster object to insert in ReadMaster table
     * with URI: consumer/meter/read<br><br>
     * Response: 201(CREATED): for successfully insert ReadMaster object<br>
     * Response: 417(EXPECTATION_FAILED): for generating error on an operation<br>
     * Response: 409(CONFLICT): when ReadMaster object is already exists<br>
     * Response: 400(BAD_REQUEST): when ReadMaster object not found in parameter.<br><br>
     * param reading<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
    public ResponseEntity insertMeterReadingsWithScheduleCheck(@RequestBody CustomReadMaster customReadMaster) {
        String methodName = "insertMeterReadingsWithScheduleCheck() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ReadMasterInterface latestReading = null;
        ErrorMessage errorMessage =null;
        ReadMaster reading = GlobalResources.convertCustomReadMasterToReadMaster(customReadMaster);
        if (reading != null) {
            String consumerNo = reading.getConsumerNo();
            if (consumerNo != null) {
                consumerNo = consumerNo.trim();
                try{
                    latestReading = readMasterService.insertReadMasterWithReadMasterKWAndReadMasterPF(reading);
                }catch(RuntimeException e){
                    errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                    logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                    latestReading = null;
                }catch (Exception ee){
                    errorMessage = new ErrorMessage(ee.getMessage());
                    logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                    latestReading = null;
                }
                if (latestReading != null) {
                    logger.info(methodName + " updated latest reading in controller");
                    response = new ResponseEntity<>(latestReading, HttpStatus.CREATED);
                } else {
                    logger.error(methodName + " some error in insertion. Sending exception object so that client may fulfill decision requirements.");
                    response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
                }

            } else {
                logger.error(methodName + " inputs parameters is null.Returning bad request");
                response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }

    /**
     * Added By Vikas
     * @param customReadMaster
     * @param finalRead
     * @return
     */
    @RequestMapping(method = RequestMethod.POST,value = "/meter-restart/final-read/{finalRead}",consumes = "application/json", produces = "application/json")
    public ResponseEntity insertMeterReStartReadingsWithScheduleCheck(@RequestBody CustomReadMaster customReadMaster, @PathVariable("finalRead") long finalRead ) {
        String methodName = "insertMeterReadingsWithScheduleCheck(): ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ReadMasterInterface latestReading = null;
        ErrorMessage errorMessage =null;
        ReadMaster currentReading = GlobalResources.convertCustomReadMasterToReadMaster(customReadMaster);
        if (currentReading != null ) {
            if(finalRead == ReadMasterInterface.FOUR_DIGIT_METER_MAX_VALUE || finalRead == ReadMasterInterface.FIVE_DIGIT_METER_MAX_VALUE || finalRead == ReadMasterInterface.SIX_DIGIT_METER_MAX_VALUE) {
                String consumerNo = currentReading.getConsumerNo();
                logger.info(methodName + "  requests to insert Meter Restart readings for consumer no : " + consumerNo);
                logger.info(methodName + "Got request as" + currentReading + "And Meter Final ReStart Read :" + finalRead);
                if (consumerNo != null) {
                    try {
                        latestReading = readMasterService.insertMeterReStartRead(currentReading, finalRead);

                    } catch (RuntimeException e) {
                        errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                        logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                        latestReading = null;
                    } catch (Exception ee) {
                        errorMessage = new ErrorMessage(ee.getMessage());
                        logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                        latestReading = null;
                    }
                    if (latestReading != null) {
                        logger.info(methodName + " updated latest currentReading in controller");
                        response = new ResponseEntity<>(latestReading, HttpStatus.CREATED);
                    } else {
                        logger.error(methodName + " some error in insertion. Sending exception object so that client may fulfill decision requirements.");
                        response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
                    }

                } else {
                    logger.error(methodName + " inputs parameters is null.Returning bad request");
                    response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            }else {
                logger.error(methodName + " Given Final Read is invalid.Returning bad request");
                response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }

    /**
     * URI - consumer/meter/read/consumer-no/{consumerNo}/editing<br><br>
     * Added By: Preetesh Date 11 July 2017<br>
     * Reading editing before next billing cycle.<br><br>
     * This URI will return all the reading which has not participated in billing and one reading from last bill month.<br><br>
     * Response 200 for OK with list of readings is sent if successfully.<br>
     * Response 417 for EXPECTATION_FAILED if no content found.<br>
     * Response 400 for BAD_REQUEST when consumerNo not found in parameter.<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/editing", produces = "application/json")
    public ResponseEntity getEditableMeterReadingsByConsumerNo(@PathVariable("consumerNo") String consumerNo) throws Exception {
        String methodName = "getEditableMeterReadingsByConsumerNo() : ";
        List<ReadMasterInterface> readings  = null;
        logger.info(methodName + " Got requests to fetch editable readings data for consumer no , " + consumerNo);
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            try{
                readings = readMasterService.getReadingsByConsumerNoForReadingsEditing(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                readings = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                readings = null;
            }
            if(readings != null){
                logger.info(methodName + "readings set successfully ");
                response =  new ResponseEntity<List<ReadMasterInterface>>(readings, HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in fetching reading data ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }

    /**
     * URI - consumer/meter/read/consumer-no/{consumerNo}/editing<br><br>
     * Added By: Preetesh Date 17 July 2017<br>
     * Reading editing before next billing cycle.<br><br>
     * This URI will update all the reading which has not participated in billing.<br><br>
     * Response 200 for OK with list of updatedReadings is sent if successfully.<br>
     * Response 417 for EXPECTATION_FAILED if fails.<br>
     * Response 400 for BAD_REQUEST when input param found null.<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/consumer-no/{consumerNo}/editing", produces = "application/json")
    public ResponseEntity setEditableMeterReadingsByConsumerNo(@PathVariable("consumerNo") String consumerNo, @RequestBody List<CustomReadMaster> customReadings ) {
        String methodName = "setEditableMeterReadingsByConsumerNo() : ";
        logger.info(methodName + "called for consumer no , " + consumerNo);
        ResponseEntity<?> response = null;
        List<ReadMasterInterface>  updatedReadings =null;
        ErrorMessage errorMessage =null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            try{
                List<ReadMasterInterface> readings = GlobalResources.convertCustomReadMasterToReadMaster(customReadings);
                updatedReadings = readMasterService.updateReadingsByConsumerNo(consumerNo,readings);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                //up = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                //latestReading = null;
            }
            if(updatedReadings != null){
                logger.info(methodName + "readings set successfully ");
                response =  new ResponseEntity<String>(updatedReadings.size()+" readings updated successfully", HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in inserting reading data ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }


    /**
     * URI - consumer/meter/read/consumer-no/{consumerNo}/bill-correction<br><br>
     * Added By: Preetesh Date 15 July 2017<br>
     * Bill correction<br><br>
     * This URI will return all the reading which has participated in last billing and one reading from last bill month from latest billing month.<br><br>
     * Response 200 for OK with list of readings is sent if successfully.<br>
     * Response 417 for EXPECTATION_FAILED if fails.<br>
     * Response 400 for BAD_REQUEST when input param found null.<br><br>
     * param consumerNo<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/bill-correction", produces = "application/json")
    public ResponseEntity getMeterReadingsByConsumerNoForBillCorrection(@PathVariable("consumerNo") String consumerNo) throws Exception {
        String methodName = "getEditableMeterReadingsByConsumerNo() : ";
        List<ReadMasterInterface> readings  = null;
        logger.info(methodName + " Got requests to fetch editable readings data for consumer no , " + consumerNo);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            readings = readMasterService.getByConsumerNoForBillCorrection(consumerNo);
            if(readings != null){
                logger.info(methodName + "readings set successfully, "+readings.size());
                response =  new ResponseEntity<List<ReadMasterInterface>>(readings, HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in fetching reading data ");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/billMonth/{billMonth}/replacementFlag/{replacementFlag}/usedOnBill/{usedOnBill}", produces = "application/json")
    public ResponseEntity getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBill(@PathVariable("consumerNo") String consumerNo, @PathVariable ("billMonth") String billMonth, @PathVariable("replacementFlag") String replacementFlag, @PathVariable("usedOnBill") Boolean usedOnBill) {
        String methodName = "getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBill() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        if (consumerNo != null && billMonth != null && replacementFlag != null && usedOnBill != null) {
            List<ReadMasterInterface> readings = readMasterService.getByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(consumerNo,billMonth,replacementFlag,usedOnBill);
            if (readings != null) {
                logger.info(methodName + " Got  reading size: " + readings.size());
                if(readings.size() > 0){
                    response = new ResponseEntity<>(readings, HttpStatus.OK);
                }else{
                    response = new ResponseEntity<>(readings, HttpStatus.NO_CONTENT);
                }
            } else {
                logger.error(methodName + "Got reading size: " + readings.size() + " read master in controller");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.POST,value = "/multiple", consumes = "application/json", produces = "application/json")
    public ResponseEntity insertMultipleMeterReadingsWithScheduleCheck(@RequestBody List<CustomReadMaster> customReadMaster) {
        String methodName = "insertMultipleMeterReadingsWithScheduleCheck() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        List<ReadMasterInterface> reading = null;
        if(customReadMaster != null && customReadMaster.size() >0) {
            reading = GlobalResources.convertCustomReadMasterToReadMaster(customReadMaster);
            if(reading != null && reading.size() > 0){
                List<ReadMasterInterface> insertedReadMasterInterfaces = new ArrayList<>();
                ReadMasterInterface insertedReadMasterInterface = null;
                HashMap<String,String> map = new HashMap<>();
                logger.info(methodName+ "total reading received to insert "+reading.size());
                for(ReadMasterInterface readMasterInterface : reading) {
                    try {
                        insertedReadMasterInterface = readMasterService.insertReadMasterWithReadMasterKWAndReadMasterPF(readMasterInterface);
                        if (insertedReadMasterInterface != null) {
                            logger.info(methodName+ "reading inserted for consumer no: "+insertedReadMasterInterface.getConsumerNo());
                            insertedReadMasterInterfaces.add(insertedReadMasterInterface);
                        }
                    }catch(RuntimeException exception){
                        logger.error(methodName + "Received Runtime Exception in controller with error message as :" + exception.getMessage());
                        if(readMasterInterface != null) {
                            logger.info(methodName+ " some error to insert consumer no :"+readMasterInterface.getConsumerNo()+" error message "+exception.getMessage());
                            map.put(readMasterInterface.getConsumerNo(), exception.getMessage());
                        }
                    }catch(Exception exception){
                        logger.error(methodName + "Received Exception in controller with error message as :" + exception.getMessage());
                        if(readMasterInterface != null) {
                            logger.info(methodName+ " some error to insert consumer no :"+readMasterInterface.getConsumerNo()+" error message "+exception.getMessage());
                            map.put(readMasterInterface.getConsumerNo(), exception.getMessage());
                        }
                    }
                }
                if (insertedReadMasterInterfaces != null && insertedReadMasterInterfaces.size() > 0) {
                    if(insertedReadMasterInterfaces.size() == reading.size()) {
                        logger.info(methodName + " all reading inserted successfully ");
                        response = new ResponseEntity<>(insertedReadMasterInterfaces, HttpStatus.CREATED);
                    }else {
                        logger.info(methodName + " some readings are not inserted");
                        response = new ResponseEntity<>(map, HttpStatus.MULTI_STATUS);
                    }
                } else {
                    logger.error(methodName + " some error in insertion. Sending exception object so that client may fulfill decision requirements.");
                    response = new ResponseEntity<>(map,HttpStatus.EXPECTATION_FAILED);
                }
            } else {
                logger.error(methodName + "Error in Global Resources converter custom read to read master.Returning bad request");
                response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}




