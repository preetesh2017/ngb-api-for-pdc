package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.PremiseTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PremiseTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */
@RestController
@RequestMapping(value = "premise-type")
public class PremiseTypeController {

    /**
     * Getting logger for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(PremiseTypeController.class);

    /**
     * Asking spring to get Singleton object of PremiseTypeService.
     */
    @Autowired
    private PremiseTypeService premiseTypeService;

    /**
     * URI : /premise-type
     * This method is used for getting all PremiseType object with URI.<br><br>
     * Response : 200(Ok) for successfully found the list of PremiseType<br>
     * Response : 417(EXPECTATION_FAILED) for facing problem to get PremiseType list<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , produces =  "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        List<? extends PremiseTypeInterface> premiseTypes = premiseTypeService.getAll();
        if (premiseTypes != null){
            if(premiseTypes.size() > 0 ){
                logger.info(methodName+"PremiseType list found successfully ");
                response = new ResponseEntity<Object>(premiseTypes , HttpStatus.OK);
            }else{
                logger.error(methodName+"PremiseTyepe list found with size"+premiseTypes.size());
                response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+"PremiseType list return null");
            response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
