package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.MeterModemMappingService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterModemMappingInterface;
import com.mppkvvcl.ngbentity.beans.MeterModemMapping;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by VIKAS on 7/17/2017.
 */

@RestController
@RequestMapping(value = "/meter/modem/mapping")
public class MeterModemMappingController {

    /**
     * Getting logger object for logging in MeterModemMappingController class.
     */
    private static final Logger logger = GlobalResources.getLogger(MeterModemMappingController.class);

    /**
     * This provides Dependency Injection by spring at runtime for meterModemMappingService
     */
    @Autowired
    private MeterModemMappingService meterModemMappingService;

    /**
     * URI - /meter/modem/mapping/meter-identifier/{meterIdentifier}<br><br>
     * This method takes meterIdentifier and fetches the list of meterModemMappings when user hit the URI.<br><br>
     * Response 200 for OK with list of meterModemMappings if successfully.<br>
     * Response 204 for No_Content if no content in meterModemMappings list.<br>
     * Response 404 for BAD_REQUEST when meterIdentifier not found in parameter.<br><br>
     * param meterIdentifier<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/meter-identifier/{meterIdentifier}", produces = "application/json")
    public ResponseEntity getByMeterIdentifier(@PathVariable("meterIdentifier") String meterIdentifier) {
        String methodName = "getByMeterIdentifier() :";
        List<MeterModemMappingInterface> meterModemMappings = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to find MeterModemMapping");
        if (meterIdentifier != null) {
            meterModemMappings = meterModemMappingService.getByMeterIdentifier(meterIdentifier);
            if (meterModemMappings != null && meterModemMappings.size() > 0) {
                logger.info(methodName + " Successfully fetched record" + meterModemMappings);
                response = new ResponseEntity<>(meterModemMappings, HttpStatus.OK);
            } else {
                logger.error(methodName + "Got zero records");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } else {
            logger.error(methodName + "got request as Null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /meter/modem/mapping<br><br>
     * This method takes meterModemMapping object and insert it into backend database when user hit the URI.<br><br>
     * Response 200 for OK with insertedMeterModemMapping if insertion successfully.<br>
     * Response 417 for EXPECTATION_FAILED if insertion fails.<br>
     * Response 400 for BAD_REQUEST when meterIdentifier not found in parameter.<br><br>
     * param meterIdentifier<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public ResponseEntity insertMeterModemMapping(@RequestBody MeterModemMapping meterModemMapping){
        String methodName = "insertMeterModemMapping() :";
        MeterModemMappingInterface insertedMeterModemMapping = null;
        ResponseEntity<?> response = null ;
        logger.info(methodName+"Got request to Insert MeterModemMapping");
        if(meterModemMapping != null) {
            logger.info(methodName+"Calling Service method to Insert MeterModemMapping");
                insertedMeterModemMapping = meterModemMappingService.insertByMeterIdentifier(meterModemMapping);
                if (insertedMeterModemMapping != null) {
                    logger.info(methodName + "Successfully inserted ");
                    response = new ResponseEntity<>(insertedMeterModemMapping, HttpStatus.CREATED);
                } else {
                    logger.error(methodName + " Some error to insert record");
                    response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
                }
            } else {
                logger.error(methodName + "got request null");
                response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        return response;
    }


}
