package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomAdjustment;
import com.mppkvvcl.ngbapi.services.AdjustmentProfileService;
import com.mppkvvcl.ngbapi.services.AdjustmentService;
import com.mppkvvcl.ngbapi.services.AdjustmentTypeService;
import com.mppkvvcl.ngbapi.services.ZoneService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Created by SHIVANSHU on 15-07-2017.
 */
@RestController
@RequestMapping(value = "/adjustment")
public class
AdjustmentController {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(AdjustmentController.class);

    /**
     * Asking spring to get Singleton object of AdjustmentService.
     */
    @Autowired
    private AdjustmentService adjustmentService;

    /**
     * Asking spring to get Singleton object of AdjustmentTypeService.
     */
    @Autowired
    private AdjustmentTypeService adjustmentTypeService;

    /**
     * Asking spring to get Singleton object of AdjustmentProfileService.
     */
    @Autowired
    private AdjustmentProfileService adjustmentProfileService;

    /**
     * Asking spring to get Singleton object of LocationService.
     */
    @Autowired
    private ZoneService zoneService;

    /**
     * URI : adjustment <br>
     * Method : POST<br>
     * Response : 201(Created) when inserted the adjustment successfully in the Database.<br>
     * Response : 400 (Bad_Request) If Adjustment is null.<br>
     * param adjustment<br>
     * return
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity insert(@RequestBody Adjustment adjustment) {
        String methodName = "insert() : ";
        ErrorMessage errorMessage = null;
        ResponseEntity response = null;
        CustomAdjustment customAdjustment = null;
        logger.info(methodName + "called");
        if (adjustment != null) {
            try {
                AdjustmentInterface insertedAdjustment = adjustmentService.insert(adjustment);
                customAdjustment = adjustmentService.prepareCustomAdjustment(adjustment);
                if(customAdjustment != null){
                    response = new ResponseEntity(customAdjustment,HttpStatus.CREATED);
                }else{
                    errorMessage = new ErrorMessage("Some Error occurred While inserting the Adjustment.");
                    logger.error("Could not insert Adjustment as : ");
                    response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Internal Error Occurred.");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch(Exception exp){
                errorMessage = new ErrorMessage(exp.getMessage());
                logger.error(methodName+" Received Exception inserting the adjustment and the reason is: "+exp.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + "Input param adjustment found null");
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI : adjustment/consumer-no/?/posting-bill-month/?<br><br>
     * This getByConsumerNoAndPostingBillMonth method is used for getting Adjustment object with URI.<br><br>
     * Response  : 200(Ok) when Adjustment object found successfully.<br>
     * Response  : 404(NOT_FOUND) when no Adjustment object found.<br>
     * Response  : 400(BAD_REQUEST) for facing problem to get consumerNo or postingBillMonth.<br><br>
     * param consumerNo<br>
     * param postingBillMonth<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , value = "/consumer-no/{consumerNo}/posting-bill-month/{postingBillMonth}" ,produces = "application/json")
    public ResponseEntity getByConsumerNoAndPostingBillMonth(@PathVariable("consumerNo") String consumerNo , @PathVariable("postingBillMonth") String postingBillMonth){
        String methodName  = "getByConsumerNoAndPostingBillMonth() : ";
        logger.info(methodName+"called");
        ResponseEntity response = null;
        CustomAdjustment customAdjustment = null;
        ErrorMessage errorMessage;
        if (consumerNo != null && postingBillMonth != null){
            try {
                AdjustmentInterface adjustment = adjustmentService.getByConsumerNoAndPostingBillMonth(consumerNo, postingBillMonth); //detected wrong on 08012018. Need to correct
                customAdjustment = adjustmentService.prepareCustomAdjustment(adjustment);
                if(customAdjustment != null) {
                    response = new ResponseEntity(customAdjustment, HttpStatus.OK);
                }else {
                    errorMessage = new ErrorMessage("NO Adjustment Found.");
                    response = new ResponseEntity(errorMessage,HttpStatus.NO_CONTENT);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Some Internal Error Occurred");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            errorMessage = new ErrorMessage("Consumer or Bill Month is Not Correct.");
            logger.error(methodName+"consumerNo"+consumerNo+" or postingBillMonth"+postingBillMonth+" not found successfully");
            response = new ResponseEntity(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI : adjustment/consumer-no/?<br><br>
     * This getByConsumerNo method is used for getting Adjustment object with URI.<br><br>
     * Response  : 200(Ok) when Adjustment object found successfully.<br>
     * Response  : 204(NO_CONTENT) when no Adjustment object found.<br>
     * Response  : 400(BAD_REQUEST) for facing problem to get consumerNo or postingBillMonth.<br><br>
     * param consumerNo
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}" , produces = "application/json")
    public ResponseEntity getByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName  = "getByConsumerNo() : ";
        logger.info(methodName + "called for consumer no " + consumerNo);
        ResponseEntity response = null;
        List<CustomAdjustment> customAdjustments = null;
        ErrorMessage errorMessage = null;
        if(consumerNo != null){
            try {
                List<? extends AdjustmentInterface> adjustments = adjustmentService.getByConsumerNo(consumerNo);
                customAdjustments = adjustmentService.prepareCustomAdjustments(adjustments);
                if(customAdjustments != null && customAdjustments.size() > 0) {
                    response = new ResponseEntity(customAdjustments, HttpStatus.OK);
                }else {
                    errorMessage = new ErrorMessage("NO Adjustment Found.");
                    response = new ResponseEntity(errorMessage,HttpStatus.NO_CONTENT);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Some Internal Error Occurred");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName  + " Consumer No given is null");
            errorMessage = new ErrorMessage("Consumer No Not Correct");
            response = new ResponseEntity(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * code by nitish
     * Below method returns count of readings for consumerNo
     * @param consumerNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getCountByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            long count = adjustmentService.getCountByConsumerNo(consumerNo);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/paged" , produces = "application/json")
    public ResponseEntity getByConsumerNoWithPagination(@PathVariable("consumerNo") String consumerNo,
                                                        @PathParam("sortBy") String sortBy, @PathParam("sortOrder") String sortOrder,
                                                        @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        String methodName  = "getByConsumerNo() : ";
        logger.info(methodName + "called for consumer no " + consumerNo);
        ResponseEntity response = null;
        List<CustomAdjustment> customAdjustments = null;
        ErrorMessage errorMessage = null;
        if(consumerNo != null){
            try {
                List<? extends AdjustmentInterface> adjustments = adjustmentService.getByConsumerNoWithPagination(consumerNo,sortBy,sortOrder,pageNumber,pageSize);
                customAdjustments = adjustmentService.prepareCustomAdjustments(adjustments);
                if(customAdjustments != null && customAdjustments.size() > 0) {
                    response = new ResponseEntity(customAdjustments, HttpStatus.OK);
                }else {
                    errorMessage = new ErrorMessage("NO Adjustment Found.");
                    response = new ResponseEntity(errorMessage,HttpStatus.NO_CONTENT);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Some Internal Error Occurred");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName  + " Consumer No given is null");
            errorMessage = new ErrorMessage("Consumer No Not Correct");
            response = new ResponseEntity(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI : adjustment/consumer-no/?<br><br>
     * This getByConsumerNo method is used for getting Adjustment object with URI.<br><br>
     * Response  : 200(Ok) when Adjustment object found successfully.<br>
     * Response  : 204(NO_CONTENT) when no Adjustment object found.<br>
     * Response  : 400(BAD_REQUEST) for facing problem to get consumerNo or postingBillMonth.<br><br>
     * param consumerNo
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "user-role/{userRole}/location-code/{locationCode}" , produces = "application/json")
    public ResponseEntity getByLocationCodeAndRole(@PathVariable("locationCode") String locationCode, @PathVariable("userRole") String userRole){
        String methodName  = "getByLocationCodeAndRole() : ";
        logger.info(methodName + "called");
        ResponseEntity response = null;
        ErrorMessage errorMessage = null;
        List<CustomAdjustment> customAdjustments = null;
        if(locationCode != null && userRole != null) {
            try{
                List<? extends AdjustmentInterface> adjustments = adjustmentService.getAllOpenAdjustmentByLocationCodeAndUserRole(locationCode,userRole);
                customAdjustments = adjustmentService.prepareCustomAdjustments(adjustments);
                if(customAdjustments != null && customAdjustments.size() != 0) {
                    response = new ResponseEntity(customAdjustments, HttpStatus.OK);
                }else{
                    errorMessage = new ErrorMessage("No Adjustments Found.");
                    response = new ResponseEntity(errorMessage, HttpStatus.NO_CONTENT);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Some Internal Error Occurred");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            errorMessage = new ErrorMessage("Invalid User.");
            logger.error(methodName + " Location Code given is null");
            response = new ResponseEntity(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * code by nitish
     * Below method returns count of readings for consumerNo
     * @param locationCode
     * @param userRole
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/user-role/{userRole}/location-code/{locationCode}", produces = "application/json")
    public ResponseEntity getCountByLocationCodeAndRole(@PathVariable("locationCode") String locationCode, @PathVariable("userRole") String userRole) {
        String methodName = "getCountByLocationCodeAndRole() : ";
        logger.info(methodName + "called " + locationCode + " " + userRole);
        ResponseEntity<?> response = null;
        if (locationCode != null && userRole != null) {
            long count = adjustmentService.getAllOpenAdjustmentCountByLocationCodeAndUserRole(locationCode,userRole);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "user-role/{userRole}/location-code/{locationCode}/paged" , produces = "application/json")
    public ResponseEntity getByLocationCodeAndRoleWithPagination(@PathVariable("locationCode") String locationCode, @PathVariable("userRole") String userRole,
                                                                 @PathParam("sortBy") String sortBy, @PathParam("sortOrder") String sortOrder,
                                                                 @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        String methodName  = "getByLocationCodeAndRoleWithPagination() : ";
        logger.info(methodName + "called");
        ResponseEntity response = null;
        ErrorMessage errorMessage = null;
        List<CustomAdjustment> customAdjustments = null;
        if(locationCode != null && userRole != null) {
            try{
                List<AdjustmentInterface> adjustments = adjustmentService.getAllOpenAdjustmentByLocationCodeAndUserRoleWithPagination(locationCode,userRole,sortBy,sortOrder,pageNumber,pageSize);
                customAdjustments = adjustmentService.prepareCustomAdjustments(adjustments);
                if(customAdjustments != null && customAdjustments.size() != 0) {
                    response = new ResponseEntity(customAdjustments, HttpStatus.OK);
                }else{
                    errorMessage = new ErrorMessage("No Adjustments Found.");
                    response = new ResponseEntity(errorMessage, HttpStatus.NO_CONTENT);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Some Internal Error Occurred");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                response = new ResponseEntity(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            errorMessage = new ErrorMessage("Invalid User.");
            logger.error(methodName + " Location Code given is null");
            response = new ResponseEntity(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI : adjustment/consumer-no/?/approval-status/?<br><br>
     * This getByConsumerNoAndApprovalStatus method is used for getting Adjustment object with URI.<br><br>
     * Response  : 200(Ok) when Adjustment object found successfully.<br>
     * Response  : 204(NO_CONTENT) when no Adjustment object found.<br>
     * Response  : 400(BAD_REQUEST) for facing problem to get consumerNo <br><br>
     * param consumerNo
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/approval-status/{approvalStatus}" , produces = "application/json")
    public ResponseEntity getByConsumerNoAndApprovalStatus(@PathVariable("consumerNo") String consumerNo, @PathVariable("approvalStatus") String approvalStatus){
        String methodName  = "getByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called");
        ResponseEntity response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        List<CustomAdjustment> customAdjustments = null;
        if(consumerNo != null && approvalStatus != null ){
            customAdjustments = adjustmentService.getByConsumerNoAndApprovalStatus(consumerNo,approvalStatus, errorMessage);
        }else{
            errorMessage.setErrorMessage("passed parameters are null");
        }
        if(customAdjustments != null){
            response = new ResponseEntity<>(customAdjustments,HttpStatus.OK);
        }else{
            logger.info(methodName + "no adjustment fetched: "+errorMessage);
            response = new ResponseEntity<>(errorMessage,HttpStatus.NO_CONTENT);
        }
        return response;
    }

    /**
     * code by nitish
     * Below method returns count of readings for consumerNo
     * @param consumerNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/consumer-no/{consumerNo}/approval-status/{approvalStatus}", produces = "application/json")
    public ResponseEntity getCountByConsumerNoAndApprovalStatus(@PathVariable("consumerNo") String consumerNo,@PathVariable("approvalStatus") String approvalStatus) {
        String methodName = "getCountByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called " + consumerNo + " " + approvalStatus);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            long count = adjustmentService.getCountByConsumerNoAndApprovalStatus(consumerNo,approvalStatus);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/approval-status/{approvalStatus}/paged" , produces = "application/json")
    public ResponseEntity getByConsumerNoAndApprovalStatusWithPagination(@PathVariable("consumerNo") String consumerNo, @PathVariable("approvalStatus") String approvalStatus,
                                                           @PathParam("sortBy") String sortBy, @PathParam("sortOrder") String sortOrder,
                                                           @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        String methodName  = "getByConsumerNoAndApprovalStatusWithPagination() : ";
        logger.info(methodName + "called");
        ResponseEntity response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        List<CustomAdjustment> customAdjustments = null;
        if(consumerNo != null && approvalStatus != null ){
            customAdjustments = adjustmentService.getByConsumerNoAndApprovalStatusWithPagination(consumerNo,approvalStatus,sortBy,sortOrder,pageNumber,pageSize);
            if(customAdjustments != null){
                response = new ResponseEntity<>(customAdjustments,HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(errorMessage,HttpStatus.NO_CONTENT);
            }
        }else{
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
