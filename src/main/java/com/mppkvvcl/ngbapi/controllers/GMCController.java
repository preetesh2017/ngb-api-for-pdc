package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.GMCService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@RestController
@RequestMapping(value = "/gmc")
public class GMCController {

    /**
     * Requesting spring to get singleton GMCService object.
     */
    @Autowired
    private GMCService gmcService;

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(GMCController.class);

}
