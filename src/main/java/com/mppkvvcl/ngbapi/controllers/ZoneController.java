package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ZoneService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ANSHIKA on 17-07-2017.
 */
@RestController
@RequestMapping(value = "/zone")
public class ZoneController {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ZoneController.class);

    /**
     * Requesting spring to get singleton ZoneController object.
     */
    @Autowired
    ZoneService zoneService;

    /**
     * coded by nitish
     * @param locationCode
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/location-code/{locationCode}",produces = "application/json")
    public ResponseEntity<?> getZoneByLocationCode(@PathVariable("locationCode")String locationCode){
        final String methodName = "getZoneByLocationCode() : ";
        logger.info(methodName + "called for location code " + locationCode);
        ResponseEntity<?> response = null;
        ZoneInterface zone = null;
        if(locationCode != null){
            zone = zoneService.getByLocationCode(locationCode);
        }
        if(zone != null){
            response = new ResponseEntity<>(zone,HttpStatus.OK);
        }else{
            ErrorMessage errorMessage = new ErrorMessage("No Zone Found for location code " + locationCode);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
