package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.ISIEnergySavingTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ISIEnergySavingTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "isi-energy-saving-type")
public class ISIEnergySavingTypeConstroller {
    private static final Logger logger = GlobalResources.getLogger(ISIEnergySavingTypeConstroller.class);

    @Autowired
    private ISIEnergySavingTypeService isiEnergySavingTypeService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getAll(){
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> responseEntity = null;
        List<? extends ISIEnergySavingTypeInterface> isiEnergySavingTypeInterfaces = isiEnergySavingTypeService.getAll();
        if(isiEnergySavingTypeInterfaces != null){
            responseEntity = new ResponseEntity<>(isiEnergySavingTypeInterfaces,HttpStatus.OK);
        }else{
            responseEntity = new ResponseEntity<>(isiEnergySavingTypeInterfaces,HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}
