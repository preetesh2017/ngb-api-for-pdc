package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.PostMeterReplacement;
import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.*;
import com.mppkvvcl.ngbdao.daos.ConsumerMeterMappingDAO;
import com.mppkvvcl.ngbentity.beans.ConsumerMeterMapping;
import com.mppkvvcl.ngbentity.beans.MeterCTRMapping;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 6/22/2017.
 */
@Service
public class ConsumerMeterMappingService {

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerMeterMappingService.class);


    /**
     * Requesting Spring to inject ConsumerMeterMappingRepository object.
     */
    @Autowired
    private ConsumerMeterMappingDAO consumerMeterMappingDAO;

    /**
     * Requesting Spring to inject MeterMasterService object.
     */
    @Autowired
    private MeterMasterService meterMasterService;

    /**
     * Requesting Spring to inject MeterCTRMappingService object.
     */
    @Autowired
    private MeterCTRMappingService meterCTRMappingService;

    /**
     * Requesting Spring to inject ScheduleService object.
     */
    @Autowired
    private  ScheduleService scheduleService;

    /**
     * Requesting Spring to inject CTRMasterService object.
     */
    @Autowired
    private  CTRMasterService ctrMasterService;

    /**
     * Requesting Spring to inject ReadMasterService object.
     */
    @Autowired
    private ReadMasterService readMasterService;


    /**
     *
     * param identifier
     * param mappingStatus
     * return
     */
    public List<ConsumerMeterMappingInterface> getByIdentifierAndMappingStatus(String identifier, String mappingStatus) {
        String methodName = " getByIdentifierAndMappingStatus(): ";
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        logger.info(methodName + "fetch meter Mapping started");
        if (identifier != null && mappingStatus != null) {
            consumerMeterMappings =  consumerMeterMappingDAO.getByMeterIdentifierAndMappingStatus(identifier,mappingStatus);
            if(consumerMeterMappings.size() > 0){
                logger.info(methodName + "successfully fetched row");
            }else{
                logger.error(methodName + "no mapping found for meter");
            }
        }
        else{
            logger.error(methodName + "identifier is null");
        }
        return consumerMeterMappings;

    }

    /**
     *
     * param meterIdentifier
     * return
     */
    public List<ConsumerMeterMappingInterface> getByIdentifier(String meterIdentifier) {
        String methodName = " getByIdentifier(): ";
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        logger.info(methodName + "fetch meter Mapping started");

        if (meterIdentifier != null) {
            consumerMeterMappings =  consumerMeterMappingDAO.getByMeterIdentifier(meterIdentifier);
            if(consumerMeterMappings.size() > 0){
                logger.info(methodName + "successfully fetched row");
            }else{
                logger.error(methodName + "no mapping found for meter");
            }
        }
        else{
            logger.error(methodName + "meterIdentifier is null");
        }
        return consumerMeterMappings;
    }

    /**
     *
     * param consumerNo
     * param status
     * return
     */
    public List<ConsumerMeterMappingInterface> getByConsumerNoAndMappingStatus(String consumerNo, String status) throws  Exception {
        String methodName = " getByConsumerNoAndMappingStatus(): ";
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        MeterMasterInterface meterMaster = null;
        List<MeterCTRMappingInterface> meterCTRMappings = null;
        logger.info(methodName + "fetching Consumer meter Mapping started");
        if (consumerNo == null || status == null) {
            logger.error(methodName + "Given input parameter is Null" );
            throw new Exception("Given input parameter is Null");
        }
        consumerMeterMappings =  consumerMeterMappingDAO.getByConsumerNoAndMappingStatus(consumerNo,status);
        if(consumerMeterMappings == null || consumerMeterMappings.size() <= 0) {
            logger.error(methodName + "some error in fetching in Consumer Meter mapping");
            throw new Exception("some error in fetching in Consumer Meter mapping");
        }
        logger.info(methodName + "fetched consumer meter mapping");
        for(ConsumerMeterMappingInterface consumerMeterMapping : consumerMeterMappings) {

            //Commenting below code since using Hibernate's OneToOne Mapping
            //in ConsumerMeterMapping Entity
            /*
            String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
            if (meterIdentifier == null) {
                logger.error(methodName + "some error in fetching in MeterIdentifier");
                continue;
            }
            logger.info(methodName + "fetching meter master by Meter identifier:" + meterIdentifier);
            meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
            if(meterMaster == null){
                logger.error(methodName + "meterMaster is null for identifier " + meterIdentifier);
                continue;
            }
            consumerMeterMapping.setMeterMaster(meterMaster);*/

            meterMaster = consumerMeterMapping.getMeterMaster();
            if(meterMaster != null){
                String meterCode = meterMaster.getCode();
                String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
                logger.info(methodName + "Checking Meter Code for CTT type meter");
                if (meterIdentifier != null && meterCode != null && meterCode.equals(MeterMasterInterface.CODE_TYPE_CTT)) {
                    meterCTRMappings = meterCTRMappingService.getByMeterIdentifierAndStatus(meterIdentifier,consumerMeterMapping.getMappingStatus());
                    consumerMeterMapping.setMeterCTRMappings(meterCTRMappings);
                }
            }
        }
        return consumerMeterMappings;
    }

    /**
     *
     * @param consumerNo
     * @return
     * @throws Exception
     */
    public BigDecimal getMFByConsumerNoForActiveMeter(String consumerNo) throws Exception{
        BigDecimal selectedMF = null;
        String methodName = " getMFByConsumerNoForActiveMeter() ";
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
    List<ConsumerMeterMappingInterface> consumerMeterMappings = getByConsumerNoAndMappingStatus(consumerNo,ConsumerMeterMappingInterface.STATUS_ACTIVE);
            if (consumerMeterMappings != null && consumerMeterMappings.size() == 1) {
                ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappings.get(0);
                if(consumerMeterMapping != null){
                    String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
                    MeterMasterInterface meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
                    if(meterMaster != null ) {
                        logger.info(methodName + "Meter Master found ");
                        String typeCode= meterMaster.getCode();
                        if(typeCode.equals(MeterMasterInterface.CODE_TYPE_CTT)) {
                            MeterCTRMappingInterface meterCTRMapping = meterCTRMappingService.getActiveMappingByMeterIdentifier(meterIdentifier);
                            if (meterCTRMapping != null) {
                                selectedMF = meterCTRMapping.getOverallMf();
                                logger.info(methodName + "MF selected from meterCTRMapping ");
                            } else {
                                logger.error(methodName + "Meter CTR mapping not found ");
                            }
                        }else{
                            if(typeCode.equals(MeterMasterInterface.CODE_TYPE_WCS) || typeCode.equals(MeterMasterInterface.CODE_TYPE_WCT)) {
                                selectedMF = meterMaster.getMf();
                                logger.info(methodName + "MF selected from MeterMaster ");
                            }else{
                                logger.error(methodName + "No Correct Type found for meter: "+meterMaster);
                                throw new Exception("No Correct Type found for meter");
                            }
                        }
                    }else{
                        logger.error(methodName + "Meter Master not found ");
                        throw new Exception("Meter Master not found ");
                    }
                }else {
                    logger.error(methodName + "Consumer Meter Mapping not found ");
                    throw new Exception("No active Meter present in given Consumer ");
                }
            }else{
                logger.error(methodName + "Consumer Meter Mapping not found");
                throw new Exception("Consumer Meter Mapping not found");
            }

        }else{
            logger.error(methodName + "consumer information not found ");
            throw new Exception("consumer information not found ");
        }
        return selectedMF;
    }

    /**
     *
     * param consumerNo
     * return
     */
    public MeterMasterInterface getActiveMeterByConsumerNo(String consumerNo) throws Exception {

        String methodName = " getActiveMeterForConsumer() ";
        MeterMasterInterface meterMaster = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            List<ConsumerMeterMappingInterface> consumerMeterMappings = getByConsumerNoAndMappingStatus(consumerNo,ConsumerMeterMappingInterface.STATUS_ACTIVE);
            if (consumerMeterMappings != null && consumerMeterMappings.size() == 1) {
                ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappings.get(0);
                if(consumerMeterMapping != null){
                    String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
                    meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
                    if(meterMaster != null ) {
                        logger.info(methodName + "Meter Master found ");
                    }else{
                        logger.error(methodName + "Meter Master not found for consumer No: "+consumerNo);
                        throw new Exception("Meter Master not found for consumer");
                    }
                }else {
                    logger.error(methodName + "mapping not found ");
                    throw new Exception("Consumer Meter Mapping not found with Active Status");
                }
            }else{
                logger.error(methodName + "single active mapping not found");
                throw new Exception("Single active Consumer Meter Mapping not Found");
            }
        }else{
            logger.error(methodName + "consumer given is null");
            throw new Exception("consumer given is null");
        }
        return meterMaster;
    }

    /**
     *
     * param consumerNo
     * return
     */
    public ConsumerMeterMappingInterface getActiveMappingByConsumerNo(String consumerNo) throws Exception{
        String methodName = " getActiveMappingByConsumerNo() ";
        ConsumerMeterMappingInterface consumerMeterMapping = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            List<ConsumerMeterMappingInterface> consumerMeterMappings = getByConsumerNoAndMappingStatus(consumerNo,ConsumerMeterMappingInterface.STATUS_ACTIVE);
            if (consumerMeterMappings != null && consumerMeterMappings.size() == 1) {
                consumerMeterMapping = consumerMeterMappings.get(0);
                if(consumerMeterMapping != null){
                    logger.info(methodName + "Meter mapping found ");
                }else{
                    logger.info(methodName + "Meter mapping not found for consumer No: "+consumerNo);
                }
            }else{
                logger.info(methodName + "single active mapping not found");
            }
        }else{
            logger.error(methodName + "consumer given is null ");
        }
        return consumerMeterMapping;
    }



    /**
     * Method to replace consumer meter mapping or meter ctr mapping as per passed PostMeterReplacement parameter<br><br>
     * param consumerNo
     * param postMeterReplacement
     * return
     */
    @Transactional(rollbackFor = Exception.class)
    public ReadMasterInterface insertMeterReplacement(String consumerNo,PostMeterReplacement postMeterReplacement) throws Exception {
        //ReadMaster currentRead = postMeterReplacement.getCurrentRead();
        String methodName = "insertMeterReplacement() : ";
        logger.info(methodName + "Got request in service method to replace meter or CTR ");
        ReadMasterInterface insertedReadMaster = null;
        if(consumerNo != null && postMeterReplacement != null){
            ConsumerMeterMappingInterface newConsumerMeterMapping = postMeterReplacement.getConsumerMeterMapping();
            MeterCTRMappingInterface newMeterCTRMapping = postMeterReplacement.getMeterCTRMapping();
            ReadMasterInterface inactiveFinalRead = GlobalResources.convertCustomReadMasterToReadMaster(postMeterReplacement.getFinalRead());
            ReadMasterInterface activeStartRead = GlobalResources.convertCustomReadMasterToReadMaster(postMeterReplacement.getStartRead());
            consumerNo = consumerNo.trim();
            if(newConsumerMeterMapping != null && newMeterCTRMapping != null){
                logger.info(methodName + "Got request to replace Meter and CTR both");
                insertedReadMaster = replaceMeterAndCTR(consumerNo,newConsumerMeterMapping,newMeterCTRMapping,inactiveFinalRead,activeStartRead);
            }else if (newConsumerMeterMapping != null) {
                logger.info(methodName + "Got request to replace only Meter");
                insertedReadMaster = replaceMeter(consumerNo,newConsumerMeterMapping,inactiveFinalRead,activeStartRead);
            }else if (newMeterCTRMapping != null) {
                logger.info(methodName + "Got request to replace only CTR "+newMeterCTRMapping);
                insertedReadMaster = replaceCTR(consumerNo,newMeterCTRMapping,inactiveFinalRead);
            }else {
                logger.error(methodName + "Unknown mappings found need to handle");
            }
        }else {
            logger.error(methodName+"consumer no given is null");
        }
        return insertedReadMaster;
    }

    @Transactional(rollbackFor = Exception.class)
    private ReadMasterInterface replaceMeterAndCTR(String consumerNo,ConsumerMeterMappingInterface newConsumerMeterMapping,MeterCTRMappingInterface newMeterCTRMapping,ReadMasterInterface inactiveFinalRead,ReadMasterInterface activeStartRead) throws Exception{
        String methodName = "replaceMeterAndCTR() : ";
        logger.info(methodName + "called");
        logger.info(methodName + "Got request to replace both Meter and CTR");
        ReadMasterInterface insertedReadMaster = null;
        String newMeterIdentifier = newConsumerMeterMapping.getMeterIdentifier();
        String newCTRIdentifier =  newMeterCTRMapping.getCtrIdentifier();
        MeterMasterInterface newMeterMaster = meterMasterService.getByIdentifier(newMeterIdentifier);
        if(newMeterMaster != null){
            logger.info(methodName +   "New Meter Identifier Exist in Meter Master "+newMeterMaster);
            List<ConsumerMeterMappingInterface> consumerMeterMappings =  getByIdentifierAndMappingStatus(newMeterIdentifier,ConsumerMeterMappingInterface.STATUS_ACTIVE);
            if(consumerMeterMappings != null &&consumerMeterMappings.size() == 0) {
                String newMeterCode = newMeterMaster.getCode();
                if (newMeterCode != null && newMeterCode.equals(MeterMasterInterface.CODE_TYPE_CTT)) {
                    CTRMasterInterface ctrMaster = ctrMasterService.getByIdentifier(newCTRIdentifier);
                    if (ctrMaster != null) {
                        logger.info(methodName + "CTRIdentifier exist in CTR Master");

                        logger.info(methodName + "fetching active meter mapping of consumer");
                        ConsumerMeterMappingInterface presentActiveConsumerMeterMapping = getActiveMappingByConsumerNo(consumerNo);
                        if (presentActiveConsumerMeterMapping != null) {
                            logger.info(methodName + "fetched active meter mapping of consumer");
                            logger.info(methodName + "Calling Schedule Service to get Latest Schedule");
                            ScheduleInterface schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
                            String billMonth = schedule.getBillMonth();
                            String updatedBillMonth = GlobalResources.getNextMonth(billMonth);

                            logger.info(methodName + "updating active meter mapping of consumer setting status to inactive");
                            presentActiveConsumerMeterMapping.setFinalRead(newConsumerMeterMapping.getFinalRead());
                            presentActiveConsumerMeterMapping.setMappingStatus(ConsumerMeterMappingInterface.STATUS_INACTIVE);
                            presentActiveConsumerMeterMapping.setRemovalDate(newConsumerMeterMapping.getRemovalDate());
                            presentActiveConsumerMeterMapping.setRemovalBillMonth(updatedBillMonth);
                            presentActiveConsumerMeterMapping.setRemark(newConsumerMeterMapping.getInactiveRemark());
                            ConsumerMeterMappingInterface presentInactiveConsumerMeterMapping = update(presentActiveConsumerMeterMapping);
                            logger.info(methodName + "updated active meter mapping of consumer setting status to inactive");

                            String oldActiveMeterIdentifier = presentActiveConsumerMeterMapping.getMeterIdentifier();
                            if (presentInactiveConsumerMeterMapping != null && oldActiveMeterIdentifier != null) {
                                logger.info(methodName + "Present Active Consumer Meter Mapping Updated to Inactive");
                                logger.info(methodName + "Inserting new Active Consumer Meter Mapping");
                                newConsumerMeterMapping.setInstallationBillMonth(updatedBillMonth);
                                newConsumerMeterMapping.setRemovalDate(null);
                                newConsumerMeterMapping.setFinalRead(null);
                                ConsumerMeterMappingInterface newActiveConsumerMeterMapping = insert(newConsumerMeterMapping);
                                if (newActiveConsumerMeterMapping != null) {
                                    logger.info(methodName + "Inserted new Active Consumer Meter Mapping");

                                    logger.info(methodName + "Fetching Old Active Meter CTR Mapping with old meter identifier as " + oldActiveMeterIdentifier);
                                    MeterCTRMappingInterface presentActiveMeterCTRMapping = meterCTRMappingService.getActiveMappingByMeterIdentifier(oldActiveMeterIdentifier);
                                    List<MeterCTRMappingInterface> meterCTRMappings = meterCTRMappingService.getByCTRIdentifierAndMappingStatus(newCTRIdentifier, ConsumerMeterMappingInterface.STATUS_ACTIVE);
                                    logger.info(methodName+"fetching  meter ctr mapping with new ctr identifier");
                                    if(meterCTRMappings != null &&meterCTRMappings.size() > 0) {
                                        logger.info(methodName+"fetched  meter ctr mapping with new ctr identifier");
                                        MeterCTRMappingInterface meterCTRMapping = meterCTRMappings.get(0);
                                        if(meterCTRMapping.getMeterIdentifier().equals(presentActiveConsumerMeterMapping.getMeterIdentifier())){
                                            logger.info(methodName+"proceeding for replacing only meter with old CTR details");
                                        }else {
                                            logger.error(methodName + "with New CTR Identifier active mapping found ,unable to  install CTR ");
                                            throw new Exception("with New CTR Identifier active mapping found ");
                                        }
                                    }
                                    logger.info(methodName+"No meter ctr mapping with new ctr identifier exist ");
                                    BigDecimal oldOverallMf = BigDecimal.ONE;
                                    if (presentActiveMeterCTRMapping != null) {
                                        logger.info(methodName + "Fetched Old Active Meter CTR Mapping with old meter identifier as " + oldActiveMeterIdentifier);
                                        logger.info(methodName + "Updating old active meter ctr mapping with inactive status");
                                        presentActiveMeterCTRMapping.setMappingStatus(MeterCTRMappingInterface.STATUS_INACTIVE);
                                        MeterCTRMappingInterface presentInactiveMeterCTRMapping = meterCTRMappingService.update(presentActiveMeterCTRMapping);
                                        if (presentInactiveMeterCTRMapping != null) {
                                            logger.info(methodName + "Updated old active meter ctr mapping to inactive status");
                                            oldOverallMf = presentInactiveMeterCTRMapping.getOverallMf();
                                            logger.info(methodName + "Inserting new active meter ctr mapping");
                                        }
                                    }
                                    MeterCTRMappingInterface newActiveMeterCTRMapping = null;
                                    newActiveMeterCTRMapping = meterCTRMappingService.insert(newMeterCTRMapping);
                                    if (newActiveMeterCTRMapping != null) {
                                        logger.info(methodName + "Inserted new active meter ctr mapping. Inserting Final Read and Start Read");
                                        insertedReadMaster = readMasterService.insertMeterReplacementRead(consumerNo, inactiveFinalRead, activeStartRead, oldOverallMf);
                                        if (insertedReadMaster != null) {
                                            logger.info(methodName + "Meter And CTR Replaced Successfully along with final read and start read");
                                        } else {
                                            logger.error(methodName + "Unable to insert FinalRead or StartRead corresponding to meter/ctr replacement. Aborting replacement");
                                            throw new Exception("Unable to insert FinalRead or StartRead corresponding to meter/ctr replacement. Aborting replacement");
                                        }
                                    } else {
                                        logger.error(methodName + "Unable to insert new meter ctr mapping. Aborting replacement");
                                        throw new Exception("Unable to insert new meter ctr mapping. Aborting replacement");
                                    }
                                } else {
                                    logger.error(methodName + "Unable to insert new active ConsumerMeterMapping. Aborting replacement");
                                    throw new Exception("Unable to insert new active ConsumerMeterMapping. Aborting replacement");
                                }
                            } else {
                                logger.error(methodName + "Unable to update present Consumer Meter mapping to inactive");
                                if (oldActiveMeterIdentifier == null) {
                                    logger.error(methodName + "Old Active Meter Identifier is null.Aborting replacement");
                                    throw new Exception("throw new Exception(Old Active Meter Identifier is null.Aborting replacement");
                                }
                            }
                        } else {
                            logger.error(methodName + "No Active Consumer Meter Mapping found. Aborting Replacement");
                            throw new Exception("No Active Consumer Meter Mapping found.");
                        }
                    } else {
                        logger.error(methodName + "CTRIdentifier doesn't exist in CTR Master for newCTRIdentifier " + newCTRIdentifier);
                        throw new Exception("CTRIdentifier doesn't exist in CTR Master");
                    }
                } else {
                    logger.error(methodName + "Meter code is null or not CTT in MeterMaster, unable to install CTR Type Meter");
                    throw new Exception("Meter code is null or not CTT in MeterMaster, unable to install CTR Type Meter");
                }
            }else {
                logger.error(methodName + "with New Meter Identifier active mapping found ,unable to  install Meter ");
                throw new Exception("with New Meter Identifier active mapping found ,unable to  install Meter");
            }
        }else {
            logger.error(methodName + "For NewMeterIdentifier MeterMaster does not exists" + newMeterIdentifier);
            throw new Exception("For NewMeterIdentifier MeterMaster does not exists");
        }
        return insertedReadMaster;
    }

    @Transactional(rollbackFor = Exception.class)
    private ReadMasterInterface replaceMeter(String consumerNo,ConsumerMeterMappingInterface newConsumerMeterMapping,ReadMasterInterface inactiveFinalRead,ReadMasterInterface activeStartRead) throws Exception{
        String methodName = "replaceMeter() : ";
        logger.info(methodName + "called");
        ReadMasterInterface insertedReadMaster = null;
        MeterCTRMappingInterface oldMeterCTRMapping = null;
        String newMeterIdentifier = newConsumerMeterMapping.getMeterIdentifier();
        MeterMasterInterface newMeterMaster = meterMasterService.getByIdentifier(newMeterIdentifier);
        if(newMeterMaster != null && !newMeterMaster.getCode().equals(MeterMasterInterface.CODE_TYPE_CTT)){
            logger.info(methodName + "NewMeterMaster exists for newMeterIdentifier " + newMeterIdentifier);
            List<ConsumerMeterMappingInterface> consumerMeterMappings =  getByIdentifierAndMappingStatus(newMeterIdentifier,ConsumerMeterMappingInterface.STATUS_ACTIVE);
            if(consumerMeterMappings != null &&consumerMeterMappings.size() == 0) {
                logger.info(methodName + "Fetching present active consumer meter mapping ");
                ConsumerMeterMappingInterface presentActiveConsumerMeterMapping = getActiveMappingByConsumerNo(consumerNo);
                if (presentActiveConsumerMeterMapping != null) {
                    logger.info(methodName + "Fetched present active consumer meter mapping ");
                    logger.info(methodName + "Calling Schedule Service to get Latest Schedule");
                    ScheduleInterface schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
                    String billMonth = schedule.getBillMonth();
                    String updatedBillMonth = GlobalResources.getNextMonth(billMonth);
                    logger.info(methodName + "Updating present active consumer meter mapping ");
                    presentActiveConsumerMeterMapping.setFinalRead(newConsumerMeterMapping.getFinalRead());
                    presentActiveConsumerMeterMapping.setMappingStatus(ConsumerMeterMappingInterface.STATUS_INACTIVE);
                    presentActiveConsumerMeterMapping.setRemovalDate(newConsumerMeterMapping.getRemovalDate());
                    presentActiveConsumerMeterMapping.setRemovalBillMonth(updatedBillMonth);
                    presentActiveConsumerMeterMapping.setRemark(newConsumerMeterMapping.getInactiveRemark());
                    ConsumerMeterMappingInterface presentInactiveConsumerMeterMapping = update(presentActiveConsumerMeterMapping);
                    if (presentInactiveConsumerMeterMapping != null) {
                        logger.info(methodName + "Updated present active consumer meter mapping ");
                        logger.info(methodName + "Inserting new active consumer meter mapping");
                        newConsumerMeterMapping.setFinalRead(null);
                        newConsumerMeterMapping.setInstallationBillMonth(updatedBillMonth);
                        newConsumerMeterMapping.setRemovalDate(null);
                        ConsumerMeterMappingInterface activeConsumerMeterMapping = insert(newConsumerMeterMapping);
                        if (activeConsumerMeterMapping != null) {
                            logger.info(methodName + "Inserted new active consumer meter mapping");
                            String oldMeterIdentifier = presentInactiveConsumerMeterMapping.getMeterIdentifier();
                            MeterMasterInterface oldMeterMaster = meterMasterService.getByIdentifier(oldMeterIdentifier);
                            if(oldMeterMaster != null && oldMeterMaster.getCode().equals(MeterMasterInterface.CODE_TYPE_CTT)){
                                oldMeterCTRMapping = meterCTRMappingService.getActiveMappingByMeterIdentifier(oldMeterMaster.getIdentifier());
                                 if(oldMeterCTRMapping == null) {
                                     logger.info(methodName+"some error in fetching in  meter ctr mapping");
                                     throw new Exception("some error in updating in meter ctr mapping");
                                 }
                                oldMeterCTRMapping.setMappingStatus(MeterCTRMappingInterface.STATUS_INACTIVE);
                                 MeterCTRMappingInterface inactiveOldMeterCTRMapping = meterCTRMappingService.update(oldMeterCTRMapping);
                                     if(inactiveOldMeterCTRMapping == null){
                                         logger.info(methodName+"some error in updating in meter ctr mapping");
                                         throw new Exception("some error in updating in meter ctr mapping");
                                     }
                                 logger.info(methodName+"updated meter ctr mapping to inactive");
                            }
                            logger.info(methodName + "Fetching Old Mf for inserting Final and Start Reading ");
                            MeterMasterInterface oldInactiveMeterMaster = meterMasterService.getByIdentifier(presentInactiveConsumerMeterMapping.getMeterIdentifier());
                            if (oldInactiveMeterMaster != null) {
                                BigDecimal oldOverallMf = oldInactiveMeterMaster.getMf();
                                if(oldInactiveMeterMaster.getCode().equals(MeterMasterInterface.CODE_TYPE_CTT) && oldMeterCTRMapping != null){
                                    oldOverallMf = oldMeterCTRMapping.getOverallMf();
                                }
                                insertedReadMaster = readMasterService.insertMeterReplacementRead(consumerNo, inactiveFinalRead, activeStartRead,oldOverallMf);
                                if (insertedReadMaster != null) {
                                    logger.info(methodName + "Successfully inserted Final And Start read for ConsumerMeterMapping Replacement");
                                } else {
                                    logger.error(methodName + "Unable to insert Final and Start Read for ConsumerMeterMapping. Aborting Replacement");
                                    throw new Exception("Unable to insert Final and Start Read for ConsumerMeterMapping. Aborting Replacement");
                                }
                            } else {
                                logger.error(methodName + "Aborting Replacement since No Old Meter Master found for update consumer meter mapping meter identifier " + presentInactiveConsumerMeterMapping.getMeterIdentifier());
                                throw new Exception("Aborting Replacement since No Old Meter Master found for update consumer meter mapping meter identifier " + presentInactiveConsumerMeterMapping.getMeterIdentifier());
                            }
                        } else {
                            logger.error(methodName + "Unable to insert new ConsumerMeterMapping with active status. Aborting replacement");
                            throw new Exception("Unable to insert new ConsumerMeterMapping with active status. Aborting replacement");
                        }
                    } else {
                        logger.error(methodName + "Unable to update active ConsumerMeterMapping with inactive status. Aborting replacement");
                        throw new Exception("Unable to update active ConsumerMeterMapping with inactive status. Aborting replacement");
                    }
                } else {
                    logger.error(methodName + "no active consumer meter mapping found for consumer no " + consumerNo);
                }
            }else {
                logger.error(methodName + "with New Meter Identifier mapping found ,unable to  install Meter");
            }
        }else {
            if(newMeterMaster == null){
                logger.error(methodName + "Meter Master does not exists for newMeterIdentifier " + newMeterIdentifier);
            }else if(newMeterMaster.getCode().equals(MeterMasterInterface.CODE_TYPE_CTT)){
                logger.error(methodName + "Meter Master exists for newMeterIdentifier " + newMeterIdentifier
                        + " but metermaster type is " + MeterMasterInterface.CODE_TYPE_CTT + " but no new Meter CTR Mapping found");
            }
        }
        return insertedReadMaster;
    }

    @Transactional(rollbackFor = Exception.class)
    private ReadMasterInterface replaceCTR(String consumerNo,MeterCTRMappingInterface newMeterCTRMapping,ReadMasterInterface inactiveFinalRead) throws Exception{
        String methodName = "replaceCTR() : ";
        logger.info(methodName + "called");
        ReadMasterInterface insertedReadMaster = null;
        String meterIdentifier = newMeterCTRMapping.getMeterIdentifier();
        String newCTRIdentifier =  newMeterCTRMapping.getCtrIdentifier();
        CTRMasterInterface newCTRMaster = ctrMasterService.getByIdentifier(newCTRIdentifier);
        if (newCTRMaster != null) {
            logger.info(methodName + "CTRMaster exists for newCTRIdentifier " + newCTRIdentifier);
            List<MeterCTRMappingInterface> meterCTRMappings = meterCTRMappingService.getByCTRIdentifierAndMappingStatus(newCTRIdentifier, ConsumerMeterMappingInterface.STATUS_ACTIVE);
            if(meterCTRMappings != null && meterCTRMappings.size() == 0) {
                MeterMasterInterface meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
                if (meterMaster != null) {
                    String meterCode = meterMaster.getCode();
                    if (meterCode.equals(MeterMasterInterface.CODE_TYPE_CTT)) {
                        MeterCTRMappingInterface presentActiveMeterCTRMapping = meterCTRMappingService.getActiveMappingByMeterIdentifier(meterIdentifier);
                        if (presentActiveMeterCTRMapping != null) {
                            logger.info(methodName + "Present Active Meter Ctr Mapping Successfully fetched");
                            logger.info(methodName + "Updating Present Active Meter CTR Mapping");
                            presentActiveMeterCTRMapping.setMappingStatus(MeterCTRMappingInterface.STATUS_INACTIVE);
                            MeterCTRMappingInterface presentInactiveMeterCTRMapping = meterCTRMappingService.update(presentActiveMeterCTRMapping);
                            if (presentInactiveMeterCTRMapping != null) {
                                logger.info(methodName + "Updated Present Active Meter CTR Mapping");
                                BigDecimal oldOverallMf = presentInactiveMeterCTRMapping.getOverallMf();
                                logger.info(methodName + "Inserting new Active Meter CTR Mapping");
                                MeterCTRMappingInterface activeMeterCTRMapping = meterCTRMappingService.insert(newMeterCTRMapping);
                                if (activeMeterCTRMapping != null) {
                                    logger.info(methodName + "Inserted new Active Meter CTR Mapping");
                                    logger.info(methodName + "Inserting Final Read with oldOverallMf");
                                    insertedReadMaster = readMasterService.insertCTRReplacementRead(consumerNo, inactiveFinalRead, oldOverallMf);
                                    if (insertedReadMaster != null) {
                                        logger.info(methodName + "Inserted Final Read with oldOverallMf");
                                        logger.info(methodName + "Successfully Replaced CTRMapping");
                                    } else {
                                        logger.error(methodName + "Unable to insert Final Read in Read Master.Aborting Replacement");
                                        throw new Exception("Unable to insert Final Read in Read Master.Aborting Replacement");
                                    }
                                } else {
                                    logger.error(methodName + "Unable to insert new Active CTR Mapping.Aborting Replacement");
                                    throw new Exception("Unable to insert new Active CTR Mapping.Aborting Replacement");
                                }
                            } else {
                                logger.error(methodName + "Unable to update present Active ctr mapping with inactive status.Aborting Replacement");
                                throw new Exception("Unable to update present Active ctr mapping with inactive status.Aborting Replacement");
                            }
                        } else {
                            logger.error(methodName + "No Active Meter CTR mapping found for meterIdentifier " + meterIdentifier
                                    + "Aborting Replacement");
                        }
                    } else {
                        logger.error(methodName + "MeterMaster code is not " + MeterMasterInterface.CODE_TYPE_CTT + " for meterIdentifer " + meterIdentifier
                                + "Aborting Replacement");
                    }
                } else {
                    logger.error(methodName + "MeterMaster not found for meterIdentifier " + meterIdentifier + " Aborting Replacement");
                }
            }else {
                logger.error(methodName + "with New CTR Identifier active   mapping found ,unable to  install CTR");
            }
        }else {
            logger.error(methodName + "CTRMaster not found for newCTRIdentifier " + newCTRIdentifier + " Aborting Replacement");
        }
        return insertedReadMaster;
    }

    @Transactional(rollbackFor = Exception.class)
    public ConsumerMeterMappingInterface InActivateMappingByConsumerNo(String consumerNo, ReadMasterInterface finalRead) throws Exception {
        String methodName = "InActivateMappingByConsumerNo() : ";
        logger.info(methodName + "called");
        ConsumerMeterMappingInterface presentConsumerMeterMapping;
        String loggedInUser = GlobalResources.getLoggedInUser();
        Date currentDate = GlobalResources.getCurrentDate();
        if (consumerNo == null) {
            logger.error(methodName + " read type configurator does not allows to add kwh reading,aborting request");
            throw new Exception("You cannot punch kwh reading for such class of consumer");
        }

        presentConsumerMeterMapping = getActiveMappingByConsumerNo(consumerNo);
        if(presentConsumerMeterMapping == null){
            logger.error(methodName + " Active Meter Mapping Not found ");
            throw new Exception("Active Meter Mapping Not found");
        }

        String meterIdentifier = presentConsumerMeterMapping.getMeterIdentifier();
        if(meterIdentifier == null){
            logger.error(methodName + " meterIdentifier string is null in mapping");
            throw new Exception(" meterIdentifier string is null in mapping");
        }

        MeterMasterInterface  meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
        if(meterMaster == null){
            logger.error(methodName + " Meter Not Found "+meterMaster);
            throw new Exception("Meter Not Found");
        }

        String meterCode = meterMaster.getCode();
        if(meterCode == null ){
            logger.error(methodName + " Meter code Not Found "+meterMaster);
            throw new Exception("Meter code Not Found");
        }

        if( meterCode.equals(MeterMasterInterface.CODE_TYPE_CTT)){
            MeterCTRMappingInterface inActiveMeterCTRMapping = meterCTRMappingService.inActivateMeterCTRMappingByMeterIdentifier(meterIdentifier);
            if (inActiveMeterCTRMapping == null) {
                logger.error(methodName + " removing MeterCTRMapping failed  "+inActiveMeterCTRMapping);
                throw new Exception("removing MeterCTRMapping failed");
            }
            logger.info(methodName + " MeterCTRMapping removed successfully  "+inActiveMeterCTRMapping);
        }

        logger.info(methodName + "Updating present active consumer meter mapping ");
        presentConsumerMeterMapping.setFinalRead(finalRead.getReading());
        presentConsumerMeterMapping.setMappingStatus(ConsumerMeterMappingInterface.STATUS_INACTIVE);
        presentConsumerMeterMapping.setRemovalDate(finalRead.getReadingDate());
        presentConsumerMeterMapping.setRemovalBillMonth(finalRead.getBillMonth());
        presentConsumerMeterMapping.setRemark(finalRead.getRemark());
        updateAuditDetails(presentConsumerMeterMapping);
        ConsumerMeterMappingInterface inActiveConsumerMeterMapping = consumerMeterMappingDAO.add(presentConsumerMeterMapping);
        if(inActiveConsumerMeterMapping == null){
            logger.error(methodName + " removing ConsumerMeterMapping failed  "+inActiveConsumerMeterMapping);
            throw new Exception("removing MeterCTRMapping failed");
        }
        logger.info(methodName + " removing ConsumerMeterMapping successfully  "+inActiveConsumerMeterMapping);
        return inActiveConsumerMeterMapping;
    }


    public ConsumerMeterMappingInterface insertActiveMappingByConsumerNo(String consumerNo, Read read) throws Exception {
        String methodName = "insertActiveMappingByConsumerNo(): ";
        logger.info(methodName + "clicked");
        ReadMasterInterface readMaster = null;
        if (consumerNo == null) {
            logger.error(methodName + "consumer no given is null");
            throw new Exception("consumer no given is null");
        }
        readMaster = read.getReadMaster();
        if(readMaster == null){
            logger.error(methodName + "read master  is null");
            throw new Exception("read master  is null");
        }
        String meterIdentifier = readMaster.getMeterIdentifier();
        if(meterIdentifier == null){
            logger.error(methodName + " meterIdentifier string is null in mapping");
            throw new Exception(" meterIdentifier string is null in mapping");
        }
        logger.info(methodName + "fetching meter details by given meter identifier :"+meterIdentifier);
        MeterMasterInterface  meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
        if(meterMaster == null){
            logger.error(methodName + " Meter Not Found "+meterMaster);
            throw new Exception("Meter Not Found");
        }
        logger.info(methodName + "fetched meter details");
        logger.info(methodName + "checking for meter identifier in consumer meter mapping already in use OR not ");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = getByIdentifierAndMappingStatus(consumerNo,ConsumerMeterMappingInterface.STATUS_ACTIVE);
        if(consumerMeterMappings != null && consumerMeterMappings.size() > 0) {
        logger.error(methodName + "meter identifier already used in other consumer in Active state ");
            throw new Exception("meter identifier already used in other consumer in Active state ");
        }
        logger.info(methodName + "meter identifier not in use ");
        String meterCode = meterMaster.getCode();
        if(meterCode == null ){
            logger.error(methodName + " Meter code Not Found "+meterMaster);
            throw new Exception("Meter code Not Found");
        }
        logger.info(methodName+ "fetching schedule");
        ScheduleInterface schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
        if(schedule == null){
            logger.error(methodName + "unable to fetch schedule");
            throw new Exception("unable to fetch schedule");
        }
        logger.info(methodName + "fetched schedule");
        if( meterCode.equals(MeterMasterInterface.CODE_TYPE_CTT)){
            logger.info(methodName + "meter is of CTT type ");
            String ctrIdentifier = read.getCtrIdentifier();
            logger.info(methodName + "checking for meter identifier already in use");
            MeterCTRMappingInterface existingMeterCTRMapping = meterCTRMappingService.getActiveMappingByMeterIdentifier(meterIdentifier);
            if(existingMeterCTRMapping != null){
                logger.error(methodName + "Meter identifier already in use in MeterCTRMapping");
                throw new Exception("Meter identifier already in use in MeterCTRMapping");
            }
            logger.info(methodName+ "Meter identifier  not in use in MeterCTRMapping");

            logger.info(methodName + "checking for CTR identifier already in use");
            List <MeterCTRMappingInterface> existingMeterCTRMappings = meterCTRMappingService.getByCTRIdentifierAndMappingStatus(ctrIdentifier,MeterCTRMappingInterface.STATUS_ACTIVE);
            if(existingMeterCTRMappings != null && existingMeterCTRMappings.size() > 0){
                logger.error(methodName + "CTR identifier already in use in MeterCTRMapping");
                throw new Exception("CTR identifier already in use in MeterCTRMapping");
            }
            logger.info(methodName + "CTR identifier  not in use in MeterCTRMapping");
            MeterCTRMappingInterface meterCTRMapping = new MeterCTRMapping();
            meterCTRMapping.setMeterIdentifier(meterIdentifier);
            meterCTRMapping.setCtrIdentifier(ctrIdentifier);
            meterCTRMapping.setOverallMf(readMaster.getMf());
            meterCTRMapping.setInstallationDate(readMaster.getReadingDate());
            meterCTRMapping.setMappingStatus(MeterCTRMappingInterface.STATUS_ACTIVE);
            logger.info(methodName+ "inserting MeterCtr mapping");
          MeterCTRMappingInterface insertedMeterCTRMapping = meterCTRMappingService.insert(meterCTRMapping);
           if (insertedMeterCTRMapping == null) {
                logger.error(methodName + " unable to insert MeterCTRMapping"+insertedMeterCTRMapping);
              throw new Exception("unable to insert MeterCTRMapping ");
            }
          logger.info(methodName + "inserted MeterCTRMapping"+insertedMeterCTRMapping);
        }

        String latestCompletedBillMonth = schedule.getBillMonth();
        String  updatedBillMonth  = GlobalResources.getNextMonth(latestCompletedBillMonth);
        logger.info(methodName + "setting variables in consumer meter mapping ");
        ConsumerMeterMappingInterface consumerMeterMapping = new ConsumerMeterMapping();
        consumerMeterMapping.setConsumerNo(consumerNo);
        consumerMeterMapping.setMeterIdentifier(meterIdentifier);
        consumerMeterMapping.setMeterIdentifier(meterMaster.getIdentifier());
        consumerMeterMapping.setMeterMake(meterMaster.getMake());
        consumerMeterMapping.setStartRead(readMaster.getReading());
        consumerMeterMapping.setMeterSerialNo(meterMaster.getSerialNo());
        consumerMeterMapping.setMappingStatus(ConsumerMeterMappingInterface.STATUS_ACTIVE);
        consumerMeterMapping.setInstallationDate(readMaster.getReadingDate());
        consumerMeterMapping.setInstallationBillMonth(updatedBillMonth);
        consumerMeterMapping.setRemark(readMaster.getRemark());
        setAuditDetails(consumerMeterMapping);
        logger.info(methodName+ "inserting consumer meter mapping");
        ConsumerMeterMappingInterface insertedConsumerMeterMapping = insert(consumerMeterMapping);
        if(insertedConsumerMeterMapping == null){
            logger.error(methodName + " unable to insert ConsumerMeterMapping ");
            throw new Exception("unable to insert ConsumerMeterMapping");
        }
        logger.info(methodName + " inserted ConsumerMeterMapping "+insertedConsumerMeterMapping);
        return insertedConsumerMeterMapping;
    }


    public ConsumerMeterMappingInterface insert(ConsumerMeterMappingInterface consumerMeterMapping)  {
        String methodName = " insert() :";
        ConsumerMeterMappingInterface insertedConsumerMeterMapping = null;
        logger.info(methodName + "called");
        if(consumerMeterMapping != null) {
            setAuditDetails(consumerMeterMapping);
            insertedConsumerMeterMapping = consumerMeterMappingDAO.add(consumerMeterMapping);
        }
        return insertedConsumerMeterMapping;
    }

    public ConsumerMeterMappingInterface update(ConsumerMeterMappingInterface consumerMeterMapping)  {
        String methodName = " update() :";
        ConsumerMeterMappingInterface updatedConsumerMeterMapping = null;
        logger.info(methodName + "called");
        if(consumerMeterMapping != null) {
            updateAuditDetails(consumerMeterMapping);
            updatedConsumerMeterMapping = consumerMeterMappingDAO.add(consumerMeterMapping);
        }
        return updatedConsumerMeterMapping;
    }

    private  void setAuditDetails(ConsumerMeterMappingInterface consumerMeterMapping){
        String methodName = "setAuditDetails()  :";
        if(consumerMeterMapping != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            consumerMeterMapping.setUpdatedOn(date);
            consumerMeterMapping.setUpdatedBy(user);
            consumerMeterMapping.setCreatedOn(date);
            consumerMeterMapping.setCreatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    private void updateAuditDetails(ConsumerMeterMappingInterface consumerMeterMapping){
        String methodName = "updateAuditDetails()  :";
        if(consumerMeterMapping != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            consumerMeterMapping.setUpdatedOn(date);
            consumerMeterMapping.setUpdatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }
}
