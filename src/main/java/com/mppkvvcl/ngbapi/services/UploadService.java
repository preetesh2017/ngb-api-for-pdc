package com.mppkvvcl.ngbapi.services;


import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.factories.ReadMasterFactory;
import com.mppkvvcl.ngbapi.factories.ReadMasterKWFactory;
import com.mppkvvcl.ngbapi.factories.ReadMasterPFFactory;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Iterator;

@Service
public class UploadService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(UploadService.class);
    
    @Autowired
    private ReadMasterService readMasterService;
    
    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;
    
    @Autowired
    private UserDetailService userDetailService;

    public Workbook uploadReadMaster(Workbook workbook, ErrorMessage errorMessage) {
        String methodName = "uploadReadMaster() : ";
        logger.info(methodName + "called ");
        if (workbook == null) {
            errorMessage.setErrorMessage("given input file is null.");
            return null;
        }

        Sheet firstSheet = NGBAPIUtility.getWorkSheetFromWorkBookBySheetNo(workbook, NGBAPIUtility.FIRST_SHEET_INDEX);
        if (firstSheet == null) {
            errorMessage.setErrorMessage("no data at first sheet of workbook.");
            return null;
        }

        Iterator<Row> iterator = firstSheet.iterator();
        if (iterator == null) {
            logger.error(methodName + "unable to get iterator from first sheet.");
            errorMessage.setErrorMessage("given input file is null.");
            return null;
        }

        Row headerRow = iterator.next();
        if (headerRow == null) {
            errorMessage.setErrorMessage("unable to get header row form workbook.");
            return null;
        }

        CellStyle cellStyle = NGBAPIUtility.getCellStyle(workbook);
        if (cellStyle == null) {
            errorMessage.setErrorMessage("some error to set cell style");
            return null;
        }

        boolean isHeaderRowValid = NGBAPIUtility.isValidReadingHeaderRow(headerRow, cellStyle, errorMessage);
        if (!isHeaderRowValid) {
            logger.error(methodName + "some error in header row of workbook.");
            return null;
        }

        boolean isCellCountValid = NGBAPIUtility.checkCellCountInRow(headerRow, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT);
        if (!isCellCountValid) {
            errorMessage.setErrorMessage("file is invalid, column count mismatch.");
            return null;
        }

        logger.info(methodName + "creating new column remark");
        Cell remarkHeaderCell = headerRow.createCell(NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT);

        if (remarkHeaderCell == null) {
            logger.error(methodName + "some error to create remark column cell.");
            errorMessage.setErrorMessage("some internal error, please try after some time.");
            return null;
        }
        remarkHeaderCell.setCellValue(NGBAPIUtility.REMARK_COLUMN_NAME);

        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            ReadMasterInterface readMasterInterface = getReadMasterFromWorkBookRow(nextRow, cellStyle, errorMessage);
            if (readMasterInterface == null) {
                Cell newCell = nextRow.createCell(NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT);
                newCell.setCellValue(errorMessage.getErrorMessage());
                continue;
            }
            readMasterInterface = readMasterService.replaceOldServiceNoOneWithNewServiceNo(readMasterInterface);
            if(readMasterInterface == null){
                errorMessage.setErrorMessage("consumer no is invalid");
                NGBAPIUtility.getNewCell(nextRow, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT, errorMessage.getErrorMessage());
                continue;
            }
            readMasterInterface = readMasterService.setDifferenceWithLatestReadMaster(readMasterInterface ,errorMessage);
            if(readMasterInterface == null){
                NGBAPIUtility.getNewCell(nextRow, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT, errorMessage.getErrorMessage());
                continue;
            }

            readMasterInterface = validateUploadedReadMaster(readMasterInterface, errorMessage);
            if (readMasterInterface == null) {
                NGBAPIUtility.getNewCell(nextRow, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT, errorMessage.getErrorMessage());
                continue;
            }
            try {
                readMasterInterface.setSource(NGBAPIUtility.PMR_FILE);
                if (readMasterInterface.getAssessment().compareTo(BigDecimal.ZERO) > 0) {
                    readMasterInterface.setMeterStatus("DEFECTIVE"); //TODO create variable for defective type.
                    readMasterInterface.setReadingType(ReadMasterInterface.READING_TYPE_ASSESSMENT);
                }
                ReadMasterInterface insertedReadMasterInterface = readMasterService.insertReadMasterWithReadMasterKWAndReadMasterPF(readMasterInterface);
                if (insertedReadMasterInterface != null) {
                    NGBAPIUtility.getNewCell(nextRow, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT, NGBAPIUtility.UPLOAD_REMARK);
                }

            } catch (RuntimeException exception) {
                logger.error(methodName + "Received Runtime Exception with error message as :" + exception.getMessage());
                errorMessage.setErrorMessage(RuntimeExceptionHandler.handleException());
                NGBAPIUtility.getNewCell(nextRow, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT, errorMessage.getErrorMessage());

            } catch (Exception exception) {
                logger.error(methodName + "Received Exception with error message as :" + exception.getMessage());
                NGBAPIUtility.getNewCell(nextRow, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT, exception.getMessage());
            }
        }
        logger.info(methodName + "returning workbook");
        return workbook;
    }

    public ReadMasterInterface getReadMasterFromWorkBookRow(Row row, CellStyle cellStyle, ErrorMessage errorMessage) {
        String methodName = "getReadMasterFromWorkBookRow() : ";
        logger.info(methodName + "called ");
        if (row == null || cellStyle == null) {
            errorMessage.setErrorMessage("given row is null.");
            return null;
        }
        Iterator<Cell> cellIterator = row.cellIterator();
        if (cellIterator == null) {
            logger.error(methodName + "some error to get cell iterator from row.");
            errorMessage.setErrorMessage("some internal error, please try after some time.");
            return null;
        }
        boolean isCellCountValid = NGBAPIUtility.checkCellCountInRow(row, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT);

        if (!isCellCountValid) {
            logger.error(methodName + "column count mismatch");
            return null;
        }
        ReadMasterInterface readMasterInterface = ReadMasterFactory.build();
        ReadMasterKWInterface readMasterKW = ReadMasterKWFactory.build();
        ReadMasterPFInterface readMasterPF = ReadMasterPFFactory.build();
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if (cell == null) {
                cell.setCellStyle(cellStyle);
                continue;
            }

            int index = cell.getColumnIndex();
            CellType type = cell.getCellTypeEnum();
            switch (index) {
                case 0:   //for consumer no
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            readMasterInterface.setConsumerNo(cellValue);
                            break;
                        }
                    }
                    cell.setCellStyle(cellStyle);
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_FORMAT_MSG);
                    break;
                case 1:  //for reading date
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            readMasterInterface.setReadingDate(GlobalResources.getDateFromStringFormat(cellValue, NGBAPIUtility.DATE_FORMAT_DD_MMM_YYYY));
                            break;
                        }
                    }
                    cell.setCellStyle(cellStyle);
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_FORMAT_MSG);
                    break;
                case 2: // for reading type
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cellValue.equalsIgnoreCase(NGBAPIUtility.PMR_FILE_READING_TYPE_NRML)) {
                                readMasterInterface.setReadingType(ReadMasterInterface.READING_TYPE_NORMAL);
                                readMasterInterface.setMeterStatus(ReadMasterInterface.METER_STATUS_WORKING);
                                break;
                            } else if (cellValue.equalsIgnoreCase(NGBAPIUtility.PMR_FILE_READING_TYPE_PFL)) {
                                readMasterInterface.setReadingType(ReadMasterInterface.READING_TYPE_PFL);
                                readMasterInterface.setMeterStatus(ReadMasterInterface.METER_STATUS_WORKING);
                                break;
                            } else if (cellValue.equalsIgnoreCase(NGBAPIUtility.PMR_FILE_READING_TYPE_M_SD)) {
                                readMasterInterface.setReadingType(ReadMasterInterface.READING_TYPE_ASSESSMENT);
                                break;
                            } else if (cellValue.equalsIgnoreCase(NGBAPIUtility.PMR_FILE_READING_TYPE_METER_CHANGE)) {
                                readMasterInterface.setReadingType(ReadMasterInterface.READING_TYPE_NORMAL);
                                readMasterInterface.setMeterStatus(ReadMasterInterface.METER_STATUS_WORKING);
                                break;
                            }
                        }
                    }
                    cell.setCellStyle(cellStyle);
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_FORMAT_MSG);
                    break;
                case 3:  //for reading KWH
                    if (type.equals(CellType.NUMERIC)) {
                        double cellValue = cell.getNumericCellValue();
                        if (cellValue >= 0) {
                            readMasterInterface.setReading(new BigDecimal(cellValue));
                            break;
                        }
                    }
                    cell.setCellStyle(cellStyle);
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_FORMAT_MSG);
                    break;
                case 4:  //for MD(KW)
                    if (type.equals(CellType.NUMERIC)) {
                        double cellValue = cell.getNumericCellValue();
                        if (cellValue >= 0) {
                            readMasterKW.setMeterMD(new BigDecimal(cellValue));
                            readMasterInterface.setReadMasterKW(readMasterKW);
                            break;
                        }
                    }
                    cell.setCellStyle(cellStyle);
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_FORMAT_MSG);
                    break;
                case 8:  //for PF
                    if (type.equals(CellType.NUMERIC)) {
                        double cellValue = cell.getNumericCellValue();
                        if (cellValue >= 0) {
                            readMasterPF.setMeterPF(new BigDecimal(cellValue));
                            readMasterInterface.setReadMasterPF(readMasterPF);
                            break;
                        }
                    }
                    cell.setCellStyle(cellStyle);
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_FORMAT_MSG);
                    break;
                case 10:  //for assessment
                    if (type.equals(CellType.NUMERIC)) {
                        double cellValue = cell.getNumericCellValue();
                        if (cellValue >= 0) {
                            readMasterInterface.setAssessment(new BigDecimal(cellValue));
                            break;
                        }
                    }
                    cell.setCellStyle(cellStyle);
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_FORMAT_MSG);
                    break;
            }
        }
        return readMasterInterface;
    }

    public ReadMasterInterface validateUploadedReadMaster(ReadMasterInterface readToCheck, ErrorMessage errorMessage) {
        String methodName = "validateUploadedReadMaster() : ";
        logger.info(methodName + "called ");
        if (readToCheck == null || readToCheck.getConsumerNo() == null) {
            errorMessage.setErrorMessage("given input read master is null.");
            return null;
        }

        String consumerNo = readToCheck.getConsumerNo();
        logger.info(methodName + "consumer no " + consumerNo);
        ConsumerNoMasterInterface consumerNoMasterInterface = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMasterInterface == null || consumerNoMasterInterface.getStatus() == null) {
            errorMessage.setErrorMessage("consumer no is invalid");
            return null;
        }

        //below code for PDC check
        String status = consumerNoMasterInterface.getStatus();
        if (status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            errorMessage.setErrorMessage("consumer is inactive.");
            return null;
        }

        if (readToCheck.getReadingDate() == null) {
            errorMessage.setErrorMessage("reading date not found.");
            return null;
        }

        if (readToCheck.getReadingType() == null) {
            errorMessage.setErrorMessage("reading type is null");
            return null;
        }

        String readingType = readToCheck.getReadingType();
        if (readingType.equals(ReadMasterInterface.READING_TYPE_NORMAL) || readingType.equals(ReadMasterInterface.READING_TYPE_PFL) || readingType.equals(ReadMasterInterface.READING_TYPE_ASSESSMENT)) {
            if (readToCheck.getReading() == null) {
                errorMessage.setErrorMessage("reading not found.");
                return null;
            }

            if (readToCheck.getDifference() == null) {
                errorMessage.setErrorMessage("unable to get difference of previous and current reading.");
                return null;
            }

            BigDecimal difference = readToCheck.getDifference();
            if (difference.compareTo(BigDecimal.ZERO) < 0) {
                errorMessage.setErrorMessage("current read is less than previous read.");
                return null;
            }

            if (readToCheck.getAssessment() == null) {
                errorMessage.setErrorMessage("assesment should be either zero or assessed unit.");
                return null;
            }
        } else {
            errorMessage.setErrorMessage("invalid reading type");
            return null;
        }
        return readToCheck;
    }
}
