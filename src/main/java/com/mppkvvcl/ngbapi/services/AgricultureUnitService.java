package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AgricultureUnitInterface;
import com.mppkvvcl.ngbdao.daos.AgricultureUnitDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by MITHLESH on 14-07-2017.
 */
@Service
public class AgricultureUnitService {
    /**
     * getting whole application logger to perform log.
     * this logger using in AgricultureUnitService class.
     */
    Logger logger = GlobalResources.getLogger(AgricultureUnitService.class);
    /**
     * Asking spring to inject dependency on AgricultureUnitRepository
     * to perform various operation on repo.
     */
    @Autowired
    AgricultureUnitDAO agricultureUnitDAO;

    public AgricultureUnitInterface getBySubcategoryCodeAndBillMonth(long subcategoryCode,String billMonth){
        String methodName = " getBySubcategoryCodeAndBillMonth() : ";
        AgricultureUnitInterface agricultureUnit = null;
        logger.info(methodName + " calling repo to get Agriculture units ");
        agricultureUnit = agricultureUnitDAO.getBySubcategoryCodeAndBillMonth(subcategoryCode,billMonth);
        if(agricultureUnit != null){
            logger.info(methodName + "Successfully retreived agriculture unit object ");
        }else {
            logger.error(methodName + "Counldn't retreive agriculture unit . please check.");
        }
        return agricultureUnit;
    }

}
