package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerGovernmentMappingInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerGovernmentMappingDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class ConsumerGovernmentMappingService {
    /**
     * Asking Spring to inject ConsumerGovernmentMappingRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer government mapping  table
     * at the backend Database
     */
    @Autowired
    private ConsumerGovernmentMappingDAO consumerGovernmentMappingDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerGovernmentMappingService.class);

    /**
     * This method takes ConsumerGovernmentMapping and insert it into the backend database.
     * Return insertedConsumerGovernmentMapping if successful else return null.<br><br>
     * param consumerGovernmentMapping<br>
     * return insertedConsumerGovernmentMapping
     */
    public ConsumerGovernmentMappingInterface insert(ConsumerGovernmentMappingInterface consumerGovernmentMapping){
        String methodName = "insert() : " ;
        ConsumerGovernmentMappingInterface insertedConsumerGovernmentMapping = null;
        logger.info(methodName + "Started insertion for ConsumerGovernmentMapping ");
        if(consumerGovernmentMapping != null){
            logger.info(methodName + "Calling ConsumerGovernmentMappingRepository for inserting consumerGovernmentMapping");
            insertedConsumerGovernmentMapping = consumerGovernmentMappingDAO.add(consumerGovernmentMapping);
            if (insertedConsumerGovernmentMapping != null) {
                logger.info(methodName + "Successfully inserted consumerGovernmentMapping");
            }else{
                logger.error(methodName + "unable to insert consumer government mapping information row. Repository sending null");
            }
        }else{
            logger.error(methodName + "consumerGovernmentMapping received is null .");
        }
        return insertedConsumerGovernmentMapping ;
    }
}
