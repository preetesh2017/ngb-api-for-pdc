package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.ReadTypeConfiguratorDAO;
import com.mppkvvcl.ngbentity.beans.ReadTypeConfigurator;
import com.mppkvvcl.ngbinterface.interfaces.ReadTypeConfiguratorInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 7/3/2017.
 */
@Service
public class ReadTypeConfiguratorService {
    /**
     * Requesting spring to inject singleton ReadTypeConfiguratorRepository object.
     */
    @Autowired
    private ReadTypeConfiguratorDAO readTypeConfiguratorDAO;
    /**
     * Requesting spring to inject singleton  ConsumerConnectionInformationService object.
     */
    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    /**
     * Requesting spring to inject singleton TariffService object.
     */
    @Autowired
    private TariffService tariffService;

    @Autowired
    private  TariffDetailService tariffDetailService;
    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadTypeConfiguratorService.class);

    /**
     * This getByConsumerNo method  fetches ReadTypeConfigurator for consumerNo <br>
     * Return readTypeConfigurator object  if successful else return null.<br><br>
     * param consumerNo<br>
     * param tariffCode<br>
     * param subCategoryCode<br>
     * return readTypeConfigurator<br>
     */

    public ReadTypeConfiguratorInterface getByConsumerNo(String consumerNo){
        String methodName = "getByConsumerNo()";
        logger.info(methodName + "called for consumerNo: " + consumerNo);
        ReadTypeConfiguratorInterface readTypeConfigurator = null;
        TariffDetailInterface tariffDetail = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            tariffDetail = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
            if (tariffDetail != null ) {
                String tariffCode = tariffDetail.getTariffCode();
                long subCategoryCode = tariffDetail.getSubcategoryCode();
                if(tariffCode != null && subCategoryCode >= 0){
                    TariffInterface tariff = tariffService.getByTariffCode(tariffCode,null);
                    if (tariff != null) {
                        readTypeConfigurator = readTypeConfiguratorDAO.getByTariffIdAndSubcategoryCode(tariff.getId(), subCategoryCode);
                    }
                }
            }
        }
        return readTypeConfigurator;
    }

    /**
     * This getAll() method is used for getting all ReadTypeConfigurator objects in list<br>
     * Return successful if List found  otherwise return null.<br><br>
     * return List of ReadTypeConfigurator<br>
     */
    public List<? extends ReadTypeConfiguratorInterface> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends ReadTypeConfiguratorInterface> readTypeConfigurators = readTypeConfiguratorDAO.getAll();
        if (readTypeConfigurators != null){
            logger.info(methodName+"    ReadTypeConfigurator objects' list found successfully");
        }else{
            logger.error(methodName+"   No ReadTypeConfigurator List return Null");
        }
        return readTypeConfigurators;
    }

    /**
     * This insert() method is used for insert ReadTypeConfigurator object in<br>
     * respective table in backend through repo<br><br>
     * param readTypeConfigurator<br><br>
     * return insertReadTypeConfigurator<br>
     */
    public ReadTypeConfiguratorInterface insert(ReadTypeConfigurator readTypeConfigurator){
        String methodName = " insert() : ";
        logger.info(methodName+"    Starting to insert ReadTypeConfigurator Object:-"+readTypeConfigurator);
        ReadTypeConfiguratorInterface insertedReadTypeConfigurator =  null;
        if (readTypeConfigurator != null){
            logger.info(methodName+"    ReadTypeConfigurator object found in method");
            insertedReadTypeConfigurator = readTypeConfiguratorDAO.add(readTypeConfigurator);
        }
        return insertedReadTypeConfigurator;
    }

    public ReadTypeConfiguratorInterface getByTariffDetail(TariffDetailInterface tariffDetail){
        String methodName = "getByTariffDetail () :";
        logger.info(methodName + "called");
        ReadTypeConfiguratorInterface readTypeConfigurator = null;
        if (tariffDetail != null) {
            String tariffCode = tariffDetail.getTariffCode();
            long subCategoryCode = tariffDetail.getSubcategoryCode();
            if(tariffCode != null && subCategoryCode >= 0){
                TariffInterface tariff = tariffService.getByTariffCode(tariffCode,null);
                if (tariff != null) {
                    logger.info(methodName+ "tariff"+tariff +"and sub "+subCategoryCode);
                    readTypeConfigurator = readTypeConfiguratorDAO.getByTariffIdAndSubcategoryCode(tariff.getId(), subCategoryCode);
                }
            }
        }
        return readTypeConfigurator;
    }


}
