package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;
import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;
import com.mppkvvcl.ngbdao.daos.ZoneDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * recoded by nitish on 23-09-2017
 * Created by ANSHIKA on 17-07-2017.
 */
@Service
public class ZoneService {
    /**
     * Requesting spring to get singleton ZoneRepository object.
     */
    @Autowired
    private ZoneDAO zoneDAO;

    @Autowired
    private DivisionService divisionService;

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ZoneService.class);

    /**
     * code by nitish. Function returns Zone from passed locationCode
     * @param locationCode
     * @return
     */
    public ZoneInterface getByLocationCode(final String locationCode){
        final String methodName = "getByLocationCode() : ";
        logger.info(methodName + "called");
        ZoneInterface zone = null;
        if(locationCode != null){
            zone = zoneDAO.getByLocationCode(locationCode);
        }
        return zone;
    }

    public List<ZoneInterface> getByDivisionId(long divisionId){
        final String methodName = "getByDivisionId() : ";
        logger.info(methodName + "called");
        return zoneDAO.getByDivisionId(divisionId);
    }

    public List<ZoneInterface> getByCircleId(long circleId){
        final String methodName = "getByCircleId() : ";
        logger.info(methodName + "called");
        List<DivisionInterface> divisions = divisionService.getByCircleId(circleId);
        List<ZoneInterface> zones = new ArrayList<>();
        for(DivisionInterface division : divisions){
            if(division != null){
                List<ZoneInterface> divisionZones = getByDivisionId(division.getId());
                if(divisionZones != null){
                    zones.addAll(divisionZones);
                }
            }
        }
        return zones;
    }
}

