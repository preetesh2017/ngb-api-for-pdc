package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConfiguratorInterface;
import com.mppkvvcl.ngbdao.daos.ConfiguratorDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by PREETESH on 8/3/2017.
 */
@Service
public class ConfiguratorService {

    @Autowired
    private ConfiguratorDAO configuratorDAO;

    private static final Logger logger = GlobalResources.getLogger(ConfiguratorService.class);


    public long getValueForCode(String code) throws  Exception {
        String methodName = " getValueForCode(): ";
        long value;
        if (code != null) {
            ConfiguratorInterface configurator = configuratorDAO.getByCode(code);
            if(configurator != null){
                value = configurator.getValue();
            }else{
                logger.error(methodName + "configurator not found method.");
                throw new Exception("configurator not found");
            }

        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
            throw new Exception("consumerNo passed is null in method");
        }
        return value;
    }
}
