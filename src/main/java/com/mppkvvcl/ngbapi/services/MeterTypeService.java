package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterTypeInterface;
import com.mppkvvcl.ngbdao.daos.MeterTypeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 04-07-2017.
 */
@Service
public class MeterTypeService {

    /*
        Getting Logger object for logging in current class MeterTypeService from GlobalResources
     */
    private Logger logger = GlobalResources.getLogger(MeterTypeService.class);

    /**
     * for asking Spring to inject singleton object of MeterTypeRepository
     * so that we get various handles for performing CRUD operation on  table
     * at the backend Database
     */
    @Autowired
    private MeterTypeDAO meterTypeDAO;

    /**
     * This getById method is used for getting MeterType object for given Id
     * Response : if MeterType object found then return it otherwise return NULL<br><br>
     * param id<br>
     * return MeterType<br>
     */
    public MeterTypeInterface getById(Long id){
        String methodName = "getById() :";
        MeterTypeInterface meterType = null;
        logger.info(methodName+"    Starting to get MeterType object by Id:-"+id);
        if (id != 0){
            logger.info(methodName+"    MeterType Id :-"+id+" found successfully");
            meterType = meterTypeDAO.getById(id);
        }
        return meterType;
    }

    /**
     * This getAll method is used for getting all MeterType objects in list
     * Response : If MeterType objects list found then return it otherwise return null<br><br>
     * return List of MeterType<br>
     */
    public List<? extends MeterTypeInterface> getAll(){
        String methodName = "getAll() :";
        logger.info(methodName+"    String to get All MeterType objects");
        List<? extends MeterTypeInterface> meterTypes = meterTypeDAO.getAll();
        return meterTypes;
    }

    /**
     * This method takes meterCode and fetch the list of meterRent from backend.
     * Return list of meterType if successful else return null.
     * param meterPhase
     * return list of meterType
     */

    public List<MeterTypeInterface> getByMeterPhase(String meterPhase)
    {
        String methodName = "getByMeterPhase() : ";
        logger.info(methodName + "Got request to view list of meterType from MeterType table");
        List<MeterTypeInterface> meterTypes = null;
        if(meterPhase != null){
            logger.info(methodName + "Calling meterTypeDAO method to get list of meterType");
            meterTypes = meterTypeDAO.getByMeterPhase(meterPhase);
        }
        return meterTypes;
    }

}
