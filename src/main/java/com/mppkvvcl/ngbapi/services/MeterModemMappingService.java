package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterModemMappingInterface;
import com.mppkvvcl.ngbdao.daos.MeterModemMappingDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

/**
 * Created by Vikas on 7/7/2017.
 */

@Service
public class MeterModemMappingService {

    private static final Logger logger = GlobalResources.getLogger(MeterModemMappingService.class);

    @Autowired
    MeterModemMappingDAO meterModemMappingDAO;

    public List<MeterModemMappingInterface> getByMeterIdentifier(String meterIdentifier){
        String methodName = "getByMeterIdentifier() : ";
        List<MeterModemMappingInterface> meterModemMappings = null;
        logger.info(methodName+"Got request to find MeterModemMapping by meter identifier"+meterIdentifier);
        if(meterIdentifier != null){
            meterModemMappings = meterModemMappingDAO.getByMeterIdentifier(meterIdentifier);
            if(meterModemMappings != null && meterModemMappings.size() > 0){
                logger.info(methodName+"Successfully fetched record"+meterModemMappings);
            }else{
                logger.error(methodName+"Unable to fetch record ");
            }
        }else {
            logger.error(methodName+"got request as Null");
        }
        return meterModemMappings;
    }

    public MeterModemMappingInterface insertByMeterIdentifier(MeterModemMappingInterface meterModemMapping) {
        String methodName = "insertByMeterIdentifier() :";
        MeterModemMappingInterface insertedMeterModemMapping = null;
        List<MeterModemMappingInterface> presentMeterModemMappings = null;
        MeterModemMappingInterface presentMeterModemMapping = null;
        MeterModemMappingInterface presentMeterModemMappingUpdate = null;
        logger.info(methodName + "Got request to Insert MeterModemMapping");
        if (meterModemMapping != null) {
            String meterIdentifierPassed = meterModemMapping.getMeterIdentifier();
            if (meterIdentifierPassed != null) {
                logger.info(methodName + "Searching for present record in DB");
                presentMeterModemMappings = meterModemMappingDAO.getByMeterIdentifierAndStatus(meterIdentifierPassed, MeterModemMappingInterface.STATUS_ACTIVE);
                if (presentMeterModemMappings != null && presentMeterModemMappings.size() > 0) {
                    presentMeterModemMapping = presentMeterModemMappings.get(0);
                    presentMeterModemMapping.setStatus(MeterModemMappingInterface.STATUS_INACTIVE);
                    logger.info(methodName + "Updating status and end date of existing record for meter Identifier" + meterIdentifierPassed);
                    Date currentDate = new Date();
                    presentMeterModemMapping.setEndDate(currentDate);
                    presentMeterModemMappingUpdate = meterModemMappingDAO.update(presentMeterModemMapping);
                    if (presentMeterModemMappingUpdate != null) {
                        logger.info(methodName + " status and end date updated of existing record for meter Identifier" + meterIdentifierPassed);
                        insertedMeterModemMapping = meterModemMappingDAO.add(meterModemMapping);
                        if (insertedMeterModemMapping != null) {
                            logger.info(methodName + "Successfully inserted Record" + insertedMeterModemMapping);
                        } else {
                            logger.error(methodName + "Unable to insert Record");
                        }
                    }
                }else {
                    insertedMeterModemMapping = meterModemMappingDAO.add(meterModemMapping);
                    if (insertedMeterModemMapping != null) {
                        logger.info(methodName + "Successfully inserted Record" + insertedMeterModemMapping);
                    } else {
                        logger.error(methodName + "Unable to insert Record");
                    }
                }
            } else {
                logger.error(methodName + "Got request as Null");
            }
        }
        return insertedMeterModemMapping;
    }
}
