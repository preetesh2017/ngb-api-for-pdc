package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SurchargeInterface;
import com.mppkvvcl.ngbdao.daos.SurchargeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

@Service
public class SurchargeService {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(SurchargeService.class);

    /**
     *Asking Spring to inject SurchargeRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on sub_category table
     * at the backend Database
     */
    @Autowired
    private SurchargeDAO surchargeDAO;

    public SurchargeInterface getByTariffIdAndSubcategoryCodeAndOutstandingAmount(long tariffId, long subcategoryCode, BigDecimal outstandingAmount){
        String methodName = "getByTariffIdAndSubcategoryCodeAndOutstandingAmount() : ";
        List<SurchargeInterface> surcharges = null;
        SurchargeInterface minimumSurcharge = null;
        logger.info(methodName + "called with input tariffId " +tariffId + " And subcategory code " + subcategoryCode + " And outstanding Amount " +outstandingAmount);
        surcharges = surchargeDAO.getByTariffIdAndSubcategoryCodeAndOutstandingAmountGreaterThanEqual(tariffId,subcategoryCode,outstandingAmount);
        if(surcharges != null && surcharges.size() > 0){
            logger.info(methodName + "successfully fetched surcharge list with size as " + surcharges.size());
            logger.info(methodName + "executing to get single surcharge logic");
            minimumSurcharge = surcharges.stream()
                                         .min(Comparator.comparing(SurchargeInterface::getOutstandingAmount))
                                         .get();
        }else {
            logger.error(methodName + "Couldn't retrieve surcharge list due some internal error");
        }
        return minimumSurcharge;
    }

}
