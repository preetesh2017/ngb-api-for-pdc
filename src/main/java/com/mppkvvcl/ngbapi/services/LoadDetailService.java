package com.mppkvvcl.ngbapi.services;


import com.mppkvvcl.ngbapi.custombeans.CustomLoadDetail;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.LoadDetailDAO;
import com.mppkvvcl.ngbentity.beans.*;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class LoadDetailService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(LoadDetailService.class);

    /**
     * Asking Spring to inject LoadDetailRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on load detail mapping  table
     * at the backend Database
     */
    @Autowired
    private LoadDetailDAO loadDetailDAO;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;

    @Autowired
    private TariffDetailService tariffDetailService;

    @Autowired
    private TariffChangeDetailService tariffChangeDetailService;

    @Autowired
    private XrayConnectionInformationService xrayConnectionInformationService;

    @Autowired
    private BillService billService;

    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;

    @Autowired
    private MeterMasterService meterMasterService;

    @Autowired
    private TariffLoadMappingService tariffLoadMappingService;

    @Autowired
    private ConfiguratorService configuratorService;


    public LoadDetailInterface getLatestLoadDetail(String consumerNo){
        String methodName = "getLatestLoadDetail() : ";
        logger.info(methodName + "called " + consumerNo);
        LoadDetailInterface loadDetail = null;
        if(consumerNo != null) {
            loadDetail = loadDetailDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return loadDetail;
    }

    public List<LoadDetailInterface>  getByConsumerNo(String consumerNo){
        String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        List<LoadDetailInterface> loadDetails = null;
        if(consumerNo != null) {
            loadDetails = loadDetailDAO.getByConsumerNo(consumerNo);
        }
        return loadDetails;
    }


    @Transactional(rollbackFor = Exception.class)
    public CustomLoadDetail insertLoadChange(CustomLoadDetail customLoadDetailLoadToInsert, long subCategoryCodeToUpdate) throws Exception {
        String methodName = "insertLoadChange(): ";
        logger.info(methodName + "called");
        long daysSinceNSC = 0;
        LoadDetailInterface existingLoad = null;
        CustomLoadDetail insertedCustomLoadDetail = null;
        LoadDetailInterface insertedLoadDetail = null;
        LoadDetailInterface updatedExistingLoad = null;
        SubCategoryInterface existingSubCategory = null;
        SubCategoryInterface subCategoryToUpdate = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        Date loadToInsertEffectiveStartDate = null;
        TariffDetailInterface existingTariffDetail = null;
        LoadDetailInterface loadToInsert = null;
        BillInterface bill = null;
        XrayConnectionInformationInterface xrayConnectionInformation = null;
        Date currentDate = GlobalResources.getCurrentDate();
        loadToInsert = customLoadDetailLoadToInsert.getLoadDetail();
        if(loadToInsert != null){
            if (loadToInsert.getSanctionedLoad() == null || loadToInsert.getSanctionedLoad().doubleValue() <= 0) {
                //Above validation checks  for whether the mandatory load type that is sanctioned load is received or not
                logger.error(methodName + " Received load detail object has sanctioned load object null or value as less than zero");
                throw new Exception(" Sanctioned Load cannot be zero or negative ");
            } else if (loadToInsert.getConsumerNo() == null || loadToInsert.getConsumerNo().length() < 10) {
                logger.error(methodName + "Consumer No found null");
                throw new Exception("Invalid consumer number");
            }
            String consumerNo = loadToInsert.getConsumerNo();

            //Checking effective Start Date of the loadToInsert requested
            loadToInsertEffectiveStartDate = loadToInsert.getEffectiveStartDate();
            if (loadToInsertEffectiveStartDate == null) {
                logger.error(methodName + " Effective start date of the new load cannot be null ");
                throw new Exception(" Please choose effective date ");
            }

            //Checking whether already atleast one load exists or not for the consumer
            existingLoad = getLatestLoadDetail(consumerNo);
            if (existingLoad == null) {
                logger.error(methodName + " No load exist for this consumer, please check. ");
                throw new Exception("No Load detail exists for this consumer");
            }

            //If one load already exists we are restriciting the user for not changing the load again in this billing month
            if (existingLoad.getBillMonth() == null) {
                logger.error(methodName + " More than once load change not allowed in a bill month ");
                throw new Exception(" Load change allowed only once . ");
            }
            //Checking for whether the effective start date of loadToInsert is greater than the existingLoad effective start date
            if (loadToInsertEffectiveStartDate.before(existingLoad.getEffectiveStartDate())) {
                logger.error(methodName + " New Load date cannot be greater than existing Load date");
                throw new Exception(" Effective Date cannot be less than current load effective date");
            } else if (loadToInsertEffectiveStartDate.after(currentDate)) {
                logger.error(methodName + "New load effective date cannot be greater than current date ");
                throw new Exception("Load date cannot be greater than current Date");
            }
            logger.info(methodName + "fetching latest bill for consumer no :" + consumerNo);
            bill = billService.getLatestBillByConsumerNo(consumerNo);
            if (bill != null) {
                Date billDate = bill.getBillDate();

                // checking effective start date to insert of new load and bill date of latest bill.
                if (loadToInsertEffectiveStartDate.before(billDate)) {
                    logger.error(methodName + " Effective Date cannot be less than latest bill date");
                    throw new Exception("Effective Date cannot be less than latest bill date");
                }
            }
            existingTariffDetail = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
            if (existingTariffDetail == null) {
                logger.error(methodName + " Fetched tariff detail object is null ");
                throw new Exception("No tariff detail exist");
            }

            existingSubCategory = subCategoryService.getCurrentSubcategory(existingTariffDetail.getSubcategoryCode());
            if (existingSubCategory == null) {
                logger.error(methodName + "Fetched Sub Catgory object is null ");
                throw new Exception(" Consumer tariff category may be invalid ");
            }

            subCategoryToUpdate = subCategoryService.getCurrentSubcategory(subCategoryCodeToUpdate);
            if (subCategoryToUpdate == null) {
                logger.error(methodName + " No sub Category for the subCategoryCodeToUpdate " + subCategoryCodeToUpdate);
                throw new Exception(" No Tariff Category exists for such load ");
            }
            //Below validation is to check whether the contract demand is available or not if the tariff requires the same
            long endContractDemandKW = subCategoryToUpdate.getEndContractDemandKW().longValue();
            if (endContractDemandKW > 0) {
                if (loadToInsert.getContractDemand() == null || loadToInsert.getContractDemand().longValue() <= 0) {
                    logger.error(methodName + " Target SubCategory requires contract demand but load received doesn't has the same ");
                    throw new Exception(" Contract Demand is required in this tariff ");
                } else if (loadToInsert.getContractDemand().longValue() > endContractDemandKW) {
                    logger.error(methodName + " Contract demand cannot be greater than endContractDemandKW of target sub category ");
                    throw new Exception(" CD cannot be greater than  " + endContractDemandKW);
                } else if (loadToInsert.getContractDemand().longValue() > loadToInsert.getSanctionedLoad().longValue()) {
                    logger.error(methodName + " Contract demand found greater than sanctioned load ");
                    throw new Exception(" CD cannot be greater than sanctioned load");
                }

            }
            consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
            if (consumerConnectionInformation == null) {
                logger.error(methodName + " Fetched consumer connection information object is null ");
                throw new Exception("Consumer connection information not found");
            }

            //Below validation is to handle check of initial agreement period , this is to validate that new contract demand should not be less than 50% of the existing contract demand
            //Currently this is not being done this is just stub
            logger.info(methodName+ "checking for agreement period");
            daysSinceNSC = GlobalResources.getDateDiffInDays(consumerConnectionInformation.getConnectionDate(), loadToInsert.getEffectiveStartDate());
            if (loadToInsert.getContractDemand() != null && loadToInsert.getContractDemand().longValue() > 0 && loadToInsert.getPhase() != null && loadToInsert.getPhase().equals(ConsumerConnectionInformationInterface.PHASE_THREE)) {
                if (daysSinceNSC <= LoadDetailInterface.INITIAL_AGREEMENT_PERIOD_IN_DAYS) {
                    List<LoadDetailInterface> loadDetails = loadDetailDAO.getByConsumerNoAndContractDemandNotNullOrderByIdAsc(consumerNo);
                    logger.info(methodName + "load details fetched" + loadDetails + "load detail size" + loadDetails.size());
                    if (loadDetails != null && loadDetails.size() > 0){
                        LoadDetailInterface initialLoadDetail = loadDetails.get(0);
                        BigDecimal initialContractDemand = initialLoadDetail.getContractDemand();
                        int compareLatestCDWithInitialCD = loadToInsert.getContractDemand().compareTo(initialContractDemand);
                        logger.info(methodName + "compareLatestCDWithInitialCD:" + compareLatestCDWithInitialCD);
                        if (compareLatestCDWithInitialCD < 0 && loadDetails.size() > 1) {
                            for (int loadDetailCounter = 1; loadDetailCounter <= loadDetails.size(); loadDetailCounter++) {
                                LoadDetailInterface currentLoadDetail = loadDetails.get(loadDetailCounter);
                                int comparedContractDemand = currentLoadDetail.getContractDemand().compareTo(initialContractDemand);
                                logger.info(methodName + "comparedContractDemand" + comparedContractDemand);
                                if (comparedContractDemand < 0) {
                                    logger.error(methodName + "under agreement period, can't reduce contract demand twice ");
                                    throw new Exception("under agreement period, can't reduce contract demand twice");
                                }
                            }
                        }
                        // BigDecimal existingLoadContractDemand = existingLoad.getContractDemand();
                        if (initialContractDemand != null) {
                            BigDecimal validContractDemand = initialContractDemand.multiply(LoadDetailInterface.INITIAL_AGREEMENT_PERIOD_MULTIPLIER);
                            BigDecimal loadToInsertContractDemand = loadToInsert.getContractDemand();
                            //Checking whether new load contract demand is atleast greater than 50% of the existing load contract demand
                            if (loadToInsertContractDemand.longValue() < validContractDemand.longValue()) {
                                logger.error(methodName + " CD cannot be less than 50% of existing CD during initial period ");
                                throw new Exception(" Contract Demand cannot be less than 50% during initial agreement period ");
                            }
                        } else {
                            logger.info(methodName + " This may be case of sub category change and hence not raising any exception at this point ");
                        }
                    }
                }
            }

            //Below snippet assigns date values to end date
            Date existingLoadEffectiveEndDate = existingLoad.getEffectiveEndDate();
            if (existingLoadEffectiveEndDate == null) {
                logger.error(methodName + " The current load doesn't has end date ");
                throw new Exception(" Some internal error related to date of load ");
            }
            loadToInsert.setEffectiveEndDate(existingLoadEffectiveEndDate);
            existingLoadEffectiveEndDate = GlobalResources.addDaysInDate(-1, loadToInsertEffectiveStartDate);
            if (existingLoadEffectiveEndDate == null) {
                logger.error(methodName + " Couldn't retrieve loadToInsertEffectiveStartDate -1 from Global Resources function addDaysInDate() ");
                throw new Exception(" Some internal error. Please try again  ");
            }
            //setting current load end date to start date of target load minus 1
            existingLoad.setEffectiveEndDate(existingLoadEffectiveEndDate);

            //Saving existingLoad for updation and loadToInsert for new load entry
            updatedExistingLoad = update(existingLoad);
            insertedLoadDetail = insert(loadToInsert);

            if (updatedExistingLoad == null || insertedLoadDetail == null) {
                logger.error(methodName + " Some error in updation of current load or insertion of new load. ");
                throw new Exception(" Some error in inserting load details ");
            }

            insertedCustomLoadDetail = new CustomLoadDetail();
            insertedCustomLoadDetail.setLoadDetailInterface(insertedLoadDetail);

            //  Checking for whether tariff change or subcategory changed
            if (customLoadDetailLoadToInsert.getIsTariffChange() != null && customLoadDetailLoadToInsert.getIsTariffChange()) {
                logger.info(methodName + "tariff change done and inserted load detail");
                return insertedCustomLoadDetail;
            }
            logger.info(methodName + "preparing tariff-load-mapping to insert ");
            TariffLoadMappingInterface tariffLoadMapping = new TariffLoadMapping();
            tariffLoadMapping.setLoadChange(customLoadDetailLoadToInsert.getIsLoadChange());   // always true in load change
            tariffLoadMapping.setTariffChange(customLoadDetailLoadToInsert.getIsTariffChange());  // always false in load change
            tariffLoadMapping.setLoadDetailId(insertedLoadDetail.getId());


            //Checking for tariff row insertion required or not
            if (subCategoryToUpdate.getCode() != existingSubCategory.getCode()) {
                logger.info(methodName + "subcategory changed for consumer no " + consumerNo);

                TariffDetailInterface tariffToInsert = new TariffDetail();
                String billMonth = existingTariffDetail.getBillMonth();
                if (billMonth == null) {
                    logger.error(methodName + "subcategory changed required in given load details, but Tariff already changed");
                    throw new Exception("subcategory changed required in given load details, but Tariff already changed");
                }
                Date existingTariffEffectiveEndDate = existingTariffDetail.getEffectiveEndDate();
                if (existingTariffEffectiveEndDate == null) {
                    logger.error(methodName + " The current tariff doesn't has end date ");
                    throw new Exception(" Some internal error related to date of tariff ");
                }
                tariffToInsert.setEffectiveEndDate(existingTariffEffectiveEndDate);
                tariffToInsert.setEffectiveStartDate(loadToInsertEffectiveStartDate);
                existingTariffEffectiveEndDate = GlobalResources.addDaysInDate(-1, loadToInsertEffectiveStartDate);

                if (existingLoadEffectiveEndDate == null) {
                    logger.error(methodName + " Couldn't retrieve loadToInsertEffectiveStartDate -1 from Global Resources function addDaysInDate() ");
                    throw new Exception(" Some internal error. Please try again  ");
                }
                existingTariffDetail.setEffectiveEndDate(existingTariffEffectiveEndDate);
                tariffToInsert.setConsumerNo(existingTariffDetail.getConsumerNo());
                tariffToInsert.setTariffCode(existingTariffDetail.getTariffCode());
                tariffToInsert.setSubcategoryCode(subCategoryCodeToUpdate);

                TariffDetailInterface updatedExistingTariffDetail = tariffDetailService.update(existingTariffDetail);
                if (updatedExistingTariffDetail == null) {
                    logger.error(methodName + "some error to update existing tariff detail");
                    throw new Exception("some error to update existing tariff detail");
                }
                TariffDetailInterface insertedTariffDetail = tariffDetailService.insert(tariffToInsert);
                if (insertedTariffDetail == null) {
                    logger.error(methodName + "some error to insert new subcategory details in tariff");
                    throw new Exception("some error to insert new subcategory details in tariff");
                }
                tariffLoadMapping.setTariffDetailId(insertedTariffDetail.getId());
                TariffChangeDetailInterface existingTariffChangeDetail = tariffChangeDetailService.getByTariffDetailId(existingTariffDetail.getId());
                TariffChangeDetailInterface tariffChangeDetail = new TariffChangeDetail();
                if (existingTariffChangeDetail != null) {
                    logger.info(methodName + "tariff change detail exist");
                    tariffChangeDetail.setTariffDetailId(insertedTariffDetail.getId());
                    tariffChangeDetail.setMeteringStatus(existingTariffChangeDetail.getMeteringStatus());
                    tariffChangeDetail.setPurposeOfInstallation(existingTariffChangeDetail.getPurposeOfInstallation());
                    tariffChangeDetail.setPurposeOfInstallationId(existingTariffChangeDetail.getPurposeOfInstallationId());
                    tariffChangeDetail.setIsSeasonal(existingTariffChangeDetail.getIsSeasonal());
                    tariffChangeDetail.setIsXray(existingTariffChangeDetail.getIsXray());
                    TariffChangeDetailInterface insertedTariffChangeDetail = tariffChangeDetailService.insert(tariffChangeDetail);
                    if (insertedTariffChangeDetail == null) {
                        logger.error(methodName + "some error to insert tariff change detail");
                        throw new Exception("some error to insert tariff change detail");
                    }
                } else {

                    tariffChangeDetail.setTariffDetailId(insertedTariffDetail.getId());
                    tariffChangeDetail.setPurposeOfInstallationId(consumerConnectionInformation.getPurposeOfInstallationId());
                    tariffChangeDetail.setPurposeOfInstallation(consumerConnectionInformation.getPurposeOfInstallation());
                    tariffChangeDetail.setMeteringStatus(consumerConnectionInformation.getMeteringStatus());

                    if (consumerConnectionInformation.getIsXray()) {
                        tariffChangeDetail.setIsXray(true);
                    } else {
                        tariffChangeDetail.setIsXray(false);
                    }
                    if (consumerConnectionInformation.getIsSeasonal()) {
                        tariffChangeDetail.setIsSeasonal(true);
                    } else {
                        tariffChangeDetail.setIsSeasonal(false);
                    }
                    TariffChangeDetailInterface insertedTariffChangeDetail = tariffChangeDetailService.insert(tariffChangeDetail);
                    if (insertedTariffChangeDetail == null) {
                        logger.error(methodName + "some error to insert tariff change detail");
                        throw new Exception("some error to insert tariff change detail");
                    }
                }
            } else {
                logger.info(methodName + "subcategory not changed"+existingLoad);
                TariffLoadMappingInterface existingTariffLoadMapping = tariffLoadMappingService.getLatestTariffLoadMappingByLoadDetailId(existingLoad.getId());
                if (existingTariffLoadMapping == null) {
                    logger.error(methodName + "tariff load mapping not found ");
                    throw new Exception("tariff load mapping not found");
                }
                logger.info(methodName+"fetched tariff load mapping ");
                tariffLoadMapping.setTariffDetailId(existingTariffLoadMapping.getTariffDetailId());
            }
            TariffLoadMappingInterface insertedTariffLoadMapping = tariffLoadMappingService.insert(tariffLoadMapping);
            if (insertedTariffLoadMapping == null) {
                logger.info(methodName + "unable to insert tariff load mapping");
                throw new Exception("unable to insert tariff load mapping");
            }
            logger.info(methodName + "inserted tariff load mapping");
        }
        logger.info(methodName + "fetching xray details to insert");
        xrayConnectionInformation = customLoadDetailLoadToInsert.getXrayConnectionInformation();

        if (xrayConnectionInformation != null && xrayConnectionInformation.getConsumerNo() != null) {
            logger.info(methodName + "X-ray detail exist in custom load detail to insert");
            String consumerNo = xrayConnectionInformation.getConsumerNo();
            XrayConnectionInformationInterface insertedXrayConnectionInformation = null;
            BigDecimal xrayLoad = xrayConnectionInformation.getXrayLoad();

            TariffDetailInterface tariffDetailInterface = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
            if(tariffDetailInterface == null){
                logger.error(methodName+ "some error in fetching tariff detail");
                throw new Exception("some error in fetching tariff detail");
            }
            TariffChangeDetailInterface tariffChangeDetailInterface = tariffChangeDetailService.getByTariffDetailId(tariffDetailInterface.getId());
            if(tariffChangeDetailInterface != null){
                if(!tariffChangeDetailInterface.getIsXray()){
                    logger.error(methodName+ "Xray details not allowed for current tariff detail for given consumer :"+consumerNo);
                    throw new Exception("some internal error, please try after some time");
                }
            }else {
                ConsumerConnectionInformationInterface consumerConnectionInformationInterface = consumerConnectionInformationService.getByConsumerNo(consumerNo);
                if(consumerConnectionInformationInterface == null){
                    logger.error(methodName+ "some error in fetching consumer connection information ");
                    throw new Exception("some error in fetching consumer connection information ");
                }
                if(consumerConnectionInformation.getIsXray()){
                    logger.error(methodName+ "Xray details not allowed for current tariff detail for given consumer :"+consumerNo);
                    throw new Exception("some internal error, please try after some time");
                }
            }

            if(loadToInsert != null && insertedLoadDetail != null){
                logger.info(methodName + "load detail inserted");
                if(insertedLoadDetail.getContractDemand() != null && insertedLoadDetail.getContractDemand().longValue() > 0){
                    BigDecimal contractDemand = insertedLoadDetail.getContractDemand();
                    int compareXrayLoad = contractDemand.compareTo(xrayLoad);
                    logger.info(methodName+ "compareXrayLoad "+compareXrayLoad);
                    if(compareXrayLoad < 0){
                        logger.error(methodName+ "X RAY load should be less than Contract demand");
                        throw new Exception("X RAY load should be less than Contract demand");
                    }
                }else{
                    logger.info(methodName+ "load detail inserted, contract demand not found");
                    if(insertedLoadDetail.getSanctionedLoad() != null && insertedLoadDetail.getSanctionedLoad().longValue() > 0){
                        BigDecimal sanctionedLoad = insertedLoadDetail.getSanctionedLoad();
                        int compareXrayLoad = sanctionedLoad.compareTo(xrayLoad);
                        if(compareXrayLoad < 0){
                            logger.error(methodName+ "X RAY load should be less than Sanctioned load");
                            throw new Exception("X RAY load should be less than Sanctioned load");
                        }
                    }
                }

            }else {
                existingLoad = getLatestLoadDetail(xrayConnectionInformation.getConsumerNo());
                if(existingLoad != null ){
                    logger.info(methodName+ "fetched load detail");
                    if(existingLoad.getContractDemand() != null && existingLoad.getContractDemand().longValue() > 0){
                        BigDecimal contractDemand = existingLoad.getContractDemand();
                        int compareXrayLoad = contractDemand.compareTo(xrayLoad);
                        if(compareXrayLoad < 0){
                            logger.error(methodName+ "X RAY load should be less than Contract demand");
                            throw new Exception("X RAY load should be less than Contract demand");
                        }
                    }else {
                        logger.info(methodName+ "fetched load detail, contract demand not found");
                        if(existingLoad.getSanctionedLoad() != null && existingLoad.getSanctionedLoad().longValue() > 0){
                            BigDecimal sanctionedLoad = insertedLoadDetail.getSanctionedLoad();
                            int compareXrayLoad = sanctionedLoad.compareTo(xrayLoad);
                            if(compareXrayLoad < 0){
                                logger.error(methodName+ "X RAY load should be less than Sanctioned load");
                                throw new Exception("X RAY load should be less than Sanctioned load");
                            }
                        }
                    }
                }

            }
            List<XrayConnectionInformationInterface> xrayConnectionInformationInterfaces  = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo, XrayConnectionInformation.STATUS_ACTIVE);
            if(xrayConnectionInformationInterfaces == null){
                logger.error(methodName+ "some error in fetching xray details");
                throw new Exception("some error in fetching xray details");
            }
            XrayConnectionInformationInterface existingXrayConnectionInformationInterface = xrayConnectionInformationInterfaces.get(0);
            if(existingXrayConnectionInformationInterface == null){
                logger.error(methodName+ "some error in fetching xray details");
                throw new Exception("some error in fetching xray details");
            }
            existingXrayConnectionInformationInterface.setStatus(ConsumerMeterMapping.STATUS_INACTIVE);                       //use variable from X-Ray connection information Bean
            XrayConnectionInformationInterface updatedXrayConnectionInformation = xrayConnectionInformationService.update(existingXrayConnectionInformationInterface);
            if(updatedXrayConnectionInformation == null){
                logger.error(methodName+ "some error to update old xray details");
                throw new Exception("some error to update old xray details");
            }
            insertedXrayConnectionInformation = xrayConnectionInformationService.insert(xrayConnectionInformation);
            if (insertedXrayConnectionInformation == null) {
                logger.error(methodName + "unable to insert xray detail, aborting load change");
                throw new Exception("some error in insertion of  xray detail");
            }
            logger.info(methodName + "inserted x ray details");
            insertedCustomLoadDetail.setXrayConnectionInformationInterface(insertedXrayConnectionInformation);
        }
        return insertedCustomLoadDetail;
    }

    public CustomLoadDetail getCustomLoadDetailByConsumerNo(String consumerNo) throws Exception {
        String methodName = "getCustomLoadDetailByConsumerNo() : ";
        logger.info(methodName + "called ");
        ConsumerNoMasterInterface consumerNoMaster = null;
        LoadDetailInterface loadDetail = null;
        TariffDetailInterface tariffDetail = null;
        TariffChangeDetailInterface tariffChangeDetail = null;
        List<XrayConnectionInformationInterface> xrayConnectionInformationInterfaces = null;
        XrayConnectionInformationInterface xrayConnectionInformation = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        BillInterface bill = null;
        if (consumerNo == null) {
            logger.error(methodName + "Given Consumer No is Null "+consumerNo);
            throw new Exception("Given Consumer No is Null");
        }
        consumerNo = consumerNo.trim();
        consumerNoMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null) {
            logger.error(methodName + "Given consumer no doesn't exist in  ConsumerNoMaster :"+consumerNo);
            throw new Exception("Given consumer no doesn't exist in  ConsumerNoMaster");
        }
        logger.info(methodName + "fetched consumerNoMaster");
        String status = consumerNoMaster.getStatus();
        if (status == null) {
            logger.error(methodName + "status found null for given consumer no :" + consumerNo);
            throw new Exception("status found null for given consumer no ");
        }
        //Below validation Checks whether consumer is INACTIVE (PDC)
        if (status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            logger.error(methodName + "Consumer is INACTIVE consumer No:" + consumerNo);
            throw new Exception("Consumer is inactive");
        }
        loadDetail = getLatestLoadDetail(consumerNo);
        if (loadDetail == null){
            logger.error(methodName + "No Load details found for Consumer no:" + consumerNo);
            throw new Exception("No load details found for Consumer");
        }
        String billMonth = loadDetail.getBillMonth();
        if(billMonth == null){
            logger.error(methodName + "load already changed , one load change allowed in one billing month");
            throw new Exception("load already changed , one load change allowed in one billing month");
        }

        consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if (consumerConnectionInformation == null) {
            logger.error(methodName + "Consumer connection information not found for consumer no :" + consumerNo);
            throw new Exception("Consumer connection information not found for consumer ");
        }

        logger.info(methodName+"setting custom load detail ");
        CustomLoadDetail customLoadDetail  = new CustomLoadDetail();
        customLoadDetail.setLoadDetailInterface(loadDetail);
        customLoadDetail.setMinDateToLoadChange(loadDetail.getEffectiveStartDate());
        customLoadDetail.setIsLoadChange(false);


        List<LoadDetailInterface> allLoadDetails = loadDetailDAO.getByConsumerNo(consumerNo);
        long daysSinceNSC = GlobalResources.getDateDiffInDays(consumerConnectionInformation.getConnectionDate(),GlobalResources.getCurrentDate());
        long agreementPeriodInDays = configuratorService.getValueForCode(ConfiguratorInterface.AGREEMENT_PERIOD);
        if(allLoadDetails.size() > 1 && daysSinceNSC <= agreementPeriodInDays){
            customLoadDetail.setIsLoadChange(true);
        }

        BillInterface latestBill = billService.getLatestBillByConsumerNo(consumerNo);
        if(latestBill != null){
            logger.info(methodName+ "fetched latest bill,comparing dates");
            Date billDate = latestBill.getBillDate();
            if(billDate.after(loadDetail.getEffectiveStartDate())) {
                customLoadDetail.setMinDateToLoadChange(billDate);
            }
        }

        tariffDetail = tariffDetailService.getLatestTariffByConsumerNo(consumerNo);
        if (tariffDetail == null) {
            logger.error(methodName + "No tariff details found for Consumer no:" + consumerNo);
            throw new Exception("No tariff details found for Consumer");
        }
        customLoadDetail.setIsTariffChange(false);
        tariffChangeDetail = tariffChangeDetailService.getByTariffDetailId(tariffDetail.getId());
        if (tariffChangeDetail != null) {
            logger.info(methodName+ "tariff change detail exist for consumer no: "+consumerNo);
            customLoadDetail.setIsTariffChange(true);
            if(tariffChangeDetail.getIsXray()){
                logger.info(methodName + "X-RAY details exist");
                xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo,XrayConnectionInformationInterface.STATUS_ACTIVE);

                if(xrayConnectionInformationInterfaces != null && xrayConnectionInformationInterfaces.size() == 0 ){
                    logger.error(methodName + "some error in fetching Xray connection information");
                    throw new Exception("some error in fetching Xray connection information");
                }
                logger.info(methodName+ "fetched Xray details");
                xrayConnectionInformation = xrayConnectionInformationInterfaces.get(0);
                customLoadDetail.setXrayConnectionInformationInterface(xrayConnectionInformation);
            }

            logger.info(methodName + "checking for consumer metering status");
            if(tariffChangeDetail.getMeteringStatus().equals(ConsumerConnectionInformationInterface.METERING_STATUS_METERED)){
                ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappingService.getActiveMappingByConsumerNo(consumerNo);
                if(consumerMeterMapping == null){
                    logger.error(methodName+ "consumer meter mapping not found");
                    throw new Exception("some error in fetching  consumer meter mapping");
                }
                String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
                MeterMasterInterface meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
                if(meterMaster == null){
                    logger.error(methodName+ "meter master not found for meter identifier :"+meterIdentifier);
                    throw new Exception("some error in fetching consumer meter phase");
                }
                String meterPhase = meterMaster.getPhase();
                customLoadDetail.setMeterPhase(meterPhase);
            }
        }else {
            logger.info(methodName+ "tariff change detail not found");
            if(consumerConnectionInformation.getIsXray()){
                logger.info(methodName+ "X-RAY details exist");
                xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo,XrayConnectionInformationInterface.STATUS_ACTIVE);
                if(xrayConnectionInformationInterfaces == null ){
                    logger.error(methodName + "some error in fetching X-ray connection information");
                    throw new Exception("some error in fetching X-ray connection information");
                }
                logger.info(methodName+ "fetched Xray details");
                xrayConnectionInformation = xrayConnectionInformationInterfaces.get(0);
                customLoadDetail.setXrayConnectionInformationInterface(xrayConnectionInformation);
            }
            logger.info(methodName + "checking for consumer metering status");
            if(consumerConnectionInformation.getMeteringStatus().equals(ConsumerConnectionInformationInterface.METERING_STATUS_METERED)){
                ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappingService.getActiveMappingByConsumerNo(consumerNo);
                if(consumerMeterMapping == null){
                    logger.error(methodName+ "consumer meter mapping not found");
                    throw new Exception("consumer meter mapping not found");
                }
                String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
                MeterMasterInterface meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
                if(meterMaster == null){
                    logger.error(methodName+ "meter master not found for meter identifier :"+meterIdentifier);
                    throw new Exception("some internal error");
                }
                String meterPhase = meterMaster.getPhase();
                customLoadDetail.setMeterPhase(meterPhase);
            }
        }
        return customLoadDetail;
    }


    public LoadDetailInterface insert (LoadDetailInterface loadDetail) {
        String methodName = "insert() : ";
        LoadDetailInterface insertedLoadDetail1 = null;
        logger.info(methodName + "called ");
        if(loadDetail != null) {
            setAuditDetails(loadDetail);
            insertedLoadDetail1 = loadDetailDAO.add(loadDetail);
        }
        return insertedLoadDetail1;
    }

    public LoadDetailInterface update (LoadDetailInterface loadDetail) {
        String methodName = "update() : ";
        LoadDetailInterface updatedLoadDetail1 = null;
        logger.info(methodName + "called ");
        if(loadDetail != null) {
            setUpdatedDetails(loadDetail);
            updatedLoadDetail1 = loadDetailDAO.update(loadDetail);
        }
        return updatedLoadDetail1;
    }

    private void setAuditDetails(LoadDetailInterface loadDetail){
        String methodName = "setAuditDetails() : ";
        if(loadDetail != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            loadDetail.setUpdatedOn(date);
            loadDetail.setUpdatedBy(user);
            loadDetail.setCreatedOn(date);
            loadDetail.setCreatedBy(user);
        }
        else {
            logger.error(methodName + "given input is null");
        }
    }
    private  void setUpdatedDetails(LoadDetailInterface loadDetail ){
        String methodName = "setUpdatedDetails() : ";
        if(loadDetail != null) {
            loadDetail.setUpdatedOn(GlobalResources.getCurrentDate());
            loadDetail.setUpdatedBy(GlobalResources.getLoggedInUser());
        } else {
            logger.error(methodName + "given input is null");
        }
    }
}
