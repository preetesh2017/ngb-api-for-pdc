package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffChangeMappingInterface;
import com.mppkvvcl.ngbdao.daos.TariffChangeMappingDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by vikas on 9/20/2017.
 */

@Service
public class TariffChangeMappingService {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(TariffChangeMappingService.class);


    @Autowired
    private TariffChangeMappingDAO tariffChangeMappingDAO;

    public List<? extends TariffChangeMappingInterface> getByTariffCategory(String tariffCategory) {
        String methodName = " getByTariffCategory() ";
        logger.info(methodName + "called");
        List<? extends TariffChangeMappingInterface> tariffChangeMappings = null;
        if(tariffCategory != null) {
            tariffChangeMappings = tariffChangeMappingDAO.getByTariffCategory(tariffCategory);
        }
        return tariffChangeMappings;
    }

}
