package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.custombeans.CustomInstrumentDetail;
import com.mppkvvcl.ngbinterface.interfaces.*;
import com.mppkvvcl.ngbdao.daos.InstrumentDishonourDAO;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbentity.beans.Configurator;
import com.mppkvvcl.ngbentity.beans.InstrumentDishonour;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/31/2017.
 */
@Service
public class InstrumentDishonourService {
    private static final Logger logger = GlobalResources.getLogger(InstrumentDishonourService.class);

    @Autowired
    private InstrumentDishonourDAO instrumentDishonourDAO;

    @Autowired
    private ConfiguratorService configuratorService;

    @Autowired
    private InstrumentDetailService instrumentDetailService;

    @Autowired
    private AdjustmentService adjustmentService;

    public boolean getValidityByConsumerNoAndDate(String consumerNo, Date date) throws RuntimeException, Exception {
        String methodName = " getValidityByConsumerNoAndDate(): ";
        boolean validity = false;
        logger.info(methodName + " service to determine consumer's status to pay bill by cheque" + consumerNo);
        if ( consumerNo != null && date != null) {
            consumerNo = consumerNo.trim();
            InstrumentDishonourInterface instrumentDishonour = instrumentDishonourDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
            if(instrumentDishonour != null){
                Date endDate = instrumentDishonour.getEndDate();
                if(date.after(endDate) ){
                    validity = true;
                }else{
                    validity = false;
                }
            }else{
                validity = true;
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
        }
        return validity;
    }

    /**
     * Dishonour the Instrument by Id and provide remark
     *
     * @param id
     * @param remark
     * @return Dishonoured Instrument
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public List<InstrumentDishonourInterface> insertDishonour(final long id, final String remark) throws Exception {
        String methodName = "insertDishonour() : ";
        logger.info(methodName + "called");

        if (id <= 0l) {
            logger.error(methodName + "Got input parameter Null");
            throw new Exception("Got input parameter Null");
        }
        final List<InstrumentDishonourInterface> instruments = new ArrayList<>();
        final CustomInstrumentDetail customInstrumentDetail = instrumentDetailService.getById(id);
        if (customInstrumentDetail == null || customInstrumentDetail.getInstrumentDetails() == null || customInstrumentDetail.getInstrumentDetails().isEmpty()) {
            logger.error(methodName + "Instrument Id doesn't exist");
            throw new Exception("Instrument Id doesn't exist");
        }
        logger.info(methodName + "Fetched Instrument Details");
        final InstrumentDetailInterface instrumentDetail = customInstrumentDetail.getInstrumentDetails().get(0);
        final List<PaymentInterface> payments = customInstrumentDetail.getPayments();
        if (payments == null || payments.isEmpty()) {
            logger.error(methodName + "No payments found for Given Instrument No ");
            throw new Exception("No payments found for Given Instrument No ");
        }
        logger.info(methodName + "Fetched All payments related to given instrument No");
        final long rcdcCharges = configuratorService.getValueForCode(ConfiguratorInterface.RC_DC_CHARGES);
        final long bankCharge = configuratorService.getValueForCode(ConfiguratorInterface.BANK_CHARGES);
        final Long rcdcCode = configuratorService.getValueForCode(ConfiguratorInterface.RC_DC_CODE);
        final Long bankChargesCode = configuratorService.getValueForCode(ConfiguratorInterface.BANK_CHARGES_CODE);
        final Long prevArrearCode = configuratorService.getValueForCode(ConfiguratorInterface.PREV_ARREAR_CODE);

        final AdjustmentInterface bankChargesAdjustment = new Adjustment();
        final String consumerNo = payments.get(0).getConsumerNo();
        bankChargesAdjustment.setConsumerNo(consumerNo);
        bankChargesAdjustment.setAmount(new BigDecimal(bankCharge));
        bankChargesAdjustment.setPosted(Adjustment.POSTED_FALSE);
        bankChargesAdjustment.setDeleted(Adjustment.DELETED_FALSE);
        bankChargesAdjustment.setCode(bankChargesCode.intValue());
        bankChargesAdjustment.setRemark("Bank Charges");

        AdjustmentInterface insertedBankChargesAdjustment = adjustmentService.insert(bankChargesAdjustment);
        if (insertedBankChargesAdjustment == null) {
            logger.error(methodName + "Some error in insertion of BankChargesAdjustment");
            throw new Exception("Some error in insertion of BankChargesAdjustment");
        }
        logger.info(methodName + "Inserted Bank Charges Adjustment");

        final long notAcceptingChequeDays = configuratorService.getValueForCode(ConfiguratorInterface.NOT_ACCEPTING_CHEQUE_AFTER_DISHONOUR);
        Date currentDate = GlobalResources.getCurrentDate();
        currentDate = GlobalResources.addDaysInDate((int) notAcceptingChequeDays, currentDate);
        for (PaymentInterface payment : payments) {

            final AdjustmentInterface prevArrearAdjustment = new Adjustment();
            prevArrearAdjustment.setConsumerNo(payment.getConsumerNo());
            prevArrearAdjustment.setAmount(new BigDecimal(payment.getAmount()));
            prevArrearAdjustment.setPosted(Adjustment.POSTED_FALSE);
            prevArrearAdjustment.setDeleted(Adjustment.DELETED_FALSE);
            prevArrearAdjustment.setCode(prevArrearCode.intValue());
            prevArrearAdjustment.setRemark("Previous Arrear");
            final AdjustmentInterface insertedAdjustment = adjustmentService.insert(prevArrearAdjustment);
            if (insertedAdjustment == null) {
                logger.error(methodName + "Some error in insertion of Adjustment equal to Payment Amount");
                throw new Exception("Some error in insertion of Adjustment equal to Payment Amount");
            }
            logger.info(methodName + "Inserted Previous Arrear Adjustment equal to Payment Amount" + insertedAdjustment);

            final AdjustmentInterface rcDcAdjustment = new Adjustment();
            rcDcAdjustment.setConsumerNo(payment.getConsumerNo());
            rcDcAdjustment.setAmount(new BigDecimal(rcdcCharges));
            rcDcAdjustment.setPosted(Adjustment.POSTED_FALSE);
            rcDcAdjustment.setDeleted(Adjustment.DELETED_FALSE);
            rcDcAdjustment.setCode(rcdcCode.intValue());
            rcDcAdjustment.setRemark("RC-DC Charges");
            final AdjustmentInterface insertedRCDCAdjustment = adjustmentService.insert(rcDcAdjustment);
            if (insertedRCDCAdjustment == null) {
                logger.error(methodName + "Some error in insertion in of RC DC Adjustment");
                throw new Exception("Some error in insertion in of RC DC Adjustment");
            }
            logger.info(methodName + "Inserted RC DC Adjustment");

            // TODO: Adjustment For Cumulative Surcharge

            logger.info(methodName + "Preparing Instrument Dishonour to Insert");
            final InstrumentDishonourInterface instrumentDishonour = new InstrumentDishonour();
            instrumentDishonour.setConsumerNo(payment.getConsumerNo());
            instrumentDishonour.setInstrumentId(instrumentDetail.getId());
            instrumentDishonour.setEndDate(currentDate);
            instrumentDishonour.setRemark(remark);

            logger.info(methodName + "Inserting Instrument Dishonour ");
            final InstrumentDishonourInterface insertedInstrumentDishonour = insert(instrumentDishonour);
            if (insertedInstrumentDishonour == null) {
                logger.error(methodName + "Some Error in insertion of instrumentDishonour details ");
                throw new Exception("Some Error in insertion of instrumentDishonour details ");
            }
            instruments.add(insertedInstrumentDishonour);
            logger.info(methodName + "Inserted InstrumentDishonour" + instrumentDishonour);
        }

        return instruments;
    }

    /**
     * Insert the Dishonoured Instrument detail
     *
     * @param instrumentDishonour
     * @return
     */
    public InstrumentDishonourInterface insert(final InstrumentDishonourInterface instrumentDishonour) {
        final String methodName = "insert() : ";
        logger.info(methodName + "called");
        InstrumentDishonourInterface instrumentDetail = null;
        if (instrumentDishonour != null) {
            setAuditDetails(instrumentDishonour);
            instrumentDetail = instrumentDishonourDAO.add(instrumentDishonour);
        }
        return instrumentDetail;
    }

    private void setAuditDetails(InstrumentDishonourInterface instrumentDishonour) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (instrumentDishonour != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            instrumentDishonour.setUpdatedOn(date);
            instrumentDishonour.setUpdatedBy(user);
            instrumentDishonour.setCreatedOn(date);
            instrumentDishonour.setCreatedBy(user);
        }
    }

    private void setUpdatedDetails(InstrumentDishonourInterface instrumentDishonour) {
        String methodName = "setUpdatedDetails() : ";
        logger.info(methodName + "called");
        if (instrumentDishonour != null) {
            instrumentDishonour.setUpdatedOn(GlobalResources.getCurrentDate());
            instrumentDishonour.setUpdatedBy(GlobalResources.getLoggedInUser());
        }
    }
}
