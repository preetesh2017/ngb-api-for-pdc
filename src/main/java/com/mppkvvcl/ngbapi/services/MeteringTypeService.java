package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeteringTypeInterface;
import com.mppkvvcl.ngbdao.daos.MeteringTypeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */
@Service
public class MeteringTypeService {

    /**
     * Getting logger for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(MeteringTypeService.class);

    /**
     * Asking spring to inject MeteringTypeRepository to that we can use it.
     */
    @Autowired
    private MeteringTypeDAO meteringTypeDAO;

    /**
     * This getAll() method is used for getting All MeteringType objects.<br><br>
     * return meteringTypes
     */
    public List<? extends MeteringTypeInterface> getAll(){
        String methodName = "getAll() :";
        logger.info(methodName + "called");
        List<? extends MeteringTypeInterface> meteringTypes = meteringTypeDAO.getAll();
        return meteringTypes;
    }
}
