package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbinterface.interfaces.SECircleMappingInterface;
import com.mppkvvcl.ngbdao.daos.SECircleMappingDAO;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * code by nitish on 23-09-2017
 */
@Service
public class SECircleMappingService {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(SECircleMappingService.class);

    @Autowired
    private SECircleMappingDAO seCircleMappingDAO;

    public SECircleMappingInterface getByUsername(final String username){
        final String methodName = "getByUsername() : ";
        logger.info(methodName + "called with username " + username);
        SECircleMappingInterface seCircleMapping = null;
        if(username != null){
            seCircleMapping = seCircleMappingDAO.getByUsername(username);
        }
        return seCircleMapping;
    }

    public SECircleMappingInterface getByCircleId(final long circleId){
        final String methodName = "getByCircleId() : ";
        logger.info(methodName + "called with circle id " + circleId);
        SECircleMappingInterface seCircleMapping = null;
        seCircleMapping = seCircleMappingDAO.getByCircleId(circleId);
        return seCircleMapping;
    }
}
