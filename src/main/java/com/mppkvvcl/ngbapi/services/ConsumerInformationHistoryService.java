package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationHistoryInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerInformationHistoryDAO;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 08-06-2017.
 */
@Service
public class ConsumerInformationHistoryService {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerInformationHistoryService.class);

    /**
     * Asking Spring to inject ConsumerInformationHistoryRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer_information_history table
     * at the backend Database
     */
    @Autowired
    private ConsumerInformationHistoryDAO consumerInformationHistoryDAO;

    public ConsumerInformationHistoryInterface insert(ConsumerInformationHistoryInterface consumerInformationHistoryInterface){
        final String methodName = "insert() : ";
        logger.info(methodName + "called");
        ConsumerInformationHistoryInterface insertedConsumerInformationHistoryInterface = null;
        if(consumerInformationHistoryInterface != null){
            setAuditDetails(consumerInformationHistoryInterface);
            insertedConsumerInformationHistoryInterface = consumerInformationHistoryDAO.add(consumerInformationHistoryInterface);
        }
        return insertedConsumerInformationHistoryInterface;
    }

    public List<ConsumerInformationHistoryInterface> insert(List<ConsumerInformationHistoryInterface> consumerInformationHistoryInterfaces){
        final String methodName = "insert() : ";
        logger.info(methodName + "called");
        List<ConsumerInformationHistoryInterface> insertedConsumerInformationHistoryInterfaces = null;
        if(consumerInformationHistoryInterfaces != null){
            insertedConsumerInformationHistoryInterfaces = new ArrayList<>();
            for(ConsumerInformationHistoryInterface cihi : consumerInformationHistoryInterfaces){
                setAuditDetails(cihi);
                ConsumerInformationHistoryInterface insertedConsumerInformationHistoryInterface = insert(cihi);
                if(insertedConsumerInformationHistoryInterface == null) return null;
                insertedConsumerInformationHistoryInterfaces.add(insertedConsumerInformationHistoryInterface);
            }
        }
        return insertedConsumerInformationHistoryInterfaces;
    }

    /**
     * This service method provides ConsumerInformationHistories
     * for input parameter consumerNo<br><br>
     * param consumerNo<br>
     * return consumerInformationHistories
     *
     */
    public List<ConsumerInformationHistoryInterface> getConsumerInformationHistoryByConsumerNo(String consumerNo){
        String methodName = "getConsumerInformationHistoryByConsumerNo : ";
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        logger.info(methodName + " Got input consumerNo. "+consumerNo);
        if(consumerNo != null){
            logger.info(methodName + " calling consumerInformationHistoryRepository to get ConsumerInformationHistories ");
            consumerInformationHistories=consumerInformationHistoryDAO.getByConsumerNo(consumerNo);
            if(consumerInformationHistories != null){
                if(consumerInformationHistories.size()>0){
                    logger.info(methodName + "successfully got consumerInformationHistories as "+consumerInformationHistories);
                }else{
                    logger.info(methodName + "got zero consumerInformationHistories ");
                }
            }else{
                logger.info(methodName + "consumerInformationHistories is null.Unable to get consumerInformationHistories with input paramter consumerNo : "+consumerNo);
            }
        }else{
            logger.info(methodName + "input parameter consumerNo is null");
        }
        return consumerInformationHistories;
    }

    /**
     * This service method provides ConsumerInformationHistories
     * for input parameter startindDate and endingDate such that endDate paramter
     * in table falls in between these two passed dates<br><br>
     * param startingDate<br>
     * param endingDate<br>
     * return consumerInformationHistories
     */
    public List<ConsumerInformationHistoryInterface> getConsumerInformationHistoryByEndDateBetween(Date startingDate,Date endingDate){
        String methodName="getConsumerInformationHistoryByEndDateBetween : ";
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        logger.info(methodName + " Got input startingDate : "+startingDate + "endingDate :"+endingDate);
        if(startingDate != null && endingDate != null){
            logger.info(methodName + " calling consumerInformationHistoryRepository to get ConsumerInformationHistories ");
            consumerInformationHistories = consumerInformationHistoryDAO.getByEndDateBetween(startingDate,endingDate);
            if(consumerInformationHistories != null){
                if(consumerInformationHistories.size() > 0){
                    logger.info(methodName + "successfully got consumerInformationHistories as "+ consumerInformationHistories);
                }else{
                    logger.info(methodName + "Got zero consumerInformationHistories ");
                }
            }else{
                logger.info(methodName + "consumerInformationHistories is null.Unable to get consumerInformationHistories with input paramters startindDate "+startingDate + "endingDate " + endingDate);
            }
        }else{
            logger.info(methodName + "input parameter may be null");
        }
        return consumerInformationHistories;
    }

    /**
     * This service method provides ConsumerInformationHistories
     * for input parameter startindDate and endingDate such that endDate paramter
     * in table falls in between these two passed dates and propertName matches the property_name column<br><br>
     * param propertyName<br>
     * param startingDate<br>
     * param endingDate<br>
     * return consumerInformationHistories
     *
     */
    public List<ConsumerInformationHistoryInterface> getConsumerInformationHistoryByPropertyNameAndEndDateBetween(String propertyName,Date startingDate,Date endingDate){
        String methodName = "getConsumerInformationHistoryByPropertyNameAndEndDateBetween : ";
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        logger.info(methodName + " Got input propertyName : "+propertyName + "  startingDate : " + startingDate + "endingDate :"+ endingDate);
        if(propertyName != null && startingDate != null && endingDate != null){
            logger.info(methodName + " calling consumerInformationHistoryRepository to get ConsumerInformationHistories ");
            consumerInformationHistories = consumerInformationHistoryDAO.getByPropertyNameAndEndDateBetween(propertyName,startingDate,endingDate);
            if(consumerInformationHistories != null){
                if(consumerInformationHistories.size() > 0){
                    logger.info(methodName + "successfully got consumerInformationHistories as "+ consumerInformationHistories);
                }else{
                    logger.info(methodName + "Got zero consumerInformationHistories ");
                }
            }else{
                logger.info(methodName + "consumerInformationHistories is null.Unable to get consumerInformationHistories with input paramters startindDate "+startingDate + "endingDate " + endingDate);
            }
        }else{
            logger.info(methodName + "input parameter may be null");
        }
        return consumerInformationHistories;
    }

    private void setAuditDetails(ConsumerInformationHistoryInterface consumerInformationHistoryInterface){
        String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if(consumerInformationHistoryInterface != null) {
            consumerInformationHistoryInterface.setCreatedOn(GlobalResources.getCurrentDate());
            consumerInformationHistoryInterface.setCreatedBy(GlobalResources.getLoggedInUser());
        }
    }
}
