package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.MeterCTRMappingInterface;
import com.mppkvvcl.ngbdao.daos.MeterCTRMappingDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/4/2017.
 */
@Service
public class MeterCTRMappingService {

    /**
     * Requesting spring to get singleton meterCTRMappingRepository object.
     */
    @Autowired
    private MeterCTRMappingDAO meterCTRMappingDAO;

    /**
     * Requesting spring to get singleton ctrMasterService object.
     */
    @Autowired
    private CTRMasterService ctrMasterService;
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(MeterCTRMappingService.class);

    /**
     * This method takes meterIdentifier and fetches the list of meterCTRMappings against it.
     * Return meterCTRMappings if successful else return null.<br><br>
     * param meterIdentifier<br>
     * return meterCTRMappings<br>
     */
    public List<MeterCTRMappingInterface> getByMeterIdentifier(String meterIdentifier) {
        String methodName = " getByMeterIdentifier(): ";
        logger.info(methodName + " service method called for fetching MeterCTRMapping " + meterIdentifier);
        List<MeterCTRMappingInterface> meterCTRMappings = null;
        if (meterIdentifier != null) {
            meterIdentifier = meterIdentifier.trim();
            meterCTRMappings = meterCTRMappingDAO.getByMeterIdentifier(meterIdentifier);
            if (meterCTRMappings != null && meterCTRMappings.size() > 0) {
                logger.info(methodName + " Successfully retrieved MeterCTRMappings" + meterCTRMappings);
            } else {
                logger.error(methodName + " CTR Mapping not retrieved" );
            }
        } else {
            logger.error(methodName + "meterIdentifier passed is null in method.");
        }
        return meterCTRMappings;
    }

    /**
     * This method takes meterIdentifier, status and fetches the list of meterCTRMappings against it.
     * Return meterCTRMappings if successful else return null.<br><br>
     * param meterIdentifier<br>
     * param status<br>
     * return meterCTRMappings<br>
     */
    public List<MeterCTRMappingInterface> getByMeterIdentifierAndStatus(String meterIdentifier, String status) {
        String methodName = " getByMeterIdentifierAndStatus(): ";
        logger.info(methodName + " service method called for fetching MeterCTRMapping meter: " + meterIdentifier+" status: "+status);
        List<MeterCTRMappingInterface> meterCTRMappings = null;
        if (meterIdentifier != null && status != null ) {
            meterIdentifier = meterIdentifier.trim();
            status = status.trim();
            meterCTRMappings = meterCTRMappingDAO.getByMeterIdentifierAndMappingStatus(meterIdentifier,status);
            if (meterCTRMappings != null && meterCTRMappings.size() > 0) {
                logger.info(methodName + " Successfully retrieved MeterCTRMappings" + meterCTRMappings);
            } else {
                logger.error(methodName + " CTR Mapping not retrieved" );
            }
        } else {
            logger.error(methodName + "meterIdentifier passed is null in method.");
        }
        return meterCTRMappings;
    }

    /**
     * This method takes meterIdentifier and fetch the meterCTRMapping object against it.
     * Return meterCTRMapping object if successful else return null.<br><br>
     * param meterIdentifier<br>
     * return meterCTRMapping<br>
     */
    public MeterCTRMappingInterface getActiveMappingByMeterIdentifier(String meterIdentifier) {
        String methodName = " getActiveMappingByMeterIdentifier(): ";
        logger.info(methodName + " service method called for fetching Active MeterCTRMapping meterIdentifier: " + meterIdentifier);
        List<MeterCTRMappingInterface> meterCTRMappings = null;
        MeterCTRMappingInterface meterCTRMapping = null;
        if (meterIdentifier != null ) {
            meterIdentifier = meterIdentifier.trim();
            meterCTRMappings = getByMeterIdentifierAndStatus(meterIdentifier,MeterCTRMappingInterface.STATUS_ACTIVE);
            if (meterCTRMappings != null && meterCTRMappings.size() == 1 ) {
                MeterCTRMappingInterface retrievedMeterCTRMapping = meterCTRMappings.get(0);
                if(retrievedMeterCTRMapping != null) {
                    meterCTRMapping = retrievedMeterCTRMapping;
                    logger.info(methodName + " Successfully retrieved MeterCTRMapping" + meterCTRMapping);
                }else{
                    logger.error(methodName + " MeterCTRMapping not retrieved");
                }
            } else {
                logger.error(methodName + " CTR Mapping list not retrieved" );
            }
        } else {
            logger.error(methodName + "meterIdentifier passed is null in method.");
        }
        return meterCTRMapping;
    }

    /**
     * This method takes ctrIdentifier, mappingStatus and fetches the list of meterCTRMappings against it.
     * Return meterCTRMappings if successful else return null.<br><br>
     * param ctrIdentifier<br>
     * param mappingStatus<br>
     * return meterCTRMappings<br>
     */
    public List<MeterCTRMappingInterface> getByCTRIdentifierAndMappingStatus(String ctrIdentifier, String mappingStatus) {
        String methodName = " getByCTRIdentifierAndMappingStatus(): ";
        logger.info(methodName + " service method called for fetching MeterCTRMapping " + ctrIdentifier);
        List<MeterCTRMappingInterface> meterCTRMappings = null;
        if (ctrIdentifier != null && mappingStatus != null) {
            ctrIdentifier = ctrIdentifier.trim();
            meterCTRMappings = meterCTRMappingDAO.getByCtrIdentifierAndMappingStatus(ctrIdentifier, mappingStatus);
            if (meterCTRMappings.size() > 0) {
                logger.info(methodName + "Successfully retrieved MeterCTRMapping" + meterCTRMappings);
            } else {
                logger.error(methodName + "CTR Mapping not retrieved");
            }
        } else {
            logger.error(methodName + "meterIdentifier and mappingStatus passed is null in method.");
        }
        return meterCTRMappings;
    }

    /**
     * Added By : Preetesh
     * fetch CTR master of a meter by meter identifier<br><br>
     * param meterIdentifier<br>
     * return CTRMaster<br>
     */
    public CTRMasterInterface getActiveCTRMasterByMeterIdentifier(String meterIdentifier) {

        String methodName = " getActiveCTRMasterByMeterIdentifier() ";
        CTRMasterInterface ctrMaster = null;
        if (meterIdentifier != null) {
            meterIdentifier = meterIdentifier.trim();
            MeterCTRMappingInterface meterCTRMapping = getActiveMappingByMeterIdentifier(meterIdentifier);
            if (meterCTRMapping != null ) {
                String ctrIdentifier = meterCTRMapping.getCtrIdentifier();
                ctrMaster = ctrMasterService.getByIdentifier(ctrIdentifier);
                if(ctrMaster != null ) {
                    logger.info(methodName + "CTR Master found ");
                }else{
                    logger.error(methodName + "CTR Master not found for meter: "+meterIdentifier);
                }
            }
        }else{
            logger.error(methodName + " mapping not found");
        }
        return ctrMaster;
    }

    /**
     * This method takes MeterCTRMapping object and insert it into the MeterCTRMapping table at the backend database.
     * Return insertedMeterCTRMapping if insertion successful else return null.<br><br>
     * param meterCTRMapping<br>
     * return insertedMeterCTRMappings<br>
     */
    public MeterCTRMappingInterface insert(MeterCTRMappingInterface meterCTRMapping) {
        MeterCTRMappingInterface insertedMeterCTRMapping =null;
        String methodName = " insert() ";
        logger.info(methodName+"called");
        if(meterCTRMapping != null) {
            setAuditDetails(meterCTRMapping);
            insertedMeterCTRMapping = meterCTRMappingDAO.add(meterCTRMapping);
        }
        return insertedMeterCTRMapping;
    }

    @Transactional(rollbackFor = Exception.class)
    public MeterCTRMappingInterface inActivateMeterCTRMappingByMeterIdentifier(String meterIdentifier) throws Exception{
        String methodName = "inActivateMeterCTRMappingByMeterIdentifier() : ";
        logger.info(methodName + "called");
        MeterCTRMappingInterface activeMeterCTRMapping = getActiveMappingByMeterIdentifier(meterIdentifier);
        if (activeMeterCTRMapping == null) {
            logger.error(methodName + " meter CTR mapping not found "+activeMeterCTRMapping);
            throw new Exception("meter CTR mapping not found");
        }
        activeMeterCTRMapping.setMappingStatus(MeterCTRMappingInterface.STATUS_INACTIVE);
        activeMeterCTRMapping.setUpdatedOn(GlobalResources.getCurrentDate());
        activeMeterCTRMapping.setUpdatedBy(GlobalResources.getLoggedInUser());
        MeterCTRMappingInterface inActiveMeterCTRMapping = meterCTRMappingDAO.add(activeMeterCTRMapping);
        return inActiveMeterCTRMapping;
    }

    public MeterCTRMappingInterface update(MeterCTRMappingInterface meterCTRMapping) {
        MeterCTRMappingInterface updatedMeterCTRMapping =null;
        String methodName = " update() ";
        logger.info(methodName+"called");
        if(meterCTRMapping != null) {
            updateAuditDetails(meterCTRMapping);
            updatedMeterCTRMapping = meterCTRMappingDAO.add(meterCTRMapping);
        }
        return updatedMeterCTRMapping;
    }

    private  void setAuditDetails(MeterCTRMappingInterface meterCTRMapping){
        String methodName = "setAuditDetails()  :";
        if(meterCTRMapping != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            meterCTRMapping.setUpdatedOn(date);
            meterCTRMapping.setUpdatedBy(user);
            meterCTRMapping.setCreatedOn(date);
            meterCTRMapping.setCreatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }

    private void updateAuditDetails(MeterCTRMappingInterface meterCTRMapping){
        String methodName = "updateAuditDetails()  :";
        if(meterCTRMapping != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            meterCTRMapping.setUpdatedOn(date);
            meterCTRMapping.setUpdatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }
}
