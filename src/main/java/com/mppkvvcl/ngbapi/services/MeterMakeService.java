package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterMakeInterface;
import com.mppkvvcl.ngbdao.daos.MeterMakeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */
@Service
public class MeterMakeService {

    /**
     * Getting logger objects for logging in current class
     */
    private Logger logger = GlobalResources.getLogger(MeterMakeService.class);

    /**
     * Asking spring to inject MeterMakeRepository so that we can use its various methods
     */
    @Autowired
    private MeterMakeDAO meterMakeDAO;

    /**
     * This getAll() method is used for getting all objects of MeterMake.<br><br>
     * return meterMakes
     */
    public List<? extends MeterMakeInterface> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends MeterMakeInterface> meterMakes = meterMakeDAO.getAll();
        return meterMakes;
    }
}
