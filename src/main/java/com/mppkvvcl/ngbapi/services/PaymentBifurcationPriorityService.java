package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.PaymentBifurcationPriorityDAO;
import com.mppkvvcl.ngbinterface.interfaces.PaymentBifurcationPriorityInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 1/9/2018.
 */
@Service
public class PaymentBifurcationPriorityService {

    private static final Logger logger = GlobalResources.getLogger(PaymentBifurcationPriorityService.class);

    @Autowired
    private PaymentBifurcationPriorityDAO paymentBifurcationPriorityDAO;

    public List<PaymentBifurcationPriorityInterface> getAllOrderByPriority() {
        String methodName = " getAllOrderByPriority() : ";
        logger.info(methodName + "called ");
        List<PaymentBifurcationPriorityInterface> paymentBifurcationPriorityInterfaces = paymentBifurcationPriorityDAO.getAllOrderByPriority();
        return paymentBifurcationPriorityInterfaces;
    }
}
