package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.HolidayInterface;
import com.mppkvvcl.ngbdao.daos.HolidayDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by PREETESH on 9/7/2017.
 */
@Service
public class HolidayService {

    @Autowired
    private HolidayDAO holidayDAO;

    private static final Logger logger = GlobalResources.getLogger(HolidayService.class);

    public boolean checkHoliday(Date date) {
        String methodName = " checkHoliday(): ";
        HolidayInterface holiday = null;
        boolean holidayCheck = false;
        logger.info(methodName + "got request to check holiday for day "+date);
        holiday = holidayDAO.getByDate(date);
        if (holiday != null ) {
            holidayCheck = true;
            logger.info(methodName + "on date "+date+", we have found a holiday " + holiday);
        } else {

            logger.info(methodName + " this is not a holiday on date: " + date);
        }
        return holidayCheck;
    }

    public boolean checkSunday(Date date) {
        String methodName = " checkSunday(): ";
        boolean sundayCheck = false;
        logger.info(methodName + "got request to check sunday for day "+date);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            sundayCheck = true;
        }
        return sundayCheck;
    }

    public boolean checkSecondSaturday(Date date) {
        String methodName = " checkSecondSaturday(): ";
        boolean secondSaturdayCheck = false;
        logger.info(methodName + "got request to check SecondSaturday for day "+date);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        int weekOfMonth = c.get(Calendar.WEEK_OF_MONTH);
        if (weekOfMonth == 2 && dayOfWeek == Calendar.SATURDAY ) {
            secondSaturdayCheck = true;
        }
        return secondSaturdayCheck;
    }
}
