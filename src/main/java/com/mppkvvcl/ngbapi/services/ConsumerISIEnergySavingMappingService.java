package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerISIEnergySavingMappingInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerISIEnergySavingMappingDAO;
import com.mppkvvcl.ngbinterface.interfaces.ISIEnergySavingTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class ConsumerISIEnergySavingMappingService {
    /**
     * Asking Spring to inject ConsumerISIEnergySavingMappingRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on  table
     * at the backend Database
     */
    @Autowired
    private ConsumerISIEnergySavingMappingDAO consumerISIEnergySavingMappingDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerISIEnergySavingMappingService.class);

    /**
     * This method takes ConsumerISIEnergySavingMapping object and insert it into the backend database.
     * Return insertedConsumerISIEnergySavingMapping if successful else return null.<br><br>
     * param consumerISIEnergySavingMapping<br>
     * return insertedConsumerISIEnergySavingMapping<br>
     */
    public ConsumerISIEnergySavingMappingInterface insert(ConsumerISIEnergySavingMappingInterface consumerISIEnergySavingMapping){
        String methodName = " insert() : ";
        ConsumerISIEnergySavingMappingInterface insertedConsumerISIEnergySavingMapping = null;
        logger.info(methodName + "Started insertion for ConsumerISIEnergySavingMapping ");
        if(consumerISIEnergySavingMapping != null){
            logger.info(methodName + " Calling ConsumerISIEnergySavingMappingRepository for inserting EmployeeMaster ");
            insertedConsumerISIEnergySavingMapping = consumerISIEnergySavingMappingDAO.add(consumerISIEnergySavingMapping);
            if (insertedConsumerISIEnergySavingMapping != null){
                logger.info(methodName + "Successfully inserted one row for ConsumerISIEnergySavingMapping");
            }else {
                logger.error(methodName + "Unable to insert into ConsumerISIEnergySavingMapping. ConsumerISIEnergySavingMappingrRepository returning null");
            }
        }else{
            logger.error(methodName + " Received ConsumerISIEnergySavingMapping object is null ");
        }
        return insertedConsumerISIEnergySavingMapping;
    }
}
