package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BankMasterInterface;
import com.mppkvvcl.ngbdao.daos.BankMasterDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 27-07-2017.
 */
@Service
public class BankMasterService {
    /**
     * Requesting spring to inject singleton BankMasterRepository object.
     */
    @Autowired
    BankMasterDAO bankMasterDAO;
    /**
     * Getting whole logger object from GlobalResources for BankMasterService class.
     */
    private static final Logger logger = GlobalResources.getLogger(BankMasterService.class);

    /**
     * This getAll() method fetches the list of BankMaster from the backend database.
     * Return list if found else return null.<br><br>
     * return list of bankMasters
     */
    public List<? extends BankMasterInterface> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "is called to get all BankMaster");
        List<? extends BankMasterInterface> bankMasters = null;
        logger.info(methodName + "Calling BankMasterRepository to get all bankMasters ");
        bankMasters = bankMasterDAO.getAll();
        if(bankMasters != null){
            if(bankMasters.size() > 0){
                logger.info(methodName + "List of bankMasters received successfully");
            }else{
                logger.error(methodName + "No content found in list");
            }
        }else{
            logger.error(methodName + "List of bankMasters not found");
        }
        return bankMasters;
    }
}
