package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomPayment;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.utility.NGBAPIUtility;
import com.mppkvvcl.ngbdao.daos.PaymentDAO;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.InstrumentDetail;
import com.mppkvvcl.ngbentity.beans.Payment;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 14-07-2017.
 */
@Service
public class PaymentService {

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(PaymentService.class);

    /**
     * Requesting spring to get singleton PaymentRepository object.
     */
    @Autowired
    private PaymentDAO paymentDAO;

    @Autowired
    private InstrumentPaymentMappingService instrumentPaymentMappingService;

    @Autowired
    private ConfiguratorService configuratorService;

    @Autowired
    private PartPaymentService partPaymentService;

    @Autowired
    private InstrumentDishonourService instrumentDishonourService;

    @Autowired
    private CashWindowStatusService cashWindowStatusService;

    @Autowired
    private InstrumentDetailService instrumentDetailService;

    @Autowired
    private BillService billService;

    @Autowired
    private AgricultureBill6MonthlyService agricultureBill6MonthlyService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private PaymentBifurcationService paymentBifurcationService;

    /**
     * This method takes locationCode and PostingBillMonth and fetch a list of payments.
     * Return list if successful else return null<br><br>
     * param locationCode<br>
     * param postingBillMonth<br>
     * return List of Payment<br>
     */
    public List<PaymentInterface> getByLocationCodeAndPostingBillMonth(String locationCode, String postingBillMonth){
        String methodName = "getByLocationCodeAndPostingBillMonth() : ";
        List<PaymentInterface> payments = null;
        logger.info(methodName + "called locationCode : " + locationCode + "and postingBillMonth : " + postingBillMonth);
        if(locationCode != null && postingBillMonth != null){
            logger.info(methodName + "Calling paymentDAO method to get payments list against locationCode : " + locationCode + "and postingBillMonth : " + postingBillMonth);
            payments = paymentDAO.getByLocationCodeAndPostingBillMonth(locationCode,postingBillMonth);
        }else{
            logger.error(methodName + "Input param locationCode : " + locationCode + "and postingBillMonth : " + postingBillMonth + "found null");
        }
        return payments;
    }

    /**
     * This method takes consumerNo and fetch a list of payments.
     * Return list if successful else return null.<br><br>
     * param consumerNo<br>
     * return List of Payment<br>
     */
    public List<PaymentInterface> getByConsumerNo(String consumerNo){
        String methodName = "getByConsumerNo() : ";
        List<PaymentInterface> payments = null;
        logger.info(methodName + "Got request to view payments against consumerNo : " + consumerNo);
        if(consumerNo != null){
            logger.info(methodName + "Calling PaymentRepository method to get Payment list against consumerNo : " + consumerNo);
            payments = paymentDAO.getByConsumerNo(consumerNo);
        }else{
            logger.error(methodName + "Input param consumerNo : " + consumerNo + "found null");
        }
        return payments;
    }

    public long getCountByConsumerNo(String consumerNo){
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        return paymentDAO.getCountByConsumerNo(consumerNo);
    }

    /**
     * coded by nitish
     * returns list of payments in paginated way as per passed parameters
     * @param consumerNo
     * @param sortBy
     * @param sortOrder
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public List<PaymentInterface> getByConsumerNoWithPagination(String consumerNo, String sortBy, String sortOrder, int pageNumber, int pageSize){
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo + " " + sortBy);
        List<PaymentInterface> paymentInterfaces = Collections.emptyList();
        pageNumber = pageNumber - 1;
        if(consumerNo != null && sortBy != null && pageNumber >= 0){
            Pageable pageable = null;
            if(sortOrder != null && sortOrder.equalsIgnoreCase(NGBAPIUtility.SORT_ORDER_ASCENDING)){
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,"id");
                    paymentInterfaces = paymentDAO.getByConsumerNoOrderByPostingBillMonthAscendingWithPageable(consumerNo,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize, Sort.Direction.ASC,sortBy);;
                    paymentInterfaces = paymentDAO.getByConsumerNo(consumerNo,pageable);
                }
            }else{
                if(sortBy.equalsIgnoreCase("postingBillMonth")){
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,"id");
                    paymentInterfaces = paymentDAO.getByConsumerNoOrderByPostingBillMonthDescendingWithPageable(consumerNo,pageable);
                }else{
                    pageable = new PageRequest(pageNumber,pageSize,Sort.Direction.DESC,sortBy);
                    paymentInterfaces = paymentDAO.getByConsumerNo(consumerNo,pageable);
                }
            }
        }
        return paymentInterfaces;
    }

    /**
     * This method takes consumerNo, deleted and posted and fetch list of payments against it.
     * Return paymentsList if successful else return null.<br><br>
     * param consumerNo<br>
     * param deleted<br>
     * param posted<br>
     * return payments
     */
    public List<PaymentInterface> getByConsumerNoAndDeletedAndPosted(String consumerNo, boolean deleted, boolean posted){
        String methodName = "getByConsumerNoAndDeletedAndPosted() : ";
        List<PaymentInterface> payments = null;
        logger.info(methodName + "Got request to view payments against consumerNo : " +consumerNo + ", deleted : " + deleted + "and posted : " + posted);
        if(consumerNo != null){
            logger.info(methodName + "Calling PaymentRepository method to get payments list against consumerNo : " +consumerNo + ", deleted : " + deleted + "and posted : " + posted);
            payments = paymentDAO.getByConsumerNoAndDeletedAndPosted(consumerNo, deleted, posted);
        }else{
            logger.error(methodName + "Input param found null");
        }
        return payments;
    }

    /**
     * Added By : Preetesh Date 26 July 2017<br><br>
     * This method takes location, windowName, payDate and fetch list of payments against it.
     * Return paymentsList if successful else return null.<br><br>
     * param location<br>
     * param windowName<br>
     * param payDate<br>
     * return payments
     */
    public List<PaymentInterface> getByLocationAndWindowNameAndDateAndDeleted(String location, String windowName, Date payDate) {
        String methodName = "getByLocationAndWindowNameAndDateAndDeleted() : ";
        List<PaymentInterface> payments = null;
        logger.info(methodName + "Got request to view payments against location : "+location+" windowName : " +windowName + ", date : " + payDate);
        if(location != null && windowName != null && payDate != null){
            payments = paymentDAO.getByLocationCodeAndPayWindowAndPayDateAndDeleted(location, windowName, payDate, PaymentInterface.DELETED_FALSE );
        }else{
            logger.error(methodName + "Input param found null");
        }
        return payments;
    }

    /**
     * Added By : Preetesh Dated : 31 July 2017
     * @param consumerNo
     * @param billMonth
     * @param deleted
     * @param posted
     * @return
     */
    public List<PaymentInterface> getByConsumerNoAndBillMonthAndDeletedAndPosted(String consumerNo, String billMonth, boolean deleted, boolean posted) {

        String methodName = "getByConsumerNoAndBillMonthAndDeletedAndPosted() : ";
        List<PaymentInterface> payments = null;
        logger.info(methodName + "Got request to view payments against consumerNo : " +consumerNo + ", deleted is : " + deleted + "and posted is : " + posted);
        if(consumerNo != null && billMonth != null){
            logger.info(methodName + "Calling PaymentRepository method to get payments list against consumerNo : " +consumerNo + " and bill month "+billMonth+", deleted : " + deleted + "and posted : " + posted);
            payments = paymentDAO.getByConsumerNoAndPostingBillMonthAndDeletedAndPosted(consumerNo,billMonth, deleted, posted);
        }else{
            logger.error(methodName + "Input param found null");
        }
        return payments;
    }

    /**
     * Added By: Preetesh Date: 11 Aug 2017
     * 1. After checking not-null for inputs, retrieve the latest bill
     * 2. Check instrument flag, if it is true,
     *    a. check instrument details, and its properties to be correct
     *    b. check acceptability of check for given consumer
     *    c. set due date to cheque due date, and retrieve payment-mode (DD/Bankers-cheque/Cheque)
     * 3. if instrument flag is false set payment-mode to CASH, and due date to bill due date
     * 4. Check for Part-Payment-Flag if it true, check for acceptability of part payment, retrieve
     *    Part-Payment-Amount, and set applied flag to true in PartPayment record.
     * 5. Now calculate already received payment against current bill
     * 6. calculate due amount of consumer
     * 7. check payment amount is greater than Part-Payment-Amount or Due Amount
     * 8. set all properties of payment and save it
     * 9. if payment saved successfully, check for instrument flag
     * 10. if payment is done through Instrument, we are required to save InstrumentDetail and
     *     Instrument-Payment mapping.
     *
     * @param payAmount
     * @param instrumentFlag
     * @param partPaymentFlag
     * @param payDate
     * @param consumerNo
     * @param instrumentDetail
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public PaymentInterface insertByAmountAndInstrumentFlagAndPartPaymentFlagAndDateAndConsumerNo(long payAmount, boolean instrumentFlag, boolean partPaymentFlag,
                                                                                                  String payDate, String consumerNo, InstrumentDetail instrumentDetail) throws RuntimeException, Exception{
        final String methodName = " insertByAmountAndInstrumentFlagAndPartPaymentFlagAndDateAndConsumerNo() : ";
        logger.info(methodName + "called for consumer  " + consumerNo);
        PaymentInterface paymentToSave;
        PaymentInterface insertedPayment;
        PartPaymentInterface partPayment;
        BillInterface bill;
        long billAmount;
        long partPaymentAmount = 0;
        long totalPaid = 0;
        long balanceAmountToPay = 0;
        Date dueDate;
        String paymentMode;
        Date currentDate = GlobalResources.getCurrentDate();
        String loggedInUser = GlobalResources.getLoggedInUser();
        boolean chequeAcceptable;
        if (consumerNo != null && payDate != null && payAmount > 0 ) {
            consumerNo = consumerNo.trim();
            Date paymentDate = GlobalResources.getDateFromString(payDate);
            bill = billService.getLatestBillByConsumerNo(consumerNo);
            logger.info(methodName + "bill retrieved "+bill);
            if(bill != null){
                if(instrumentFlag) {
                    if(instrumentDetail != null && instrumentDetail.getBankName()!=null
                            && instrumentDetail.getInstrumentNo() != null
                            && instrumentDetail.getMicrCode() != null
                            && instrumentDetail.getPayMode() != null
                            && instrumentDetail.getInstrumentDate() != null
                            && instrumentDetail.getAmount() != null
                            && instrumentDetail.getAmount().longValue()>0
                            ){
                        chequeAcceptable = instrumentDishonourService.getValidityByConsumerNoAndDate(consumerNo, paymentDate);
                        if(chequeAcceptable){
                            dueDate = bill.getChequeDueDate();
                            paymentMode = instrumentDetail.getPayMode();
                            logger.info(methodName + "cheque can be accepted from consumer "+consumerNo+
                                    " and cheque due date selected "+dueDate);
                        }else{
                            logger.error(methodName + "consumer is in defaulter's list, can't take cheque ");
                            throw new Exception("consumer is in defaulter's list, can't take cheque ");
                        }
                    }else{
                        logger.error(methodName + "instrument details missing ");
                        throw new Exception("instrument details missing ");
                    }

                }else{
                    dueDate = bill.getDueDate();
                    paymentMode = PaymentInterface.PAY_MODE_CASH;
                    logger.info(methodName + "payment in cash, normal due date selected "+dueDate);
                }
                logger.info(methodName + "comparing payment date "+paymentDate+" with due date: "+dueDate);

                if(paymentDate.after(dueDate)){
                    billAmount = bill.getNetBill().longValue()+bill.getCurrentBillSurcharge().longValue();
                    logger.info("payment date is after due date"+ billAmount);
                }else{
                    billAmount =  bill.getNetBill().longValue();
                    logger.info("payment date is before due date"+ billAmount);
                }
                logger.info(methodName + " bill amount selected "+billAmount);

                if(partPaymentFlag) {
                    partPayment = partPaymentService.getNotAppliedPartPaymentForConsumerNo(consumerNo);
                    if (partPayment != null) {
                        logger.info(methodName + "found part payment approval " + partPayment);
                        Date approvedOn = GlobalResources.getDateWithoutTimeStamp(partPayment.getCreatedOn());
                        if (approvedOn != null) {
                            if (paymentDate.equals(approvedOn) || paymentDate.after(approvedOn)) {
                                long dayDifference = GlobalResources.getDateDiffInDays(paymentDate, approvedOn);
                                long partPaymentValidForDays = configuratorService.getValueForCode("PART-PAYMENT-VALID-FOR-DAYS");
                                if (dayDifference <= partPaymentValidForDays) {
                                    partPaymentAmount = partPayment.getAmount();
                                    logger.info(methodName + "found part payment approval for consumer " + consumerNo);
                                    if (partPaymentAmount > 0) {
                                        logger.info(methodName + "found part payment amount as " + partPaymentAmount);
                                        partPayment.setApplied(true);
                                        partPayment.setUpdatedBy(loggedInUser);
                                        partPayment.setUpdatedOn(currentDate);
                                        PartPaymentInterface updatedPartPayment = partPaymentService.update(partPayment);
                                        if(updatedPartPayment != null){
                                            logger.info(methodName + "part payment updated successfully");
                                        }else{
                                            logger.error(methodName + "part payment not updated");
                                            throw new Exception("part payment not updated");
                                        }
                                    } else {
                                        logger.error(methodName + "part payment amount not found");
                                        throw new Exception("part payment amount not found");
                                    }

                                }else{
                                    logger.error(methodName + "consumer has no active part payment approval ");
                                    throw new Exception("consumer has no active part payment approval ");
                                }
                            } else {
                                logger.error(methodName + "consumer has no active part payment approval ");
                                throw new Exception("consumer has no active part payment approval ");
                            }
                        }else{
                            logger.error(methodName + "part payment creation date not found ");
                            throw new Exception("part payment creation date not found ");
                        }
                    } else {
                        logger.error(methodName + "consumer has no part payment approval ");
                        throw new Exception("consumer has no part payment approval ");
                    }
                }

                List<PaymentInterface> payments = getByConsumerNoAndDeletedAndPosted(consumerNo, PaymentInterface.DELETED_FALSE, PaymentInterface.POSTED_FALSE);
                if(payments != null && payments.size() > 0){
                    for (PaymentInterface payment : payments) {
                        if(payment != null ){
                            long paidAmount = payment.getAmount();
                            if( paidAmount > 0){
                                totalPaid = totalPaid + paidAmount;
                            }
                        }
                    }
                }else{
                    logger.info(methodName + "no payments found");
                }

                balanceAmountToPay = billAmount - totalPaid;

                List<CashWindowStatusInterface> cashWindowStatuses = cashWindowStatusService.getByUsernameAndStatus(loggedInUser, CashWindowStatusInterface.STATUS_OPEN);
                if(cashWindowStatuses == null || cashWindowStatuses.size() != 1){
                    logger.error(methodName + "cash window not found");
                    throw new Exception("cash window not found ");
                }
                String windowName = cashWindowStatuses.get(0).getWindowName();
                Date windowDate = cashWindowStatuses.get(0).getDate();
                if(windowName != null){
                    paymentToSave = new Payment();
                    logger.info(methodName + "amount paid:"+totalPaid+" part payment amount: "+partPaymentAmount+"bill amount "+billAmount+" balanceAmountToPay: "+balanceAmountToPay+
                            " payAmount: "+payAmount);
                    if (partPaymentFlag && partPaymentAmount > 0 && payAmount >= partPaymentAmount) {
                        paymentToSave.setAmount(payAmount);
                    }else if (payAmount >= balanceAmountToPay){
                        paymentToSave.setAmount(payAmount);
                    }else{
                        logger.error(methodName + "part payment not allowed OR payment amount is lesser than billed amount");
                        throw new Exception("part payment not allowed OR payment amount is lesser than billed amount ");
                    }

                    paymentToSave.setSource(PaymentInterface.SOURCE_MANUAL);
                    paymentToSave.setOnline(PaymentInterface.ONLINE_FALSE);
                    String location = bill.getLocationCode();
                    paymentToSave.setLocationCode(location);
                    paymentToSave.setConsumerNo(consumerNo);
                    /**
                     * punching date is window date
                     */
                    paymentToSave.setPunchingDate(windowDate);
                    paymentToSave.setPayDate(paymentDate);
                    paymentToSave.setAmount(payAmount);
                    paymentToSave.setPayMode(paymentMode);
                    paymentToSave.setPayWindow(windowName);
                    paymentToSave.setDeleted(PaymentInterface.DELETED_FALSE);
                    paymentToSave.setPosted(PaymentInterface.POSTED_FALSE);
                    paymentToSave.setCreatedBy(loggedInUser);
                    paymentToSave.setCreatedOn(currentDate);
                    paymentToSave.setUpdatedBy(loggedInUser);
                    paymentToSave.setUpdatedOn(currentDate);

                    insertedPayment = paymentDAO.add(paymentToSave);
                    if(insertedPayment != null){
                        logger.info(methodName + "Payment saved successfully");

                        /**
                         * call for payment bifurcation
                         */
                        ErrorMessage bifurcationError = new ErrorMessage();
                        PaymentBifurcationInterface paymentBifurcationInterface = paymentBifurcationService.bifurcatePayment(insertedPayment,bifurcationError);
                        if(paymentBifurcationInterface == null){
                            logger.error("Bifurcation error: "+bifurcationError);
                            throw new Exception(bifurcationError.getErrorMessage());
                        }

                        /**
                         * Later on we shall make a separate method in InstrumentDetailsService for saving instrument
                         * -- saving instrument details if flag is on
                         */
                        if(instrumentFlag){
                            instrumentDetail.setDishonoured(InstrumentDetailInterface.DISHONOURED_FALSE);
                            instrumentDetail.setCreatedBy(loggedInUser);
                            instrumentDetail.setCreatedOn(currentDate);
                            instrumentDetail.setUpdatedBy(loggedInUser);
                            instrumentDetail.setUpdatedOn(currentDate);
                            InstrumentDetailInterface insertedInstrumentDetail = instrumentDetailService.insert(instrumentDetail);
                            if(insertedInstrumentDetail != null){
                                /**
                                 * Later-on we shall make a separate method in InstrumentPaymentMappingService for saving mappings
                                 * -- saving mapping if instrument-detail and payment saved successfully
                                 */
                                InstrumentPaymentMappingInterface insertedInstrumentPaymentMapping = instrumentPaymentMappingService.insertByInstrumentIdAndPaymentId(insertedInstrumentDetail.getId(),insertedPayment.getId());
                                if(insertedInstrumentPaymentMapping != null){
                                    logger.info(methodName + "InstrumentDetail,InstrumentPaymentMapping saved successfully");
                                }else{
                                    logger.error(methodName + "InstrumentPaymentMapping not saved ");
                                    throw new Exception("InstrumentPaymentMapping not saved ");
                                }
                            }else{
                                logger.error(methodName + "instrument details not saved ");
                                throw new Exception("instrument details not saved ");
                            }
                        }
                    }else{
                        logger.error(methodName + "payment not saved");
                        throw new Exception("payment not saved");
                    }
                }else{
                    logger.error(methodName + "window not found");
                    throw new Exception("window not found ");
                }
            }else{
                logger.error(methodName + "bill not found");
                throw new Exception("bill not found ");
            }
        } else {
            logger.error(methodName + "consumerNo passed is null in method.");
            throw new Exception("consumerNo passed is null in method");
        }
        return insertedPayment;
    }

    @Transactional(rollbackFor = Exception.class)
    public PaymentInterface updatePayment(PaymentInterface paymentToUpdate)throws  Exception {
        String methodName = "updatePayment() : ";
        PaymentInterface paymentInDB = null;
        long totalPaid = 0;
        long balanceAmountToPay;
        logger.info(methodName + "called");
        if(paymentToUpdate == null){
            logger.error(methodName + "payment to update found null");
            throw new Exception("payment to update found null");
        }
        long id = paymentToUpdate.getId();
        paymentInDB = paymentDAO.getById(id);
        if (paymentInDB == null) {
            logger.error(methodName + "payment not found");
            throw new Exception("payment not found ");
        }
        if(paymentInDB.getPayMode().equals(PaymentInterface.PAY_MODE_CHEQUE)||
                paymentInDB.getPayMode().equals(PaymentInterface.PAY_MODE_BANKERCHEQUE) ||
                paymentInDB.getPayMode().equals(PaymentInterface.PAY_MODE_DD) ){
            logger.error(methodName + "payment in cheque mode, go through the cheque edit form");
            throw new Exception("payment in cheque mode, go through the cheque edit form ");
        }
        String loggedIn = GlobalResources.getLoggedInUser();
        /**
         * payment punching date and window open date are same
         */
        String windowName = cashWindowStatusService.getOpenWindowNameByUserAndDate(loggedIn,paymentToUpdate.getPunchingDate());

        if (windowName == null) {
            logger.error(methodName + "open window not found");
            throw new Exception("open window not found");
        }

        if(!windowName.equals(paymentToUpdate.getPayWindow())){
            logger.error(methodName + " window name not matching");
            throw new Exception("window name not matching");
        }

        long updatedAmount = paymentToUpdate.getAmount();
        if(updatedAmount < PaymentInterface.MINIMUM_PAYMENT_AMOUNT){
            logger.error(methodName + " Amount is Less than One Enter Correct Amount ");
            throw new Exception("Amount is Less than One Enter Correct Amount");
        }

        String consumerNo=paymentInDB.getConsumerNo();
        BigDecimal billAmount;
        BillInterface bill = billService.getByConsumerNo(consumerNo);
        if(bill == null){
            logger.error(methodName + " bill not found ");
            throw new Exception(" bill not found ");
        }

        if(paymentInDB.getPayDate().after(bill.getDueDate())){
            billAmount = bill.getNetBill().add(bill.getCurrentBillSurcharge());
        }else{
            billAmount =  bill.getNetBill();
        }

        List<PaymentInterface> payments = getByConsumerNoAndDeletedAndPosted(consumerNo, PaymentInterface.DELETED_FALSE, PaymentInterface.POSTED_FALSE);
        if(payments != null && payments.size() > 0){
            // payments.remove(payment);
            for (PaymentInterface pay : payments) {
                if(pay != null && pay.getId()!= id){
                    long paidAmount = pay.getAmount();
                    if( paidAmount > 0){
                        totalPaid = totalPaid + paidAmount;
                    }
                }
            }
        }else{
            logger.info(methodName + "no payments found");
        }

        balanceAmountToPay = billAmount.longValue() - totalPaid;

        if(balanceAmountToPay <= updatedAmount){
            logger.info(methodName + "Payment amount acceptable : ");
        }else{
            //check for part payment
            Date updatedOn = paymentInDB.getCreatedOn();
            PartPaymentInterface partPayment = partPaymentService.getByConsumerNoAndAppliedAndUpdatedOn(consumerNo, PartPaymentInterface.APPLIED_TRUE,updatedOn);
            if(partPayment == null ){
                logger.error(methodName + "part payment not found and amount edited is also less than due bill amount");
                throw new Exception("part payment not found and amount edited is also less than due bill amount ");
            }else if (partPayment.getAmount() > updatedAmount){
                logger.error(methodName + "updated amount is less part payment amount ");
                throw new Exception("updated amount is less part payment amount ");
            }
        }
        // all checks passed
        paymentInDB.setAmount(updatedAmount);
        paymentInDB.setUpdatedOn(GlobalResources.getCurrentDate());
        paymentInDB.setUpdatedBy(loggedIn);
        PaymentInterface updatedPayment = paymentDAO.update(paymentInDB);
        return updatedPayment;
    }


    @Transactional(rollbackFor = Exception.class)
    public PaymentInterface deletePaymentById(long id) throws  Exception {
        String methodName = "deletePaymentById() : ";
        PaymentInterface paymentInDB = null;
        PaymentInterface paymentDeleted = null;
        logger.info(methodName + "Got request to Delete payments By id : " + id);
        paymentInDB = paymentDAO.getById(id);

        if (paymentInDB == null) {
            logger.error(methodName + "payment not found");
            throw new Exception("payment not found ");
        }
        String payMode=paymentInDB.getPayMode();

        if(payMode.equals(PaymentInterface.PAY_MODE_CHEQUE) || payMode.equals(PaymentInterface.PAY_MODE_BANKERCHEQUE) || payMode.equals(PaymentInterface.PAY_MODE_DD) ) {
            InstrumentPaymentMappingInterface instrumentPaymentMapping = instrumentPaymentMappingService.getByPaymentId(id);

            if (instrumentPaymentMapping == null) {
                logger.error(methodName + "instrument mapping not found");
                throw new Exception("instrument mapping not found ");
            }

            long instrumentDetailId = instrumentPaymentMapping.getInstrumentDetailId();

            List<InstrumentPaymentMappingInterface> instrumentPaymentMappings = instrumentPaymentMappingService.getByInstrumentDetailId(instrumentDetailId);
            if(instrumentPaymentMappings == null ){
                logger.error(methodName + "instrument mappings not found");
                throw new Exception("instrument mappings not found ");
            }
            if( instrumentPaymentMappings.size() > 1 ){
                logger.error(methodName + "payment is associated with multi-payment-through-cheque");
                throw new Exception("payment is associated with multi-payment-through-cheque");
            }

            long instrumentMappingId = instrumentPaymentMapping.getId();
            boolean instrumentMappingDeleted = instrumentPaymentMappingService.deleteById(instrumentMappingId);
            boolean instrumentDeleted = instrumentDetailService.deleteById(instrumentDetailId);

            if (instrumentMappingDeleted && instrumentDeleted) {
                paymentInDB.setDeleted(true);
                paymentDAO.update(paymentInDB);
            } else {
                logger.error(methodName + "Instrument Details Not Deleted ");
                throw new Exception("Instrument Details Not Deleted ");
            }

        }
        paymentInDB.setDeleted(true);
        paymentDeleted =  paymentDAO.update(paymentInDB);
        return paymentDeleted;
    }


    //added by : Preetesh

    @Transactional(rollbackFor = Exception.class)
    public List<PaymentInterface> insertMultiplePaymentsByInstrument(String bankName, String payMode, String instrumentNo, String instrumentDateString, String micrCode, long amount,
                                                                     List<? extends PaymentInterface> payments) throws RuntimeException, Exception {
        String methodName = " insertMultiplePaymentsByInstrument() : ";
        logger.info(methodName + "called");
        if (bankName == null || payMode == null || instrumentNo == null || instrumentDateString == null || micrCode == null || payments == null || payments.size() == 0 ){
            logger.error(methodName + "some input is null ");
            throw new Exception("some input is null ");
        }
        bankName = bankName.trim();
        payMode = payMode.trim();
        instrumentNo = instrumentNo.trim();
        instrumentDateString = instrumentDateString.trim();
        micrCode = micrCode.trim();
        List<PaymentInterface> savedPayments = null;

        if(!checkPayModeForInstrument(payMode)){
            logger.error(methodName + "payMode for instrument check failed ");
            throw new Exception("PayMode check failed ");
        }

        Date instrumentDate = GlobalResources.getDateFromString(instrumentDateString);
        if(instrumentDate == null){
            logger.error(methodName + "instrumentDateString to date conversion failed ");
            throw new Exception("instrumentDateString to date conversion failed ");
        }

        if(!checkInstrumentAmountAndTotalPaymentAmount(amount,payments)){
            logger.error(methodName + "Instrument Amount and Total Amount of payments not matching");
            throw new Exception("Instrument Amount and Total Amount of payments not matching ");
        }

        String loggedInUser = GlobalResources.getLoggedInUser();
        List<CashWindowStatusInterface> cashWindowStatuses = cashWindowStatusService.getByUsernameAndStatus(loggedInUser, CashWindowStatusInterface.STATUS_OPEN);
        if(cashWindowStatuses == null || cashWindowStatuses.size() != 1){
            logger.error(methodName + "No Open Window Found for logged In User");
            throw new Exception("No Open Window Found for logged In User");
        }
        Date windowDate = cashWindowStatuses.get(0).getDate();
        String windowName = cashWindowStatuses.get(0).getWindowName();

        Date currentDateTime = GlobalResources.getCurrentDate();
        InstrumentDetail instrumentDetailToSave = new InstrumentDetail();
        instrumentDetailToSave.setPayMode(payMode);
        instrumentDetailToSave.setBankName(bankName);
        instrumentDetailToSave.setInstrumentNo(instrumentNo);
        instrumentDetailToSave.setInstrumentDate(instrumentDate);
        instrumentDetailToSave.setMicrCode(micrCode);
        instrumentDetailToSave.setAmount(new BigDecimal(amount));
        instrumentDetailToSave.setDishonoured(InstrumentDetailInterface.DISHONOURED_FALSE);
        instrumentDetailToSave.setCreatedBy(loggedInUser);
        instrumentDetailToSave.setCreatedOn(currentDateTime);
        instrumentDetailToSave.setUpdatedBy(loggedInUser);
        instrumentDetailToSave.setUpdatedOn(currentDateTime);

        InstrumentDetailInterface insertedInstrumentDetail = instrumentDetailService.insert(instrumentDetailToSave);
        if(insertedInstrumentDetail == null){
            logger.error(methodName + "not able to save instrument details");
            throw new Exception("not able to save instrument details");
        }

        long instrumentId = insertedInstrumentDetail.getId();
        savedPayments = new ArrayList<>();
        for(PaymentInterface payment:payments){
            String consumerNo = payment.getConsumerNo();
            Date paymentDate = payment.getPayDate();
            if(consumerNo == null || paymentDate == null){
                logger.error(methodName + "consumer no found null in payment "+payment);
                throw new Exception("consumer no found null in payment "+payment);
            }

            boolean chequeAcceptable = instrumentDishonourService.getValidityByConsumerNoAndDate(consumerNo, paymentDate);
            if(!chequeAcceptable){
                logger.error(methodName + "cheque cant be accepted by consumer no due to dishonouring history "+consumerNo);
                throw new Exception("cheque cant be accepted by consumer no due to dishonouring history "+consumerNo);
            }

            BillInterface bill = billService.getByConsumerNo(consumerNo);
            if(bill == null){
                logger.error(methodName + "bill not found for consumer no "+consumerNo);
                throw new Exception("bill not found for consumer no "+consumerNo);
            }

            long totalPaid = 0;

            List<PaymentInterface> paymentsFromConsumer = getByConsumerNoAndDeletedAndPosted(consumerNo, PaymentInterface.DELETED_FALSE, PaymentInterface.POSTED_FALSE);
            if(paymentsFromConsumer != null && paymentsFromConsumer.size() > 0){
                for (PaymentInterface paymentFromConsumer : paymentsFromConsumer) {
                    if(paymentFromConsumer != null ){
                        long paidAmount = paymentFromConsumer.getAmount();
                        if( paidAmount > 0){
                            totalPaid = totalPaid + paidAmount;
                        }
                    }
                }
            }else{
                logger.info(methodName + "no payments found");
            }

            long balanceAmountToPay;
            if(paymentDate.after(bill.getChequeDueDate())){
                balanceAmountToPay = bill.getNetBill().longValue()
                        +bill.getCurrentBillSurcharge().longValue() - totalPaid;
            }else{
                balanceAmountToPay = bill.getNetBill().longValue() - totalPaid;
            }

            if(payment.getAmount() < balanceAmountToPay){
                logger.error(methodName + "bill amount is less than due amount for consumer no "+consumerNo);
                throw new Exception("bill amount is less than due amount for consumer no "+consumerNo);
            }

            payment.setSource(PaymentInterface.SOURCE_MANUAL);
            payment.setOnline(PaymentInterface.ONLINE_FALSE);
            payment.setLocationCode(bill.getLocationCode());
            payment.setPunchingDate(windowDate);
            payment.setPayWindow(windowName);
            payment.setPayMode(payMode);
            payment.setDeleted(PaymentInterface.DELETED_FALSE);
            payment.setCreatedBy(loggedInUser);
            payment.setCreatedOn(currentDateTime);
            payment.setUpdatedBy(loggedInUser);
            payment.setUpdatedOn(currentDateTime);

            PaymentInterface savedPayment = paymentDAO.add(payment);
            if(savedPayment ==  null){
                logger.error(methodName + "payment not saved "+payment);
                throw new Exception("payment not saved "+payment);
            }

            /**
             * call for payment bifurcation
             */
            ErrorMessage bifurcationError = new ErrorMessage();
            PaymentBifurcationInterface paymentBifurcationInterface = paymentBifurcationService.bifurcatePayment(savedPayment,bifurcationError);
            if(paymentBifurcationInterface == null){
                logger.error(methodName+"Bifurcation error "+bifurcationError);
                throw new Exception("error in payment bifurcation ");
            }


            long paymentId = payment.getId();
            InstrumentPaymentMappingInterface instrumentPaymentMapping = instrumentPaymentMappingService.insertByInstrumentIdAndPaymentId(instrumentId,paymentId);
            if(instrumentPaymentMapping ==  null){
                logger.error(methodName + "InstrumentPaymentMapping not saved "+payment);
                throw new Exception("InstrumentPaymentMapping not saved "+payment);
            }
            savedPayments.add(savedPayment);
        }
        return savedPayments;
    }

    private boolean checkPayModeForInstrument(String payMode) {
        if( payMode.equals(PaymentInterface.PAY_MODE_CHEQUE) || payMode.equals(PaymentInterface.PAY_MODE_BANKERCHEQUE)
                || payMode.equals(PaymentInterface.PAY_MODE_DD)){
            return true;
        }else{
            return false;
        }
    }

    private boolean checkInstrumentAmountAndTotalPaymentAmount(long amount, List<? extends PaymentInterface> payments) {
        long totalAmount = 0;
        for(PaymentInterface payment : payments){
            if(payment == null || payment.getAmount() < PaymentInterface.MINIMUM_PAYMENT_AMOUNT){
                return false;
            }
            totalAmount = totalAmount + payment.getAmount();
        }
        if(amount == totalAmount){
            return true;
        }
        return false;
    }

    @Transactional(rollbackFor = Exception.class)
    public PaymentInterface insertCustomPayment(CustomPayment customPayment) throws Exception {
        String methodName = " insertCustomPayment(): ";
        logger.info(methodName + " method called ");
        if (customPayment == null || customPayment.getPayment() == null) {
            logger.error(methodName + "payment data passed is null");
            throw new Exception("payment data passed is null in method");
        }

        long payAmount = customPayment.getPayment().getAmount();
        String consumerNo = customPayment.getPayment().getConsumerNo();
        Date paymentDate = customPayment.getPayment().getPayDate();
        boolean instrumentFlag = customPayment.isInstrumentFlag();
        boolean partPaymentFlag = customPayment.isPartPaymentFlag();
        boolean agricultureFlag = customPayment.isAgricultureFlag();
        InstrumentDetailInterface instrumentDetail = customPayment.getInstrumentDetail();

        PaymentInterface paymentToSave;
        PaymentInterface insertedPayment;
        PartPaymentInterface partPayment;

        AgricultureBill6MonthlyInterface agricultureBill6Monthly = null;
        BillInterface bill = null;
        Date billChequeDueDate;
        Date billDueDate;
        BigDecimal netBillAmount;
        BigDecimal netBillWithSurcharge;

        long billAmount;
        long partPaymentAmount = 0;
        long totalPaid = 0;
        long balanceAmountToPay = 0;
        Date dueDate;
        String paymentMode;

        Date currentDate = GlobalResources.getCurrentDate();
        String loggedInUser = GlobalResources.getLoggedInUser();
        boolean chequeAcceptable;
        if (consumerNo == null || paymentDate == null || payAmount < PaymentInterface.MINIMUM_PAYMENT_AMOUNT)

            consumerNo = consumerNo.trim();
        if (agricultureFlag) {
            agricultureBill6Monthly = agricultureBill6MonthlyService.getTopByConsumerNo(consumerNo);
            if(agricultureBill6Monthly == null){
                logger.error(methodName + "agriculture bill not found");
                throw new Exception("agriculture bill not found");
            }
            billChequeDueDate = agricultureBill6Monthly.getChequeDueDate();
            billDueDate = agricultureBill6Monthly.getDueDate();
            netBillAmount = agricultureBill6Monthly.getNetBill();
            if(billChequeDueDate == null || billDueDate == null || netBillAmount == null){
                logger.error(methodName + "bill component found null");
                throw new Exception("bill component found null");
            }
            bill = billService.getLatestBillByConsumerNo(consumerNo);
            if(bill == null){
                logger.error(methodName + "bill  found null");
                throw new Exception("bill  found null");
            }
            BigDecimal cumulativeSurcharge = bill.getCumulativeSurcharge();
            if(cumulativeSurcharge == null){
                logger.error(methodName + "cumulativeSurcharge  found null");
                throw new Exception("cumulativeSurcharge  found null");
            }
            netBillWithSurcharge = agricultureBill6Monthly.getNetBill().add(cumulativeSurcharge);
        } else {
            bill = billService.getLatestBillByConsumerNo(consumerNo);
            billChequeDueDate = bill.getChequeDueDate();
            billDueDate = bill.getDueDate();
            netBillAmount = bill.getNetBill();
            netBillWithSurcharge = bill.getNetBill().add(bill.getCurrentBillSurcharge());

        }
        logger.info(methodName + "bill parameter retrieved chequeDueDate " + billChequeDueDate + " DueDate "
                + billDueDate + " netBillAmount " + netBillAmount + " netBillWithSurcharge " + netBillWithSurcharge);
        if (billChequeDueDate == null || billDueDate == null || netBillAmount == null || netBillWithSurcharge == null) {
            logger.error(methodName + "one of the bill parameters found null in bill");
            throw new Exception("one of the bill parameters found null in bill ");
        }

/**
 * checks for Cheque/PartPayment acceptability
 */
        if (instrumentFlag) {
            if (instrumentDetail == null || instrumentDetail.getBankName() == null
                    || instrumentDetail.getInstrumentNo() == null
                    || instrumentDetail.getMicrCode() == null
                    || instrumentDetail.getPayMode() == null
                    || instrumentDetail.getInstrumentDate() == null
                    || instrumentDetail.getAmount() == null
                    || instrumentDetail.getAmount().longValue() < PaymentInterface.MINIMUM_PAYMENT_AMOUNT
                    ) {
                logger.error(methodName + "instrument details missing ");
                throw new Exception("instrument details missing ");
            }
            chequeAcceptable = instrumentDishonourService.getValidityByConsumerNoAndDate(consumerNo, paymentDate);
            if (chequeAcceptable) {
                dueDate = billChequeDueDate;
                paymentMode = instrumentDetail.getPayMode();
                logger.info(methodName + "cheque can be accepted from consumer " + consumerNo +
                        " and cheque due date selected " + dueDate);
            } else {
                logger.error(methodName + "consumer is in defaulter's list, can't take cheque ");
                throw new Exception("consumer is in defaulter's list, can't take cheque ");
            }
        } else {
            dueDate = billDueDate;
            paymentMode = PaymentInterface.PAY_MODE_CASH;
            logger.info(methodName + "payment in cash, normal due date selected " + dueDate);
        }
        logger.info(methodName + "comparing payment date " + paymentDate + " with due date: " + dueDate);
        paymentDate = GlobalResources.getDateWithoutTimeStamp(paymentDate);

        if (paymentDate.after(dueDate)) {
            billAmount = netBillWithSurcharge.longValue();
        } else {
            billAmount = netBillAmount.longValue();
        }
        logger.info(methodName + " bill amount selected " + billAmount);

        if (partPaymentFlag) {
            partPayment = partPaymentService.getNotAppliedPartPaymentForConsumerNo(consumerNo);
            if (partPayment == null) {
                logger.error(methodName + "consumer has no part payment approval ");
                throw new Exception("consumer has no part payment approval ");
            }
            logger.info(methodName + "found part payment approval " + partPayment);
            Date approvedOn = GlobalResources.getDateWithoutTimeStamp(partPayment.getCreatedOn());
            if (approvedOn == null) {
                logger.error(methodName + "part payment creation date not found ");
                throw new Exception("part payment creation date not found ");
            }
            if (paymentDate.before(approvedOn)) {
                logger.error(methodName + "consumer has no active part payment approval ");
                throw new Exception("consumer has no active part payment approval ");
            }

            long dayDifference = GlobalResources.getDateDiffInDays(paymentDate, approvedOn);
            long partPaymentValidForDays = configuratorService.getValueForCode("PART-PAYMENT-VALID-FOR-DAYS");
            if (dayDifference > partPaymentValidForDays) {
                logger.error(methodName + "consumer has no active part payment approval ");
                throw new Exception("consumer has no active part payment approval ");
            }
            partPaymentAmount = partPayment.getAmount();
            logger.info(methodName + "found part payment approval for consumer " + consumerNo);
            if (partPaymentAmount < 1) {
                logger.error(methodName + "part payment amount not found");
                throw new Exception("part payment amount not found");
            }
            logger.info(methodName + "found part payment amount as " + partPaymentAmount);
            partPayment.setApplied(true);
            partPayment.setUpdatedBy(loggedInUser);
            partPayment.setUpdatedOn(currentDate);
            PartPaymentInterface updatedPartPayment = partPaymentService.update(partPayment);
            if (updatedPartPayment == null) {
                logger.error(methodName + "part payment not updated");
                throw new Exception("part payment not updated");
            }
            logger.info(methodName + "part payment updated successfully");
        }

        List<PaymentInterface> payments = getByConsumerNoAndDeletedAndPosted(consumerNo, PaymentInterface.DELETED_FALSE, PaymentInterface.POSTED_FALSE);
        if (payments != null && payments.size() > 0) {
            for (PaymentInterface payment : payments) {
                if (payment != null) {
                    long paidAmount = payment.getAmount();
                    if (paidAmount > 0) {
                        totalPaid = totalPaid + paidAmount;
                    }
                }
            }
        } else {
            logger.info(methodName + "no payments found");
        }

        balanceAmountToPay = billAmount - totalPaid;

        List<CashWindowStatusInterface> cashWindowStatuses = cashWindowStatusService.getByUsernameAndStatus(loggedInUser, CashWindowStatusInterface.STATUS_OPEN);
        if (cashWindowStatuses == null || cashWindowStatuses.size() != 1) {
            logger.error(methodName + "cash window not found");
            throw new Exception("cash window not found ");
        }
        String windowName = cashWindowStatuses.get(0).getWindowName();
        Date windowDate = cashWindowStatuses.get(0).getDate();
        if (windowName == null) {
            logger.error(methodName + "window not found");
            throw new Exception("window not found ");
        }
        paymentToSave = new Payment();
        logger.info(methodName + "amount paid:" + totalPaid + " part payment amount: " + partPaymentAmount + "bill amount " + billAmount + " balanceAmountToPay: " + balanceAmountToPay +
                " payAmount: " + payAmount);
        if (partPaymentFlag && partPaymentAmount > 0 && payAmount >= partPaymentAmount) {
            paymentToSave.setAmount(payAmount);
        } else if (payAmount >= balanceAmountToPay) {
            paymentToSave.setAmount(payAmount);
        } else {
            logger.error(methodName + "part payment not allowed OR payment amount is lesser than billed amount");
            throw new Exception("part payment not allowed OR payment amount is lesser than billed amount ");
        }

        paymentToSave.setSource(PaymentInterface.SOURCE_MANUAL);
        paymentToSave.setOnline(PaymentInterface.ONLINE_FALSE);
        String location = userDetailService.getLoggedInUserDetails().getLocationCode();
        paymentToSave.setLocationCode(location);
        paymentToSave.setConsumerNo(consumerNo);
        /**
         * punching date is window date
         */
        paymentToSave.setPunchingDate(windowDate);
        paymentToSave.setPayDate(paymentDate);
        paymentToSave.setAmount(payAmount);
        paymentToSave.setPayMode(paymentMode);
        paymentToSave.setPayWindow(windowName);
        paymentToSave.setDeleted(PaymentInterface.DELETED_FALSE);
        paymentToSave.setPosted(PaymentInterface.POSTED_FALSE);
        paymentToSave.setCreatedBy(loggedInUser);
        paymentToSave.setCreatedOn(currentDate);
        paymentToSave.setUpdatedBy(loggedInUser);
        paymentToSave.setUpdatedOn(currentDate);

        insertedPayment = paymentDAO.add(paymentToSave);
        if (insertedPayment == null) {
            logger.error(methodName + "payment not saved");
            throw new Exception("payment not saved");
        }
        logger.info(methodName + "Payment saved successfully");
        /**
         * call for payment bifurcation
         */
        ErrorMessage bifurcationError = new ErrorMessage();
        PaymentBifurcationInterface paymentBifurcationInterface = paymentBifurcationService.bifurcatePayment(insertedPayment,bifurcationError);
       if(paymentBifurcationInterface == null){
           logger.error("Bifurcation error "+bifurcationError);
           throw new Exception(bifurcationError.getErrorMessage());
       }

        if (instrumentFlag) {
            instrumentDetail.setDishonoured(InstrumentDetailInterface.DISHONOURED_FALSE);
            instrumentDetail.setCreatedBy(loggedInUser);
            instrumentDetail.setCreatedOn(currentDate);
            instrumentDetail.setUpdatedBy(loggedInUser);
            instrumentDetail.setUpdatedOn(currentDate);
            InstrumentDetailInterface insertedInstrumentDetail = instrumentDetailService.insert(instrumentDetail);
            if (insertedInstrumentDetail == null) {
                logger.error(methodName + "instrument details not saved ");
                throw new Exception("instrument details not saved ");
            }
            InstrumentPaymentMappingInterface insertedInstrumentPaymentMapping = instrumentPaymentMappingService.insertByInstrumentIdAndPaymentId(insertedInstrumentDetail.getId(), insertedPayment.getId());
            if (insertedInstrumentPaymentMapping == null) {
                logger.error(methodName + "InstrumentPaymentMapping not saved ");
                throw new Exception("InstrumentPaymentMapping not saved ");
            }
            logger.info(methodName + "InstrumentDetail,InstrumentPaymentMapping saved successfully");
        }
        return insertedPayment;
    }


    public PaymentInterface getOne(long paymentId) {
        String methodName = "getOne() : ";
        PaymentInterface existingPayment = null;
        if(paymentId>0){
            existingPayment = paymentDAO.getById(paymentId);
        }
        return existingPayment;

    }

    /**
     * Get the Payments for consumer number by specified payment modes
     *
     * @param consumerNo
     * @param payModes
     * @return List of Payments
     */
    public List<PaymentInterface> getByConsumerNoAndPayModes(final String consumerNo, final String[] payModes) {
        final String methodName = "getByConsumerNoAndPayModes() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if (consumerNo != null && !consumerNo.isEmpty() && payModes != null && payModes.length > 0) {
            payments = paymentDAO.getByConsumerNoAndPayModes(consumerNo, payModes);
        }
        return payments;
    }

    public PaymentInterface update(PaymentInterface paymentInterfaceToUpdate){
        final String methodName = "update() : ";
        logger.info(methodName+"called");
        PaymentInterface updatedPayment = null;
        if(paymentInterfaceToUpdate != null){
            updatedPayment = paymentDAO.update(paymentInterfaceToUpdate);
            //
        }
        return updatedPayment;
    }
}
