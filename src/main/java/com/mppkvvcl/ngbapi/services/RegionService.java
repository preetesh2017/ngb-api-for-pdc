package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.RegionInterface;
import com.mppkvvcl.ngbdao.daos.RegionDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by MITHLESH on 18-07-2017.
 */
@Service
public class RegionService {
    /**
     * Asking Spring to fetch Region Repository Singleton Object
     */
    @Autowired
    RegionDAO regionDAO;
    /**
     * This logger is used for logging in current class
     */
    Logger logger = GlobalResources.getLogger(RegionService.class);
    /**
     * This getAll() method  is used to fetch List of Region from Region table in the backend database<br>
     * Return list successful else return null<br><br>
     * return regions<br>
     */
    public List<? extends RegionInterface> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "Received request to fetch List<Region> from table Region");
        List<? extends RegionInterface> regions = regionDAO.getAll();
        if(regions != null){
            if(regions.size() > 0){
                logger.info(methodName + "Successfully got List<Region> with size : "+regions.size());
            }else{
                logger.error(methodName + "Successfully got List<Region> with size : "+regions.size());
            }
        }else{
            logger.error(methodName + "Fetched List<Region> is null");
        }
         return regions;
    }
}
