package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;
import com.mppkvvcl.ngbdao.daos.ReadMasterPFDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by PREETESH on 6/30/2017.
 */

@Service
public class ReadMasterPFService {
    /**
     * Requesting spring to inject singleton ReadMasterPFRepository object.
     */
    @Autowired
    private ReadMasterPFDAO readMasterPFDAO;

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ReadMasterPFService.class);

    /**
     * This getByReadMasterId method takes readMasterId and fetches the object of ReadMasterPF table from backend database<br>
     * Return readMasterPF object if successful else return null.<br><br>
     * param readMasterId<br><br>
     * return readMasterPF<br>
     */
    public ReadMasterPFInterface getByReadMasterId(long readMasterId) {
        String methodName = "getByReadMasterId() : ";
        logger.info(methodName + "called for id " + readMasterId);
        ReadMasterPFInterface readMasterPF=null;
        readMasterPF = readMasterPFDAO.getByReadMasterId(readMasterId);
        if (readMasterPF != null) {
            logger.info(methodName + "successfully fetched ReadMasterPF row");
        }
        return  readMasterPF;
    }

    /**
     * This insert method takes readMasterPF object and insert it into the ReadMasterPF table in the backend database.<br>
     * Return insertedReadMaster object if successful else return null.<br><br>
     * param readMasterPF<br><br>
     * return insertedReadMaster<br>
     */
    public ReadMasterPFInterface insert(ReadMasterPFInterface readMasterPF){
        String methodName = "insert() : ";
        ReadMasterPFInterface insertedReadMaster = null;
        logger.info(methodName + "called");
        if (readMasterPF != null) {
            insertedReadMaster = readMasterPFDAO.add(readMasterPF);
        }
        return insertedReadMaster;
    }
}


