package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbinterface.interfaces.EEDivisionMappingInterface;
import com.mppkvvcl.ngbdao.daos.EEDivisionMappingDAO;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * code by nitish on 23-09-2017
 */
@Service
public class EEDivisionMappingService {
    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(EEDivisionMappingService.class);

    @Autowired
    private EEDivisionMappingDAO eeDivisionMappingDAO;

    /**
     * gives underlying record from passed username
     * @param username
     * @return
     */
    public EEDivisionMappingInterface getByUsername(final String username){
        final String methodName = "getByUsername() : ";
        logger.info(methodName + "called with username " + username);
        EEDivisionMappingInterface eeDivisionMapping = null;
        if(username != null){
            eeDivisionMapping = eeDivisionMappingDAO.getByUsername(username);
        }
        return eeDivisionMapping;
    }

    /**
     * gives underlying record from passed divisionId
     * @param divisionId
     * @return
     */
    public EEDivisionMappingInterface getByDivisionId(final long divisionId){
        final String methodName = "getByDivisionId() : ";
        logger.info(methodName + "called with division id " + divisionId);
        EEDivisionMappingInterface eeDivisionMapping = null;
        eeDivisionMapping = eeDivisionMappingDAO.getByDivisionId(divisionId);
        return eeDivisionMapping;
    }
}
