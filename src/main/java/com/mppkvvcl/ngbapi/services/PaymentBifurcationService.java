package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.PaymentBifurcationDAO;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.PaymentBifurcation;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentBifurcationInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentBifurcationPriorityInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by PREETESH on 1/9/2018.
 */
@Service
public class PaymentBifurcationService {

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger logger = GlobalResources.getLogger(PaymentBifurcationService.class);

    @Autowired
    private PaymentBifurcationDAO paymentBifurcationDAO;

    @Autowired
    private PaymentBifurcationPriorityService paymentBifurcationPriorityService;

    @Autowired
    private BillService billService;

    public PaymentBifurcationInterface bifurcatePayment(PaymentInterface paymentToBifurcate, ErrorMessage errorMessage) {
        String methodName = "bifurcatePayment() : ";
        logger.info(methodName+"called");
        PaymentBifurcationInterface paymentBifurcation = null;
        if(paymentToBifurcate != null){
            BillInterface bill = billService.getLatestBillByConsumerNo(paymentToBifurcate.getConsumerNo());
            if(bill == null){
                errorMessage.setErrorMessage(methodName + "bill not found");
                return null;
            }
            detachBill(bill);
            long billId = bill.getId();

            List<PaymentBifurcationInterface> paymentBifurcationInterfaces = getByBillId(billId);
            if(paymentBifurcationInterfaces != null && paymentBifurcationInterfaces.size() > 0){
                for(PaymentBifurcationInterface paymentBifurcationInterface : paymentBifurcationInterfaces){
                  if(paymentBifurcationInterface == null){
                      errorMessage.setErrorMessage(" existing bifurcation found null");
                      return null;
                  }
                   bill = adjustBillWithBifurcation(bill,paymentBifurcationInterface);
                    if(bill == null){
                        errorMessage.setErrorMessage("Error in updating bill with existing bifurcation");
                        return null;
                    }
                }
            }

            BigDecimal paymentAmount = new BigDecimal(paymentToBifurcate.getAmount());
            paymentBifurcation = new PaymentBifurcation();
            paymentBifurcation.setPaymentId(paymentToBifurcate.getId());
            paymentBifurcation.setBillId(billId);
            paymentBifurcation.setConsumerNo(bill.getConsumerNo());
            paymentBifurcation.setLocationCode(bill.getLocationCode());
            paymentBifurcation.setGroupNo(bill.getGroupNo());
            paymentBifurcation.setReadingDiaryNo(bill.getReadingDiaryNo());

            List<PaymentBifurcationPriorityInterface> paymentBifurcationPriorityInterfaces = paymentBifurcationPriorityService.getAllOrderByPriority();
            if(paymentBifurcationInterfaces == null || paymentBifurcationInterfaces.size() < 1){
                errorMessage.setErrorMessage(methodName + "error in retrieving Bifurcation priority");
                return null;
            }

            for(PaymentBifurcationPriorityInterface paymentBifurcationPriorityInterface: paymentBifurcationPriorityInterfaces){
                if(paymentBifurcationPriorityInterface != null){
                    String head = paymentBifurcationPriorityInterface.getHead();
                    if(head == null){
                        errorMessage.setErrorMessage(methodName + "priority head found null");
                        return null;
                    }
                    switch(head){

                        case "asd_installment":
                            BigDecimal asdInstallment = bill.getAsdInstallment();
                            if(asdInstallment == null){
                                errorMessage.setErrorMessage(methodName + "asd installment in bill found null");
                                return null;
                            }
                            if(paymentAmount.compareTo(BigDecimal.ZERO) > 0){
                                if(paymentAmount.compareTo(asdInstallment) > 0){
                                    paymentBifurcation.setAsdInstallment(asdInstallment);
                                    paymentAmount = paymentAmount.subtract(asdInstallment);
                                }else{
                                    paymentBifurcation.setAsdInstallment(paymentAmount);
                                    paymentAmount = BigDecimal.ZERO;
                                }
                            }else{
                                paymentBifurcation.setAsdInstallment(BigDecimal.ZERO);
                            }
                            break;

                        case "asd_arrear":
                            BigDecimal asdArrear = bill.getAsdArrear();
                            if(asdArrear == null){
                                errorMessage.setErrorMessage(methodName + "asdArrear in bill found null");
                                return null;
                            }
                            if(paymentAmount.compareTo(BigDecimal.ZERO) > 0){
                                if(paymentAmount.compareTo(asdArrear) > 0){
                                    paymentBifurcation.setAsdArrear(asdArrear);
                                    paymentAmount = paymentAmount.subtract(asdArrear);
                                }else{
                                    paymentBifurcation.setAsdArrear(paymentAmount);
                                    paymentAmount = BigDecimal.ZERO;
                                }
                            }else{
                                paymentBifurcation.setAsdArrear(BigDecimal.ZERO);
                            }
                            break;

                        case "arrear":
                            BigDecimal arrear = bill.getArrear();
                            if(arrear == null){
                                errorMessage.setErrorMessage(methodName + "arrear in bill found null");
                                return null;
                            }
                            if(paymentAmount.compareTo(BigDecimal.ZERO) > 0){
                                if(paymentAmount.compareTo(arrear) > 0 ){
                                    paymentBifurcation.setArrear(arrear);
                                    paymentAmount = paymentAmount.subtract(arrear);
                                }else{
                                    paymentBifurcation.setArrear(paymentAmount);
                                    paymentAmount = BigDecimal.ZERO;
                                }
                            }else{
                                paymentBifurcation.setArrear(BigDecimal.ZERO);
                            }
                            break;

                        case "cumulative_surcharge":
                            BigDecimal cumulativeSurcharge = bill.getCumulativeSurcharge();
                            if(cumulativeSurcharge == null){
                                errorMessage.setErrorMessage(methodName + "cumulativeSurcharge in bill found null");
                                return null;
                            }
                              if(paymentAmount.compareTo(BigDecimal.ZERO) > 0){
                                if(paymentAmount.compareTo(cumulativeSurcharge) > 0 ){
                                    paymentBifurcation.setCumulativeSurcharge(cumulativeSurcharge);
                                    paymentAmount = paymentAmount.subtract(cumulativeSurcharge);
                                }else{
                                    paymentBifurcation.setCumulativeSurcharge(paymentAmount);
                                    paymentAmount = BigDecimal.ZERO;
                                }
                            }else{
                                paymentBifurcation.setCumulativeSurcharge(BigDecimal.ZERO);
                            }
                            break;

                        case "current_bill":
                            BigDecimal currentBill = bill.getCurrentBill();
                            if(currentBill == null){
                                errorMessage.setErrorMessage(methodName + "currentBill in bill found null");
                                return null;
                            }
                            if(paymentAmount.compareTo(BigDecimal.ZERO) > 0){
                                if(paymentAmount.compareTo(currentBill) > 0){
                                    paymentBifurcation.setCurrentBill(currentBill);
                                    paymentAmount = paymentAmount.subtract(currentBill);
                                }else{
                                    paymentBifurcation.setCurrentBill(paymentAmount);
                                    paymentAmount = BigDecimal.ZERO;
                                }
                            }else{
                                paymentBifurcation.setCurrentBill(BigDecimal.ZERO);
                            }
                            break;
                    }
                }
            }
            if(paymentAmount.compareTo(BigDecimal.ZERO) > 0){
                paymentBifurcation.setAdvancePayment(paymentAmount);
            }
        }
        paymentBifurcation.setCreatedOn(GlobalResources.getCurrentDate());
        paymentBifurcation.setCreatedBy(GlobalResources.getLoggedInUser());
        paymentBifurcation.setUpdatedOn(GlobalResources.getCurrentDate());
        paymentBifurcation.setUpdatedBy(GlobalResources.getLoggedInUser());
        PaymentBifurcationInterface insertedPaymentBifurcationInterface = paymentBifurcationDAO.add(paymentBifurcation);
        return insertedPaymentBifurcationInterface;
    }


    private BillInterface adjustBillWithBifurcation(BillInterface bill, PaymentBifurcationInterface paymentBifurcation){
        String methodName = "adjustBillWithBifurcation() : ";
        logger.info(methodName+"called");

        BigDecimal billComponent = bill.getAsdInstallment();
        BigDecimal bifurcationComponent = paymentBifurcation.getAsdInstallment();
        if(billComponent != null && bifurcationComponent != null){
            if(billComponent.compareTo(BigDecimal.ZERO) > 0 && bifurcationComponent.compareTo(BigDecimal.ZERO) > 0){
                BigDecimal difference = billComponent.subtract(bifurcationComponent);
                if(difference.compareTo(BigDecimal.ZERO) > 0){
                    bill.setAsdInstallment(difference);
                }else{
                    bill.setAsdInstallment(BigDecimal.ZERO);
                }
            }
        }else{
            logger.error(methodName + "bill component or bifurcation component is null ");
            return null;
        }

        billComponent = bill.getAsdArrear();
        bifurcationComponent = paymentBifurcation.getAsdArrear();
        if(billComponent != null && bifurcationComponent != null){
            if(billComponent.compareTo(BigDecimal.ZERO) > 0 && bifurcationComponent.compareTo(BigDecimal.ZERO) > 0){
                BigDecimal difference = billComponent.subtract(bifurcationComponent);
                if(difference.compareTo(BigDecimal.ZERO) > 0){
                    bill.setAsdArrear(difference);
                }else{
                    bill.setAsdArrear(BigDecimal.ZERO);
                }
            }
        }else{
            logger.error(methodName + "bill component or bifurcation component is null ");
            return null;
        }

        billComponent = bill.getArrear();
        bifurcationComponent = paymentBifurcation.getArrear();
        if(billComponent != null && bifurcationComponent != null){
            if(billComponent.compareTo(BigDecimal.ZERO) > 0 && bifurcationComponent.compareTo(BigDecimal.ZERO) > 0){
                BigDecimal difference = billComponent.subtract(bifurcationComponent);
                if(difference.compareTo(BigDecimal.ZERO) > 0){
                    bill.setArrear(difference);
                }else{
                    bill.setArrear(BigDecimal.ZERO);
                }
            }
        }else{
            logger.error(methodName + "bill component or bifurcation component is null ");
            return null;
        }

        billComponent = bill.getCumulativeSurcharge();
        bifurcationComponent = paymentBifurcation.getCumulativeSurcharge();
        if(billComponent != null && bifurcationComponent != null){
            if(billComponent.compareTo(BigDecimal.ZERO) > 0 && bifurcationComponent.compareTo(BigDecimal.ZERO) > 0){
                BigDecimal difference = billComponent.subtract(bifurcationComponent);
                if(difference.compareTo(BigDecimal.ZERO) > 0){
                    bill.setCumulativeSurcharge(difference);
                }else{
                    bill.setCumulativeSurcharge(BigDecimal.ZERO);
                }
            }
        }else{
            logger.error(methodName + "bill component or bifurcation component is null ");
            return null;
        }

        billComponent = bill.getCurrentBill();
        bifurcationComponent = paymentBifurcation.getCurrentBill();
        if(billComponent != null && bifurcationComponent != null){
            if(billComponent.compareTo(BigDecimal.ZERO) > 0 && bifurcationComponent.compareTo(BigDecimal.ZERO) > 0){
                BigDecimal difference = billComponent.subtract(bifurcationComponent);
                if(difference.compareTo(BigDecimal.ZERO) > 0){
                    bill.setCurrentBill(difference);
                }else{
                    bill.setCurrentBill(BigDecimal.ZERO);
                }
            }
        }else{
            logger.error(methodName + "bill component or bifurcation component is null ");
            return null;
        }
        return bill;
    }

    private List<PaymentBifurcationInterface> getByBillId(long billId) {
        String methodName = "getByBillId() : ";
        logger.info(methodName+"called");
        List<PaymentBifurcationInterface> paymentBifurcationInterfaces = paymentBifurcationDAO.getByBillIdOrderByIdAsc(billId);
        return paymentBifurcationInterfaces;
    }

    private void detachBill(BillInterface billInterface){
        final String methodName = "detachSecurityDepositInterest() : ";
        logger.info(methodName + "called");
        if(billInterface != null){
            entityManager.detach(billInterface);
        }
    }
}
