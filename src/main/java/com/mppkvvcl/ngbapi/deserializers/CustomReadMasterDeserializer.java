package com.mppkvvcl.ngbapi.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.mppkvvcl.ngbapi.custombeans.CustomReadMaster;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ReadMaster;
import com.mppkvvcl.ngbentity.beans.ReadMasterKW;
import com.mppkvvcl.ngbentity.beans.ReadMasterPF;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;
import org.slf4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * class for implementing custom de-serialization for ReadMaster class
 */
public class CustomReadMasterDeserializer extends JsonDeserializer<ReadMaster> {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static  Logger logger = GlobalResources.getLogger(CustomReadMasterDeserializer.class);

    private final String DATE_FORMAT = "yyyy-MM-dd";

    private final String DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm:ss";

    @Override
    public CustomReadMaster deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String methodName = "deserialize() : ";
        logger.info(methodName + "called");
        CustomReadMaster readMasterInterface = null;
        if(jsonParser != null){
            //ReadMasterInterface readMasterInterface = jsonParser.readValueAs(ReadMaster.class);
            ObjectCodec objectCodec = jsonParser.getCodec();
            if(objectCodec != null){
                JsonNode jsonNode = objectCodec.readTree(jsonParser);
                if(jsonNode != null){

                    long id = -1;
                    JsonNode idNode = jsonNode.get("id");
                    if(idNode != null && !idNode.isNull()){
                        id = jsonNode.get("id").asLong();
                    }

                    String billMonth = null;
                    JsonNode billMonthNode = jsonNode.get("billMonth");
                    if(billMonthNode != null && !billMonthNode.isNull()){
                        billMonth = jsonNode.get("billMonth").asText();
                    }

                    String groupNo = null;
                    JsonNode groupNoNode = jsonNode.get("groupNo");
                    if(groupNoNode != null && !groupNoNode.isNull()) {
                        groupNo = jsonNode.get("groupNo").asText();
                    }

                    String readingDiaryNo = null;
                    JsonNode readingDiaryNoNode = jsonNode.get("readingDiaryNo");
                    if(readingDiaryNoNode != null && !readingDiaryNoNode.isNull()) {
                        readingDiaryNo = jsonNode.get("readingDiaryNo").asText();
                    }

                    String consumerNo = null;
                    JsonNode consumerNoNode = jsonNode.get("consumerNo");
                    if(consumerNoNode != null && !consumerNoNode.isNull()) {
                        consumerNo = jsonNode.get("consumerNo").asText();
                    }

                    String meterIdentifier = null;
                    JsonNode meterIdentifierNode = jsonNode.get("meterIdentifier");
                    if(meterIdentifierNode != null && !meterIdentifierNode.isNull()) {
                        meterIdentifier = jsonNode.get("meterIdentifier").asText();
                    }

                    Date readingDate = null;
                    JsonNode readingDateNode = jsonNode.get("readingDate");
                    if(readingDateNode != null && !readingDateNode.isNull()){
                        readingDate = getDateInFormat(jsonNode.get("readingDate").asText().trim(),DATE_FORMAT);
                    }

                    String readingType = null;
                    JsonNode readingTypeNode = jsonNode.get("readingType");
                    if(readingTypeNode != null && !readingTypeNode.isNull()){
                        readingType = jsonNode.get("readingType").asText();
                    }

                    String meterStatus = null;
                    JsonNode meterStatusNode = jsonNode.get("meterStatus");
                    if(meterStatusNode != null && !meterStatusNode.isNull()) {
                        meterStatus = jsonNode.get("meterStatus").asText();
                    }

                    String replacementFlag = null;
                    JsonNode replacementFlagNode = jsonNode.get("replacementFlag");
                    if( replacementFlagNode != null && !replacementFlagNode.isNull()){
                        replacementFlag = jsonNode.get("replacementFlag").asText();
                    }

                    String source = null;
                    JsonNode sourceNode = jsonNode.get("source");
                    if(sourceNode != null && !sourceNode.isNull()){
                        source = jsonNode.get("source").asText();
                    }

                    BigDecimal reading = null;
                    JsonNode readingNode = jsonNode.get("reading");
                    if(readingNode != null && !readingNode.isNull()) {
                        reading = jsonNode.get("reading").decimalValue();
                    }

                    BigDecimal difference = null;
                    JsonNode differenceNode = jsonNode.get("difference");
                    if(differenceNode != null && !differenceNode.isNull()) {
                        difference = jsonNode.get("difference").decimalValue();
                    }

                    BigDecimal mf = null;
                    JsonNode mfNode = jsonNode.get("mf");
                    if(mfNode != null && !mfNode.isNull()) {
                        mf = jsonNode.get("mf").decimalValue();
                    }

                    BigDecimal consumption = null;
                    JsonNode consumptionNode = jsonNode.get("consumption");
                    if(consumptionNode != null && !consumptionNode.isNull()){
                        consumption = jsonNode.get("consumption").decimalValue();
                    }

                    BigDecimal assessment = null;
                    JsonNode assessmentNode = jsonNode.get("assessment");
                    if(assessmentNode != null && !assessmentNode.isNull()){
                        assessment = jsonNode.get("assessment").decimalValue();
                    }

                    BigDecimal propagatedAssessment = null;
                    JsonNode propagatedAssessmentNode = jsonNode.get("propagatedAssessment");
                    if(propagatedAssessmentNode != null && !propagatedAssessmentNode.isNull()){
                        propagatedAssessment = jsonNode.get("propagatedAssessment").decimalValue();
                    }

                    BigDecimal totalConsumption = null;
                    JsonNode totalConsumptionNode = jsonNode.get("totalConsumption");
                    if(totalConsumptionNode != null && !totalConsumptionNode.isNull()){
                        totalConsumption = jsonNode.get("totalConsumption").decimalValue();
                    }

                    boolean usedOnBill = false;
                    JsonNode usedOnBillNode = jsonNode.get("usedOnBill");
                    if(usedOnBillNode != null  && !usedOnBillNode.isNull()) {
                        usedOnBill = jsonNode.get("usedOnBill").asBoolean();
                    }

                    String createdBy = null;
                    JsonNode createdByNode = jsonNode.get("createdBy");
                    if(createdByNode != null && !createdByNode.isNull()) {
                        createdBy = jsonNode.get("createdBy").asText();
                    }

                    Date createdOn = null;
                    JsonNode createdOnNode = jsonNode.get("createdOn");
                    if(createdOnNode != null && !createdOnNode.isNull()){
                        createdOn = getDateInFormat(jsonNode.get("createdOn").asText().trim(),DATE_TIME_FORMAT);
                    }

                    String updatedBy = null;
                    JsonNode updatedByNode = jsonNode.get("updatedBy");
                    if(updatedByNode != null && !updatedByNode.isNull()) {
                        updatedBy = jsonNode.get("updatedBy").asText();
                    }

                    Date updatedOn = null;
                    JsonNode updatedOnNode = jsonNode.get("updatedOn");
                    if(updatedOnNode != null && !updatedOnNode.isNull()){
                        updatedOn = getDateInFormat(jsonNode.get("updatedOn").asText().trim(),DATE_TIME_FORMAT);
                    }

                    String remark = null;
                    JsonNode remarkNode = jsonNode.get("remark");
                    if(remarkNode != null && !remarkNode.isNull()) {
                        remark = jsonNode.get("remark").asText();
                    }

                    ReadMasterKWInterface readMasterKWInterface = getReadMasterKWInterface(jsonNode.get("readMasterKW"));
                    ReadMasterPFInterface readMasterPFInterface = getReadMasterPFInterface(jsonNode.get("readMasterPF"));

                    readMasterInterface = new CustomReadMaster(id,billMonth,groupNo,readingDiaryNo,consumerNo,meterIdentifier,readingDate,readingType,meterStatus,replacementFlag,source,reading,difference,
                            mf,consumption,assessment,propagatedAssessment,totalConsumption,usedOnBill,createdBy,createdOn,updatedBy,updatedOn,remark,readMasterKWInterface,readMasterPFInterface);
                    logger.info(methodName + "Sending ReadMaster after custom parsing is " + readMasterInterface);
                }
            }
        }
        return readMasterInterface;
    }

    private ReadMasterKWInterface getReadMasterKWInterface(JsonNode jsonNode){
        String methodName = "getReadMasterKWInterface() : ";
        logger.info(methodName + "called ");
        ReadMasterKWInterface readMasterKWInterface = null;
        if(jsonNode != null && !jsonNode.isNull()){
            long id = 0 ;
            JsonNode idNode = jsonNode.get("id");
            if(idNode != null && !idNode.isNull()){
                id = jsonNode.get("id").asLong();
            }

            long readMasterId = 0;
            JsonNode readMasterIdNode = jsonNode.get("readMasterId");
            if(readMasterIdNode != null && !readMasterIdNode.isNull()){
                readMasterId = jsonNode.get("readMasterId").asLong();
            }

            BigDecimal meterMD = null;
            JsonNode meterMDNode = jsonNode.get("meterMD");
            if(meterMDNode != null && !meterMDNode.isNull()) {
                meterMD = jsonNode.get("meterMD").decimalValue();
            }

            BigDecimal multipliedMD = null;
            JsonNode multipliedMDNode = jsonNode.get("multipliedMD");
            if(multipliedMDNode != null && !multipliedMDNode.isNull()){
                multipliedMD = jsonNode.get("multipliedMD").decimalValue();
            }

            BigDecimal billingDemand = null;
            JsonNode billingDemandNode = jsonNode.get("billingDemand");
            if(billingDemandNode != null && !billingDemandNode.isNull()) {
                billingDemand = jsonNode.get("billingDemand").decimalValue();
            }

            String createdBy = null;
            JsonNode createdByNode = jsonNode.get("createdBy");
            if(createdByNode != null && !createdByNode.isNull()){
                createdBy = jsonNode.get("createdBy").asText();
            }

            Date createdOnString = null;
            JsonNode createdOnNode = jsonNode.get("createdOn");
            if(createdOnNode != null && !createdOnNode.isNull()){
                createdOnString = getDateInFormat(jsonNode.get("createdOn").asText().trim(),DATE_TIME_FORMAT);
            }

            String updatedBy = null;
            JsonNode updatedByNode = jsonNode.get("updatedBy");
            if(updatedByNode != null && !updatedByNode.isNull()){
                updatedBy = jsonNode.get("updatedBy").asText();
            }

            Date updatedOnString = null;
            JsonNode updatedOnNode = jsonNode.get("updatedOn");
            if(updatedOnNode != null && !updatedOnNode.isNull()){
                updatedOnString = getDateInFormat(jsonNode.get("updatedOn").asText().trim(),DATE_TIME_FORMAT);
            }

            readMasterKWInterface = new ReadMasterKW(id,readMasterId,meterMD,multipliedMD,billingDemand,createdBy,createdOnString,updatedBy,updatedOnString);
            //logger.info(methodName + "ReadMasterKW from custom parser is " + readMasterKWInterface);
        }
        return readMasterKWInterface;
    }

    private ReadMasterPFInterface getReadMasterPFInterface(JsonNode jsonNode){
        String methodName = "getReadMasterPFInterface() : ";
        logger.info(methodName + "called ");
        ReadMasterPFInterface readMasterPFInterface = null;
        if(jsonNode != null && !jsonNode.isNull()){
            long id = 0 ;
            JsonNode idNode = jsonNode.get("id");
            if(idNode != null && !idNode.isNull()){
                id = jsonNode.get("id").asLong();
            }

            long readMasterId = 0;
            JsonNode readMasterIdNode = jsonNode.get("readMasterId");
            if(readMasterIdNode != null && !readMasterIdNode.isNull()) {
                readMasterId = jsonNode.get("readMasterId").asLong();
            }

            BigDecimal meterPF = null;
            JsonNode meterPFNode = jsonNode.get("meterPF");
            if(meterPFNode != null && !meterPFNode.isNull()) {
                meterPF = jsonNode.get("meterPF").decimalValue();
            }

            BigDecimal billingPF = null;
            JsonNode billingPFNode = jsonNode.get("billingPF");
            if(billingPFNode != null && !billingPFNode.isNull()) {
                billingPF = jsonNode.get("billingPF").decimalValue();
            }

            String createdBy = null;
            JsonNode createdByNode = jsonNode.get("createdBy");
            if(createdByNode != null && !createdByNode.isNull()) {
                createdBy = jsonNode.get("createdBy").asText();
            }

            Date createdOnString = null;
            JsonNode createdOnNode = jsonNode.get("createdOn");
            if(createdOnNode != null && !createdOnNode.isNull()){
                createdOnString = getDateInFormat(jsonNode.get("createdOn").asText().trim(),DATE_TIME_FORMAT);
            }

            String updatedBy = null;
            JsonNode updatedByNode= jsonNode.get("updatedBy");
            if(updatedByNode != null && !updatedByNode.isNull()) {
                updatedBy = jsonNode.get("updatedBy").asText();
            }

            Date updatedOnString = null;
            JsonNode updatedOnNode = jsonNode.get("updatedOn");
            if(updatedOnNode != null && !updatedOnNode.isNull()){
                updatedOnString = getDateInFormat(jsonNode.get("updatedOn").asText().trim(),DATE_TIME_FORMAT);
            }

            readMasterPFInterface = new ReadMasterPF(id,readMasterId,meterPF,billingPF,createdBy,createdOnString,updatedBy,updatedOnString);
            //logger.info(methodName + "ReadMasterPF from custom parser is " + readMasterPFInterface);
        }
        return readMasterPFInterface;
    }

    private Date getDateInFormat(String dateToParse,String format){
        final String methodName = "getDateInFormat() : ";
        logger.info(methodName + "called " + dateToParse);
        Date parsedDate = null;
        if(dateToParse != null && format != null && !dateToParse.contains("null")){
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            try{
                parsedDate = sdf.parse(dateToParse);
            }catch (Exception exception){
                logger.error(methodName + "Exception occurred " + exception);
                if(!dateToParse.contains("-")){
                    logger.info(methodName + "Parsing date considering time stamp ");
                    Date timestamp = new Date();
                    timestamp.setTime(Long.parseLong(dateToParse));
                    try{
                        parsedDate = sdf.parse(sdf.format(timestamp));
                    }catch (Exception e){
                        logger.error(methodName + "Exception occurred while parsing timestamp");
                    }
                }
            }
        }
        return parsedDate;
    }
}
