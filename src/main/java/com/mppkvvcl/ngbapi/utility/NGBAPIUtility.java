package com.mppkvvcl.ngbapi.utility;

import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * Developed by Nitish on 18112017.
 * Below class provides various utility methods for helping
 * NGB-API development.
 * Understand the code & concept before changing anything. Some of the
 * functions in this class uses Java's reflection feature. Understand it
 * before changing anything.
 */
public class NGBAPIUtility {

    private static final Logger logger = GlobalResources.getLogger(NGBAPIUtility.class);


    public static final String SORT_ORDER_ASCENDING = "ASC";

    public static final String SORT_ORDER_DESCENDING = "DESC";

    public static final int FIRST_SHEET_INDEX = 0;

    public static final int READ_UPLOAD_FILE_COLUMN_COUNT = 15;

    public static final String REMARK_COLUMN_NAME = "REMARK";

    public static final String UPLOAD_REMARK = "UPLOADED";

    public static final String ACCT_ID_COLUMN_NAME = "ACCT_ID";

    public static final String CURRENT_READ_DTTM_COLUMN_NAME = "CURRENT_READ_DTTM";

    public static final String READER_REM_CD_COLUMN_NAME = "READER_REM_CD";

    public static final String KWH_READING_COLUMN_NAME = "KWH_READING";

    public static final String KW_READING_COLUMN_NAME = "KW_READING";

    public static final String PF_READING_COLUMN_NAME = "PF_READING";

    public static final String ASSESSED_UNITS_COLUMN_NAME = "ASSESSED_UNITS";

    public static final String PMR_FILE = "PMR_FILE";

    public static final String DATE_FORMAT_DD_MMM_YYYY = "dd-MMM-yyyy";

    public static final String PMR_FILE_READING_TYPE_NRML = "NRML";

    public static final String PMR_FILE_READING_TYPE_PFL = "PFL";

    public static final String PMR_FILE_READING_TYPE_M_SD = "M_SD";

    public static final String PMR_FILE_READING_TYPE_METER_CHANGE = "Meter Change";

    public static final String PMR_FILE_INVALID_FORMAT_MSG = "NOT IN REQUIRED FORMAT.";

    public static final String PMR_FILE_INVALID_READING_TYPE_MSG = "READING TYPE COLUMN NOT VALID .";

    public static final String PMR_FILE_INVALID_CONSUMER_NO_MSG = "CONSUMER NO COLUMN NOT VALID.";

    public static final String PMR_FILE_INVALID_READING_DATE_MSG = "READING DATE COLUMN NOT VALID.";

    public static final String PMR_FILE_INVALID_KWH_MSG = "KWH COLUMN NOT VALID.";

    public static final String PMR_FILE_INVALID_KW_MSG = "KW COLUMN NOT VALID.";

    public static final String PMR_FILE_INVALID_PF_MSG = "PF COLUMN NOT VALID.";

    public static final String PMR_FILE_INVALID_ASSESSMENT_MSG = "ASSESSMENT COLUMN NOT VALID.";


    /**
     * This function gives Java's Persistence Column Annotation class
     * if used on passed field.
     * @param field
     * @return
     */
    public static Column getColumnAnnotationByNameValue(Field field){
        final String methodName = "getColumnAnnotation() : ";
        logger.info(methodName + "called");
        Column columnAnnotation = null;
        if(field != null){
            Annotation[] annotations = field.getDeclaredAnnotations();
            for(Annotation annotation : annotations){
                if(annotation != null && annotation instanceof Column){
                    columnAnnotation = (Column)annotation;
                }
            }
        }
        return columnAnnotation;
    }

    /**
     * This method returns Field class object for provided fieldName
     * in provided object
     * @param fieldName
     * @param object
     * @return
     */
    public static Field getFieldByName(String fieldName, Object object){
        final String methodName = "getFieldByName() : ";
        logger.info(methodName + "called");
        Field field = null;
        if(fieldName != null && object != null){
            Field fields[] = object.getClass().getDeclaredFields();
            for (Field f : fields) {
                if(f.getName().equals(fieldName)){
                    field = f;
                    break;
                }
            }
        }
        return field;
    }

    /**
     * Function to get column name of backend database table
     * as per provided params
     * @param fieldName
     * @param object
     * @return
     */
    public static String getColumnNameByFieldName(String fieldName,Object object){
        final String methodName = "getColumnNameByFieldName() : ";
        logger.info(methodName + "called");
        String columnName = null;
        if(fieldName != null && object != null){
            Field field = getFieldByName(fieldName,object);
            Column column = getColumnAnnotationByNameValue(field);
            if(column != null){
                columnName = column.name();
            }
        }
        return columnName;
    }

    /**
     * This method returns Method class object for provided field Name
     * in provided object if there is a getter for provided field Name
     * @param field
     * @param object
     * @return
     */
    public static Method getGetterForField(String field, Object object){
        final String methodName = "getGetterForField() : ";
        logger.info(methodName + "called");
        Method method = null;
        if(field != null && object != null){
            Method methods[] = object.getClass().getMethods();
            for (Method m : methods) {
                if(isGetter(m) && m.getName().toLowerCase().endsWith(field.toLowerCase())){
                    method = m;
                    break;
                }
            }
        }
        return method;
    }

    /**
     * This method returns Method class object for provided field Name
     * in provided object if there is a setter for provided field Name
     * @param field
     * @param object
     * @return
     */
    public static Method getSetterForField(String field,Object object){
        final String methodName = "getSetterForField() : ";
        logger.info(methodName + "called");
        Method method = null;
        if(field != null && object != null){
            Method methods[] = object.getClass().getMethods();
            for (Method m : methods) {
                if(isSetter(m) && m.getName().toLowerCase().endsWith(field.toLowerCase())){
                    method = m;
                    break;
                }
            }
        }
        return method;
    }

    /**
     * This method checks whether the passed Method class object
     * is a getter method or not. returns True if method name starts with get,
     * returns a Type and takes 0 parameter
     * @param method
     * @return
     */
    public static boolean isGetter(Method method){
        final String methodName = "isGetter() : ";
        //logger.info(methodName + "called");
        if(!method.getName().startsWith("get"))      return false;
        if(method.getParameterTypes().length != 0)   return false;
        if(void.class.equals(method.getReturnType())) return false;
        return true;
    }

    /**
     * This method checks whether the passed Method class object
     * is a setter method or not. returns True if method name starts with set,
     * returns void and takes 1 parameter
     * @param method
     * @return
     */
    public static boolean isSetter(Method method){
        final String methodName = "isSetter() : ";
        //logger.info(methodName + "called");
        if(!method.getName().startsWith("set")) return false;
        if(method.getParameterTypes().length != 1) return false;
        return true;
    }

    public static Workbook getWorkBookFromFile(MultipartFile file) {
        final String methodName = "getWorkBookFromFile() : ";
        logger.info(methodName + "called");
        Workbook workbook = null;
        if (file == null || file.isEmpty()) {
            return workbook;
        }
        try (InputStream inputStream = file.getInputStream();
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        ) {
            workbook = WorkbookFactory.create(bufferedInputStream);
        } catch (Exception exception) {
            logger.info(methodName + "received exception "+exception.getMessage());
            workbook = null;
        }
        return workbook;
    }

    public static Sheet  getWorkSheetFromWorkBookBySheetNo(Workbook workbook, int sheetNumber) {
        final String methodName = " getWorkSheetFromWorkBookBySheetNo() : ";
        logger.info(methodName + "called");
        Sheet sheet = null;
        if (workbook == null || sheetNumber < 0) {
            return sheet;
        }
        sheet = workbook.getSheetAt(sheetNumber);
        return sheet;
    }

    public static boolean checkCellCountInRow(Row row, int cellCount) {
        final String methodName = "checkCellCountInRow() : ";
        logger.info(methodName + "called");
        if (row != null && cellCount >= 0) {
            return cellCount == row.getPhysicalNumberOfCells();
        }
        return false;
    }

    public static Cell getNewCell(Row row, int cellNumber, String message) {
        final String methodName = "getNewCell() : ";
        logger.info(methodName + "called");
        Cell newCell = null;
        if (row != null && message != null && cellNumber >= 0) {
            newCell = row.createCell(cellNumber);
            newCell.setCellValue(message);
        }
        return newCell;
    }

    public static boolean isValidReadingHeaderRow(Row row, CellStyle cellStyle, ErrorMessage errorMessage) {
        final String methodName = "isValidReadingHeaderRow() : ";
        logger.info(methodName + "called");
        boolean matched = true;
        if (row == null || cellStyle == null) {
            matched = matched && false;
            return matched;
        }
        Iterator<Cell> cellIterator = row.cellIterator();
        if (cellIterator == null) {
            logger.error(methodName + "some error to get cell iterator from row.");
            errorMessage.setErrorMessage("some internal error , please try after some time.");
            return false;
        }

        boolean isValidColumnCount = NGBAPIUtility.checkCellCountInRow(row, NGBAPIUtility.READ_UPLOAD_FILE_COLUMN_COUNT);
        if (!isValidColumnCount) {
            logger.error(methodName + "row count mismatch");
            errorMessage.setErrorMessage(methodName + "header row doesn't have required cells.");
            return false;
        }

        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if (cell == null) {
                cell.setCellStyle(cellStyle);
                continue;
            }
            int index = cell.getColumnIndex();
            CellType type = cell.getCellTypeEnum();
            switch (index) {
                case 0: //for account id
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cell.getStringCellValue().equalsIgnoreCase(NGBAPIUtility.ACCT_ID_COLUMN_NAME)) {
                                break;
                            }
                        }
                    }
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_CONSUMER_NO_MSG);
                    cell.setCellStyle(cellStyle);
                    matched = matched && false;
                    break;
                case 1:  //for reading date
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cell.getStringCellValue().equalsIgnoreCase(NGBAPIUtility.CURRENT_READ_DTTM_COLUMN_NAME)) {
                                break;
                            }
                        }
                    }
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_READING_DATE_MSG);
                    cell.setCellStyle(cellStyle);
                    matched = matched && false;
                    break;
                case 2: //for reading type
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cell.getStringCellValue().equalsIgnoreCase(NGBAPIUtility.READER_REM_CD_COLUMN_NAME)) {
                                break;
                            }
                        }
                    }
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_READING_TYPE_MSG);
                    cell.setCellStyle(cellStyle);
                    matched = matched && false;
                    break;
                case 3: //for KWH
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cell.getStringCellValue().equalsIgnoreCase(NGBAPIUtility.KWH_READING_COLUMN_NAME)) {
                                break;
                            }
                        }
                    }
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_KWH_MSG);
                    cell.setCellStyle(cellStyle);
                    matched = matched && false;
                    break;
                case 4: //for KW
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cell.getStringCellValue().equalsIgnoreCase(NGBAPIUtility.KW_READING_COLUMN_NAME)) {
                                break;
                            }
                        }
                    }
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_KW_MSG);
                    cell.setCellStyle(cellStyle);
                    matched = matched && false;
                    break;
                case 8: //for PF
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cell.getStringCellValue().equalsIgnoreCase(NGBAPIUtility.PF_READING_COLUMN_NAME)) {
                                break;
                            }
                        }
                    }
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_PF_MSG);
                    cell.setCellStyle(cellStyle);
                    matched = matched && false;
                    break;
                case 10: //for Assessed unit
                    if (type.equals(CellType.STRING)) {
                        String cellValue = cell.getStringCellValue();
                        if (cellValue != null && !cellValue.isEmpty()) {
                            if (cell.getStringCellValue().equalsIgnoreCase(NGBAPIUtility.ASSESSED_UNITS_COLUMN_NAME)) {
                                break;
                            }
                        }
                    }
                    errorMessage.setErrorMessage(NGBAPIUtility.PMR_FILE_INVALID_ASSESSMENT_MSG);
                    cell.setCellStyle(cellStyle);
                    matched = matched && false;
                    break;
            }
        }
        return matched;
    }

    public static CellStyle getCellStyle(Workbook workbook) {
        String methodName = "getCellStyle() : ";
        logger.info(methodName + "called");
        CellStyle cellStyle = null;
        if (workbook != null) {
            cellStyle = workbook.createCellStyle();
            cellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }
        return cellStyle;
    }
}
