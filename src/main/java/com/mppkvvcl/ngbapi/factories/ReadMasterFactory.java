package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.ReadMaster;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;

public class ReadMasterFactory {
    public static ReadMasterInterface build(){
        ReadMasterInterface readMasterInterface = new ReadMaster();
        return readMasterInterface;
    }
}
