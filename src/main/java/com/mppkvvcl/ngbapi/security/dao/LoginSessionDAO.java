package com.mppkvvcl.ngbapi.security.dao;

import com.mppkvvcl.ngbinterface.interfaces.LoginSessionInterface;
import com.mppkvvcl.ngbdao.interfaces.LoginSessionDAOInterface;
import com.mppkvvcl.ngbapi.security.repositories.LoginSessionRepository;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginSessionDAO implements LoginSessionDAOInterface<LoginSessionInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(LoginSessionDAO.class);

    @Autowired
    private LoginSessionRepository loginSessionRepository;

    @Override
    public List<LoginSessionInterface> getByUsername(String username) {
        return null;
    }

    @Override
    public List<LoginSessionInterface> getByIp(String ip) {
        return null;
    }

    @Override
    public List<LoginSessionInterface> getByUsernameAndIp(String username, String ip) {
        return null;
    }

    @Override
    public List<LoginSessionInterface> getByBrowserName(String browserName) {
        return null;
    }

    @Override
    public LoginSessionInterface add(LoginSessionInterface loginSessionInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        LoginSessionInterface insertedLoginSession = null;
        if(loginSessionInterface != null){
            insertedLoginSession = loginSessionRepository.save(loginSessionInterface);
        }
        return insertedLoginSession;
    }

    @Override
    public LoginSessionInterface update(LoginSessionInterface loginSessionInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        LoginSessionInterface updatedLoginSession = null;
        if(loginSessionInterface != null){
            updatedLoginSession = loginSessionRepository.save(loginSessionInterface);
        }
        return updatedLoginSession;
    }

    @Override
    public LoginSessionInterface get(LoginSessionInterface loginSessionInterface) {
        return null;
    }

    @Override
    public LoginSessionInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called for id " + id);
        LoginSessionInterface existingLoginSession = null;
        existingLoginSession = loginSessionRepository.findOne(id);
        return existingLoginSession;
    }
}
