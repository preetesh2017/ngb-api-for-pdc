package com.mppkvvcl.ngbapi.security.repositories;

import com.mppkvvcl.ngbinterface.interfaces.LoginSessionInterface;
import com.mppkvvcl.ngbapi.security.beans.LoginSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoginSessionRepository extends JpaRepository<LoginSession,Long> {
    public List<LoginSessionInterface> findByUsername(String username);
    public List<LoginSessionInterface> findByIp(String ip);
    public List<LoginSessionInterface> findByUsernameAndIp(String username, String ip);
    public List<LoginSessionInterface> findByBrowserName(String browserName);
    public LoginSessionInterface save(LoginSessionInterface loginSessionInterface);
}
