package com.mppkvvcl.ngbapi.security.repositories;

import com.mppkvvcl.ngbapi.security.beans.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by NITISH on 07-07-2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    public User findByUsername(String username);
}
