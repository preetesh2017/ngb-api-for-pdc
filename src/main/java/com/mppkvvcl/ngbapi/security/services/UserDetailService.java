package com.mppkvvcl.ngbapi.security.services;

import com.mppkvvcl.ngbinterface.interfaces.*;
import com.mppkvvcl.ngbapi.services.EEDivisionMappingService;
import com.mppkvvcl.ngbapi.services.OICZoneMappingService;
import com.mppkvvcl.ngbapi.services.SECircleMappingService;
import com.mppkvvcl.ngbapi.services.ZoneService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbapi.security.repositories.UserDetailRepository;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 7/25/2017.
 */
@Service
public class UserDetailService {

    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(UserDetailService.class);

    /**
     * Requesting spring to inject singleton userDetailRepository object.
     */
    @Autowired
    private UserDetailRepository userDetailRepository;

    @Autowired
    private SECircleMappingService seCircleMappingService;

    @Autowired
    private EEDivisionMappingService eeDivisionMappingService;

    @Autowired
    private OICZoneMappingService oicZoneMappingService;

    @Autowired
    private ZoneService zoneService;

    /**
     * This getByName method takes input param name and fetches the userDetail object of UserDetail  table from backend database.<br>
     * Return userDetail object if successful else return null.<br><br>
     * param name<br>
     * return userDetail<br>
     */
    public UserDetail getByName(String name) {
        String methodName = " getByName(): ";
        UserDetail userDetail = null;
        logger.info(methodName + "called for name " + name);
        if (name != null){
            userDetail =  userDetailRepository.findByName(name);
        }
        return userDetail;
    }

    public UserDetail getByUsername(String username) {
        String methodName = "getByUsername(): ";
        UserDetail userDetail = null;
        logger.info(methodName + "called for username " + username);
        if (username != null){
            userDetail =  userDetailRepository.findByUsername(username);
        }
        return userDetail;
    }

    public UserDetail getLoggedInUserDetails() {
        String methodName = " getLoggedInUserDetails(): ";
        UserDetail userDetail = null;
        logger.info(methodName + "called");
        String username = GlobalResources.getLoggedInUser();
        if (username != null){
            userDetail =  userDetailRepository.findByUsername(username);
        }
        return userDetail;
    }

    public List<UserDetail> getByLocationCodeAndRole(final String locationCode,final String role){
        final String methodName = "getByLocationCodeAndRole() : ";
        logger.info(methodName + "called for locationCode " + locationCode + " and role " + role);
        List<UserDetail> userDetails = null;
        if(locationCode != null && role != null){
            userDetails = userDetailRepository.findByLocationCodeAndRole(locationCode,role);
        }
        return userDetails;
    }

    /**
     * coded by nitish
     * This Method fetches the username of AE role for passed location from DataBase
     * param locationCode
     * @param locationCode
     * @return
     */
    public String getAEByLocationCode(final String locationCode){
        final String methodName = "getAEByLocationCode() : ";
        logger.info(methodName + "called for location code " + locationCode);
        String username = null;
        if(locationCode != null){
            ZoneInterface zone = zoneService.getByLocationCode(locationCode);
            if(zone != null){
                OICZoneMappingInterface oicZoneMapping = oicZoneMappingService.getByZoneId(zone.getId());
                if(oicZoneMapping != null){
                    username = oicZoneMapping.getUserDetail().getUsername();
                }
            }
        }
        return username;
        //return zoneRepository.getAEByLocationCode(locationCode, Role.OIC);
    }

    /**
     * code by nitish
     * This Method fetches the username of EE role for passed location from DataBase
     * param locationCode
     * @param locationCode
     * @return
     */
    public String getEEByLocationCode(final String locationCode){
        final String methodName = "getEEByLocationCode() : ";
        logger.info(methodName + "called for location code " + locationCode);
        String username = null;
        if(locationCode != null){
            ZoneInterface zone = zoneService.getByLocationCode(locationCode);
            if(zone != null){
                DivisionInterface division = zone.getDivision();
                if(division != null){
                    EEDivisionMappingInterface eeDivisionMapping = eeDivisionMappingService.getByDivisionId(division.getId());
                    if(eeDivisionMapping != null){
                        username = eeDivisionMapping.getUserDetail().getUsername();
                    }
                }
            }
        }
        return username;
        //return zoneRepository.getEEByLocationCode(locationCode, Role.EE);
    }

    /**
     * This Method fetches the username of SE role for passed location from DataBase
     * param locationCode
     * return Username of SE for particular location
     * coded by nitish
     * @param locationCode
     * @return
     */
    public String getSEByLocationCode(final String locationCode){
        final String methodName = "getSEByLocationCode() : ";
        logger.info(methodName + "called for location code " + locationCode);
        String username = null;
        if(locationCode != null){
            ZoneInterface zone = zoneService.getByLocationCode(locationCode);
            if(zone != null){
                DivisionInterface division = zone.getDivision();
                if(division != null){
                    CircleInterface circle = division.getCircle();
                    if(circle != null){
                        SECircleMappingInterface seCircleMapping = seCircleMappingService.getByCircleId(circle.getId());
                        if(seCircleMapping != null){
                            username = seCircleMapping.getUserDetail().getUsername();
                        }
                    }
                }
            }
        }
        return username;
        //return zoneRepository.getSEByLocationCode(locationCode, Role.SE);
    }
}
