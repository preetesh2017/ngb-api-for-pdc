package com.mppkvvcl.ngbapi.security.services;

import com.mppkvvcl.ngbapi.security.beans.Role;
import com.mppkvvcl.ngbapi.security.repositories.RoleRepository;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by NITISH on 14-07-2017.
 */
@Service
public class RoleService {

    private static final Logger logger = GlobalResources.getLogger(RoleService.class);

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> getAllRoles(){
        String methodName = "getAllRoles() : ";
        logger.info(methodName + "called");
        return roleRepository.findAll();
    }

    public List<String> getAllRoleNames(){
        String methodName = "getAllRoleNames() : ";
        logger.info(methodName + "called");
        return roleRepository.findAllRoleNames();
    }

    public Role getByRole(String roleName){
        String methodName = "getByRole";
        logger.info(methodName + " called");
        Role role = null;
        if(roleName != null) {
            role = roleRepository.findByRole(roleName);
        }else{
            logger.error("Role Passed is Empty");
        }
        return role;
    }
}
